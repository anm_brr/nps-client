package com.feratech.nps.application;

import com.feratech.nps.dao.*;
import com.feratech.nps.domain.AdditionalSku;
import com.feratech.nps.error.ExceptionResolver;
import com.feratech.nps.helper.UrlResolver;
import com.feratech.nps.utils.Logger;
import com.google.inject.AbstractModule;
import com.google.inject.Scopes;
import com.loopj.android.http.AsyncHttpClient;

/**
 * User: Bazlur Rahman Rokon
 * Date: 7/9/13 - 3:26 AM
 */
public class ApplicationModule extends AbstractModule {
    private static final Logger log = Logger.getLogger(ApplicationModule.class);

    @Override
    protected void configure() {
        log.verbose("configure");

        bind(AsyncHttpClient.class).in(Scopes.SINGLETON);
        bind(StoreInfoDao.class).to(StoreInfoDaoImpl.class).in(Scopes.SINGLETON);
        bind(QuestionnaireDao.class).to(QuestionnaireDaoImpl.class).in(Scopes.SINGLETON);
        bind(AdditionalSku.class).in(Scopes.SINGLETON);
        bind(FixedDisplaySkuDao.class).in(Scopes.SINGLETON);
        bind(HotSpotDao.class).in(Scopes.SINGLETON);
        bind(HotSpotWithComplianceDao.class).in(Scopes.SINGLETON);
        bind(HotSpotWithOralCareDao.class).in(Scopes.SINGLETON);
        bind(HotSpotWithSkinCareDao.class).in(Scopes.SINGLETON);
        bind(MustHaveSkuDao.class).in(Scopes.SINGLETON);
        bind(PointOfPurchaseDao.class).in(Scopes.SINGLETON);
        bind(QuestionMetaDao.class).in(Scopes.SINGLETON);
        bind(QuestionnaireMetaDao.class).in(Scopes.SINGLETON);
        bind(QuestionSetDao.class).in(Scopes.SINGLETON);
        bind(ShopTypeDao.class).in(Scopes.SINGLETON);
        bind(UnsuccessfulAuditDao.class).in(Scopes.SINGLETON);
        bind(WaveDisplaySkuDao.class).in(Scopes.SINGLETON);

        requestStaticInjection(UrlResolver.class,
                ExceptionResolver.class
        );
    }
}
