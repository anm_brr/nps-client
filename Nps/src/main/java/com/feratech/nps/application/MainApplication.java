package com.feratech.nps.application;

import android.app.Application;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import com.feratech.nps.dao.StoreInfoDao;
import com.feratech.nps.db.SQLiteDatabaseAdapter;
import com.feratech.nps.domain.DataVersion;
import com.feratech.nps.service.WebServices;
import com.feratech.nps.utils.Constants;
import com.feratech.nps.utils.Logger;
import com.feratech.nps.utils.Utils;
import com.google.inject.Inject;
import org.acra.ACRA;
import org.acra.annotation.ReportsCrashes;
import roboguice.RoboGuice;

/**
 * User: Bazlur Rahman Rokon
 * Date: 7/9/13 - 2:39 AM
 */
@ReportsCrashes(formKey = "", formUri = "http://www.bugsense.com/api/acra?api_key=b1897127")
public class MainApplication extends Application {
    private static final String SHOULD_CALL_ONCE = "should_call_once";

    private static final Logger log = Logger.getLogger(MainApplication.class);

    private SharedPreferences prefs;

    @Inject
    WebServices webServices;

    @Inject
    private StoreInfoDao storeInfoDao;

    @Override
    public void onCreate() {
        super.onCreate();
        log.verbose("onCreate()");
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        RoboGuice.setBaseApplicationInjector(this, RoboGuice.DEFAULT_STAGE,
                RoboGuice.newDefaultRoboModule(this), new ApplicationModule());

        SQLiteDatabaseAdapter.getInstance(this, Constants.DATABASE_NAME);


        if (shouldCall()) {
            webServices.retrieveCurrentDataVersion(new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    DataVersion dataVersion = (DataVersion) msg.obj;
                    Utils.saveDataVersion(MainApplication.this, dataVersion);
                }
            });

//            List<StoreInfo> infos = ObjectSerializer.readFromJson(this);
//            try {
//                storeInfoDao.insertBulkData(infos);
//            } catch (SQLException e) {
//                log.error("unable to insert bulk Data");
//            }
//            log.verbose("should call once,so called ={}", infos.size());
            prefs.edit().putBoolean(SHOULD_CALL_ONCE, false).commit();
        }

        ACRA.init(this);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        log.debug("onTerminate()");
    }

    private boolean shouldCall() {
        log.verbose("should call in first in life cycle ");
        boolean shouldCall;
        shouldCall = prefs.getBoolean(SHOULD_CALL_ONCE, true);
        return shouldCall;
    }
}
