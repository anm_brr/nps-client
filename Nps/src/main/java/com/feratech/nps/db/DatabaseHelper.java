package com.feratech.nps.db;

import android.app.Application;
import android.database.sqlite.SQLiteDatabase;
import com.feratech.nps.domain.*;
import com.feratech.nps.utils.Constants;
import com.feratech.nps.utils.Logger;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

/**
 * User: Bazlur Rahman Rokon
 * Date: 7/9/13 - 3:01 AM
 */

@Singleton
public class DatabaseHelper extends OrmLiteSqliteOpenHelper {
    private static final Logger log = Logger.getLogger(DatabaseHelper.class);
    private static final String DATABASE_NAME = Constants.DATABASE_NAME;

    private Dao<AdditionalSku, Integer> additionalSkuDao = null;
    private Dao<FixedDisplaySku, Integer> fixedDisplaySkuDao = null;
    private Dao<HotSpot, Integer> hotSpotDao = null;
    private Dao<HotSpotWithCompliance, Integer> hotSpotWithComplianceDao = null;
    private Dao<HotSpotWithOralCare, Integer> hotSpotWithOralCareDao = null;
    private Dao<HotSpotWithSkinCare, Integer> hotSpotWithSkinCaresDao = null;
    private Dao<MustHaveSku, Integer> mustHaveSkuDao = null;
    private Dao<PointOfPurchase, Integer> pointOfPurchaseDao = null;
    private Dao<Questionnaire, Integer> questionnaireDao = null;
    private Dao<QuestionMeta, Integer> questionMetaDao = null;
    private Dao<QuestionnaireMeta, Integer> questionnaireMetaDao = null;
    private Dao<QuestionSet, Integer> questionSetDao = null;
    private Dao<ShopType, Integer> shopTypeDao = null;
    private Dao<StoreInfo, Integer> storeInfoDao = null;
    private Dao<UnsuccessfulAudit, Integer> unsuccessfulAuditDao = null;
    private Dao<WaveDisplaySku, Integer> waveDisplaySkuDao = null;

    @Inject
    public DatabaseHelper(Application context) {
        super(context, DATABASE_NAME, null, Constants.DATABASE_VERSION);
        log.verbose("DatabaseHelper() -- constructor ");
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource) {
        log.verbose("onCreate()");
        try {
            createTable(connectionSource);
        } catch (SQLException e) {
            log.error("onCreate() -> Can't crate database", e);
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        log.debug("onUpgrade()-> oldVersion={}, newVersion={}", oldVersion, newVersion);
        try {
            dropTable(connectionSource, true);
            onCreate(sqLiteDatabase, connectionSource);
        } catch (SQLException e) {
            log.error(e, "Can't drop database");
            e.printStackTrace();
        }
    }


    public Dao<AdditionalSku, Integer> getAdditionalSkuDao() throws SQLException {
        log.debug("getAdditionalSkuDao()");
        if (additionalSkuDao == null) this.additionalSkuDao = getDao(AdditionalSku.class);
        return additionalSkuDao;
    }

    public Dao<FixedDisplaySku, Integer> getFixedDisplaySkuDao() throws SQLException {
        log.debug("getFixedDisplaySkuDao()");
        if (fixedDisplaySkuDao == null) this.fixedDisplaySkuDao = getDao(FixedDisplaySku.class);
        return fixedDisplaySkuDao;
    }

    public Dao<MustHaveSku, Integer> getMustHaveSkuDao() throws SQLException {
        log.debug("getMustHaveSkuDao()");
        if (mustHaveSkuDao == null) this.mustHaveSkuDao = getDao(MustHaveSku.class);
        return mustHaveSkuDao;
    }

    public Dao<HotSpot, Integer> getHotSpotDao() throws SQLException {
        log.debug("getHotSpotDao()");
        if (hotSpotDao == null) hotSpotDao = getDao(HotSpot.class);
        return hotSpotDao;
    }

    public Dao<HotSpotWithCompliance, Integer> getHotSpotWithComplianceDaoDao() throws SQLException {
        log.debug("getHotSpotWithComplianceDaoDao()");
        if (hotSpotWithComplianceDao == null) hotSpotWithComplianceDao = getDao(HotSpotWithCompliance.class);
        return hotSpotWithComplianceDao;
    }

    public Dao<HotSpotWithSkinCare, Integer> getHotSpotWithSkinCaresDaoDao() throws SQLException {
        log.debug("getHotSpotWithSkinCaresDaoDao()");
        if (hotSpotWithSkinCaresDao == null) hotSpotWithSkinCaresDao = getDao(HotSpotWithSkinCare.class);
        return hotSpotWithSkinCaresDao;
    }

    public Dao<HotSpotWithOralCare, Integer> getHotSpotWithOralCareDaoDao() throws SQLException {
        log.debug("getHotSpotWithOralCareDaoDao()");
        if (hotSpotWithOralCareDao == null) hotSpotWithOralCareDao = getDao(HotSpotWithOralCare.class);
        return hotSpotWithOralCareDao;
    }

    public Dao<PointOfPurchase, Integer> getPointOfPurchaseDao() throws SQLException {
        log.debug("getPointOfPurchaseDao()");
        if (pointOfPurchaseDao == null) pointOfPurchaseDao = getDao(PointOfPurchase.class);
        return pointOfPurchaseDao;
    }

    public Dao<QuestionMeta, Integer> getQuestionMetaDao() throws SQLException {
        log.debug("getQuestionMetaDao()");
        if (questionMetaDao == null) questionMetaDao = getDao(QuestionMeta.class);
        return questionMetaDao;
    }

    public Dao<QuestionnaireMeta, Integer> getQuestionnaireMetaDao() throws SQLException {
        log.debug("getQuestionnaireMetaDao()");
        if (questionnaireMetaDao == null) questionnaireMetaDao = getDao(QuestionnaireMeta.class);
        return questionnaireMetaDao;
    }

    public Dao<QuestionSet, Integer> getQuestionSetDao() throws SQLException {
        log.debug("getQuestionSetDao()");
        if (questionSetDao == null) questionSetDao = getDao(QuestionSet.class);
        return questionSetDao;
    }

    public Dao<StoreInfo, Integer> getStoreInfoDao() throws SQLException {
        log.debug("getStoreInfoDao()");
        if (this.storeInfoDao == null) this.storeInfoDao = getDao(StoreInfo.class);
        return this.storeInfoDao;
    }

    public Dao<ShopType, Integer> getShopTypeDao() throws SQLException {
        log.debug("getShopTypeDao()");
        if (shopTypeDao == null) shopTypeDao = getDao(ShopType.class);
        return shopTypeDao;
    }

    public Dao<Questionnaire, Integer> getQuestionnaireDao() throws SQLException {
        log.debug("getQuestionnaireDao()");
        if (questionnaireDao == null) questionnaireDao = getDao(Questionnaire.class);
        return questionnaireDao;
    }

    public Dao<WaveDisplaySku, Integer> getWaveDisplaySkuDao() throws SQLException {
        log.debug("getWaveDisplaySkuDao()");
        if (waveDisplaySkuDao == null) waveDisplaySkuDao = getDao(WaveDisplaySku.class);
        return waveDisplaySkuDao;
    }

    public Dao<UnsuccessfulAudit, Integer> getUnsuccessfulAuditsDao() throws SQLException {
        log.debug("getUnsuccessfulAuditsDao()");
        if (unsuccessfulAuditDao == null) unsuccessfulAuditDao = getDao(UnsuccessfulAudit.class);
        return unsuccessfulAuditDao;
    }

    @Override
    public void close() {
        log.debug("close()");
        this.storeInfoDao = null;
        this.additionalSkuDao = null;
        this.fixedDisplaySkuDao = null;
        this.hotSpotDao = null;
        this.hotSpotWithComplianceDao = null;
        this.hotSpotWithOralCareDao = null;
        this.hotSpotWithSkinCaresDao = null;
        this.mustHaveSkuDao = null;
        this.pointOfPurchaseDao = null;
        this.questionnaireDao = null;
        this.questionMetaDao = null;
        this.questionnaireMetaDao = null;
        this.questionSetDao = null;
        this.shopTypeDao = null;
        this.storeInfoDao = null;
        this.unsuccessfulAuditDao = null;
        this.waveDisplaySkuDao = null;
        super.close();
    }

    private void createTable(ConnectionSource connectionSource) throws SQLException {
        log.debug("createTable()");
        createTablesIfNotExists(connectionSource);
    }

    private void dropTable(ConnectionSource connectionSource, boolean ignoreError) throws SQLException {
        log.debug("dropTable()");
        dropAllTables(connectionSource, ignoreError);
    }

    private void createTablesIfNotExists(ConnectionSource connectionSource) throws SQLException {
        TableUtils.createTableIfNotExists(connectionSource, AdditionalSku.class);
        TableUtils.createTableIfNotExists(connectionSource, FixedDisplaySku.class);
        TableUtils.createTableIfNotExists(connectionSource, HotSpot.class);
        TableUtils.createTableIfNotExists(connectionSource, HotSpotWithCompliance.class);
        TableUtils.createTableIfNotExists(connectionSource, HotSpotWithOralCare.class);
        TableUtils.createTableIfNotExists(connectionSource, HotSpotWithSkinCare.class);
        TableUtils.createTableIfNotExists(connectionSource, MustHaveSku.class);
        TableUtils.createTableIfNotExists(connectionSource, PointOfPurchase.class);
        TableUtils.createTableIfNotExists(connectionSource, QuestionMeta.class);
        TableUtils.createTableIfNotExists(connectionSource, Questionnaire.class);
        TableUtils.createTableIfNotExists(connectionSource, QuestionnaireMeta.class);
        TableUtils.createTableIfNotExists(connectionSource, QuestionSet.class);
        TableUtils.createTableIfNotExists(connectionSource, ShopType.class);
        TableUtils.createTableIfNotExists(connectionSource, StoreInfo.class);
        TableUtils.createTableIfNotExists(connectionSource, UnsuccessfulAudit.class);
        TableUtils.createTableIfNotExists(connectionSource, WaveDisplaySku.class);
    }

    private void dropAllTables(ConnectionSource connectionSource, boolean ignoreError) throws SQLException {
        TableUtils.dropTable(connectionSource, AdditionalSku.class, ignoreError);
        TableUtils.dropTable(connectionSource, FixedDisplaySku.class, ignoreError);
        TableUtils.dropTable(connectionSource, HotSpot.class, ignoreError);
        TableUtils.dropTable(connectionSource, MustHaveSku.class, ignoreError);
        TableUtils.dropTable(connectionSource, PointOfPurchase.class, ignoreError);
        TableUtils.dropTable(connectionSource, StoreInfo.class, ignoreError);
        TableUtils.dropTable(connectionSource, WaveDisplaySku.class, ignoreError);
        TableUtils.dropTable(connectionSource, UnsuccessfulAudit.class, ignoreError);
        TableUtils.dropTable(connectionSource, Questionnaire.class, ignoreError);
        TableUtils.dropTable(connectionSource, QuestionMeta.class, ignoreError);
        TableUtils.dropTable(connectionSource, QuestionSet.class, ignoreError);
        TableUtils.dropTable(connectionSource, ShopType.class, ignoreError);
        TableUtils.dropTable(connectionSource, QuestionnaireMeta.class, ignoreError);
        TableUtils.dropTable(connectionSource, HotSpotWithCompliance.class, ignoreError);
        TableUtils.dropTable(connectionSource, HotSpotWithOralCare.class, ignoreError);
        TableUtils.dropTable(connectionSource, HotSpotWithSkinCare.class, ignoreError);
    }
}