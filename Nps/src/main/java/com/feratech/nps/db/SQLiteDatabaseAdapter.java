package com.feratech.nps.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import com.feratech.nps.utils.Constants;
import com.feratech.nps.utils.Logger;

import java.io.*;

/**
 * User: Bazlur Rahman Rokon
 * Date: 6/15/13 - 9:19 PM
 */
public class SQLiteDatabaseAdapter extends SQLiteOpenHelper {
    private static final Logger log = Logger.getLogger(SQLiteDatabaseAdapter.class);

    private static SQLiteDatabase sqliteDb;
    private static SQLiteDatabaseAdapter instance;
    private static final int DATABASE_VERSION = Constants.DATABASE_VERSION;
    // the default database path is :
    // /data/data/pkgNameOfYourApplication/databases/
    private static String DB_PATH_PREFIX = "/data/data/";
    private static String DB_PATH_SUFFIX = "/databases/";
    private static final String TAG = "feratech";
    private Context context;

    /**
     * Contructor
     *
     * @param context : app context
     * @param name    : database name
     * @param factory : cursor Factory
     * @param version : DB version
     */
    private SQLiteDatabaseAdapter(Context context, String name,
                                  SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
        this.context = context;
        log.info("Create or Open database : ={}", name);
    }

    /**
     * Initialize method
     *
     * @param context      : application context
     * @param databaseName : database name
     */
    private static void initialize(Context context, String databaseName) {
        log.debug("initialize{}");
        if (instance == null) {
            /**
             * Try to check if there is an Original copy of DB in asset
             * Directory
             */
            if (!checkDatabase(context, databaseName)) {
                // if not exists, I try to copy from asset dir
                try {
                    copyDataBase(context, databaseName);
                } catch (IOException e) {
                    log.error("Database {} does not exists and there is no Original Version in Asset dir", databaseName, e);
                }
            }

            log.info("Try to create instance of database ( {} )", databaseName);
            instance = new SQLiteDatabaseAdapter(context, databaseName,
                    null, DATABASE_VERSION);
            sqliteDb = instance.getWritableDatabase();
            log.info("instance of database (\" {} \") created !", databaseName);
        }
    }

    /**
     * Static method for getting singleton instance
     *
     * @param context      : application context
     * @param databaseName : database name
     * @return : singleton instance
     */
    public static final SQLiteDatabaseAdapter getInstance(
            Context context, String databaseName) {
        log.debug("getInstance()- context - databaseName ={}", databaseName);
        initialize(context, databaseName);
        return instance;
    }

    /**
     * Method to get database instance
     *
     * @return database instance
     */
    public SQLiteDatabase getDatabase() {
        return sqliteDb;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d(TAG, "onCreate : nothing to do");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.d(TAG, "onCreate : nothing to do");

    }

    /**
     * Method for Copy the database from asset directory to application's data
     * directory
     *
     * @param databaseName : database name
     * @throws IOException : exception if file does not exists
     */
    private void copyDataBase(String databaseName) throws IOException {
        copyDataBase(context, databaseName);
    }

    /**
     * Static method for copy the database from asset directory to application's
     * data directory
     *
     * @param aContext     : application context
     * @param databaseName : database name
     * @throws IOException : exception if file does not exists
     */
    private static void copyDataBase(Context aContext, String databaseName)
            throws IOException {
        log.debug("copyDataBase() -> databaseName={}", databaseName);

        // Open your local db as the input stream
        InputStream myInput = aContext.getAssets().open(databaseName);

        // Path to the just created empty db
        String outFileName = getDatabasePath(aContext, databaseName);
        log.debug("Check if create dir : {}", (DB_PATH_PREFIX
                + aContext.getPackageName() + DB_PATH_SUFFIX));

        // if the path doesn't exist first, create it
        File f = new File(DB_PATH_PREFIX + aContext.getPackageName()
                + DB_PATH_SUFFIX);
        if (!f.exists())
            f.mkdir();

        log.info("Trying to copy local DB to : {}", outFileName);

        // Open the empty db as the output stream
        OutputStream myOutput = new FileOutputStream(outFileName);

        // transfer bytes from the inputfile to the outputfile
        byte[] buffer = new byte[1024];
        int length;
        while ((length = myInput.read(buffer)) > 0) {
            myOutput.write(buffer, 0, length);
        }

        // Close the streams
        myOutput.flush();
        myOutput.close();
        myInput.close();

        log.info("DB ({}) copied!", databaseName);
    }

    /**
     * Method to check if database exists in application's data directory
     *
     * @param databaseName : database name
     * @return : boolean (true if exists)
     */
    public boolean checkDatabase(String databaseName) {
        log.debug("checkDatabase() databaseName={}", databaseName);
        return checkDatabase(context, databaseName);
    }

    /**
     * Static Method to check if database exists in application's data directory
     *
     * @param aContext     : application context
     * @param databaseName : database name
     * @return : boolean (true if exists)
     */
    public static boolean checkDatabase(Context aContext, String databaseName) {
        log.debug("checkDatabase() databaseName ={}", databaseName);
        SQLiteDatabase checkDB = null;

        try {
            String myPath = getDatabasePath(aContext, databaseName);

            log.info("Trying to conntect to : {}", myPath);
            checkDB = SQLiteDatabase.openDatabase(myPath, null,
                    SQLiteDatabase.OPEN_READONLY);
            log.info("Database {} found!", databaseName);
            checkDB.close();
        } catch (SQLiteException e) {
            log.info("Database {} does not exists!", databaseName);
        }

        return checkDB != null ? true : false;
    }

    /**
     * Method that returns database path in the application's data directory
     *
     * @param databaseName : database name
     * @return : complete path
     */
    private String getDatabasePath(String databaseName) {
        log.debug("getDatabasePath()");
        return getDatabasePath(context, databaseName);
    }

    /**
     * Static Method that returns database path in the application's data
     * directory
     *
     * @param aContext     : application context
     * @param databaseName : database name
     * @return : complete path
     */
    private static String getDatabasePath(Context aContext, String databaseName) {
        log.debug("getDatabasePath={}", (DB_PATH_PREFIX + aContext.getPackageName() + DB_PATH_SUFFIX
                + databaseName));
        return DB_PATH_PREFIX + aContext.getPackageName() + DB_PATH_SUFFIX
                + databaseName;
    }
}