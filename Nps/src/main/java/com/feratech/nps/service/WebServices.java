package com.feratech.nps.service;

import android.content.Context;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import com.feratech.nps.R;
import com.feratech.nps.domain.DataVersion;
import com.feratech.nps.domain.Questionnaire;
import com.feratech.nps.domain.StoreInfo;
import com.feratech.nps.domain.json.QuestionnaireJsonModel;
import com.feratech.nps.error.ExceptionResolver;
import com.feratech.nps.handler.DefaultAsyncHttpResponseHandler;
import com.feratech.nps.handler.RequestStatus;
import com.feratech.nps.utils.Logger;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.PersistentCookieStore;
import com.loopj.android.http.RequestParams;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import static com.feratech.nps.helper.UrlResolver.resolve;

/**
 * User: Bazlur Rahman Rokon
 * Date: 7/30/13 - 5:28 AM
 */
@Singleton
public class WebServices {
    private static final Logger log = Logger.getLogger(WebServices.class);
    private static final int IMAGE_WIDTH_ACTUAL = 768;
    private static final int IMAGE_HEIGHT_ACTUAL = 1024;

    private AsyncHttpClient httpClient;
    private int DEFAULT_TIMEOUT = 3 * 600000;  //30 minute

    private Context context;

    @Inject
    public WebServices(Context context, AsyncHttpClient httpClient) {
        this.context = context;
        this.httpClient = httpClient;
        this.httpClient.setTimeout(DEFAULT_TIMEOUT);
        this.httpClient.setCookieStore(new PersistentCookieStore(context));
        this.httpClient.setUserAgent(getUserAgentHeader(context));
    }

    private String getUserAgentHeader(Context context) {
        return String.format("UPS Android/%s (%s %s; Android/%s %s)", getAppVersionName(context),
                Build.MANUFACTURER, Build.MODEL, Build.VERSION.RELEASE, Build.VERSION.CODENAME);
    }

    private String getAppVersionName(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            return null;
        }
    }

    public void retrieveStoreList(Handler handler) {
        httpClient.get(resolve(R.string.store_list_url), new DefaultAsyncHttpResponseHandler(handler, StoreInfo.class, List.class));
    }

    public void retrieveCurrentDataVersion(Handler handler) {
        httpClient.get(resolve(R.string.checkversion), new DefaultAsyncHttpResponseHandler(handler, DataVersion.class));
    }


    public void retrieveQuestionnaire(Handler handler) {
        httpClient.get(resolve(R.string.questionnaire_url), new DefaultAsyncHttpResponseHandler(handler, Questionnaire.class, List.class));
    }

    public void saveImage(File file, Handler handler) {
        try {
            RequestParams params = new RequestParams();
            try {
                params.put("store_image", file);
            } catch (FileNotFoundException e) {
                log.error("Image file not found", e);
            }
            httpClient.post(resolve(R.string.store_image_url), params, new DefaultAsyncHttpResponseHandler(handler));

        } catch (Exception e) {
            log.error("unable to upload images deu to errors", e);
        }
    }

    public void uploadImage(AsyncHttpResponseHandler handler, Uri firstImage) {
        log.verbose("uploadImage()");
        try {
            File myFile = new File(firstImage.getPath());
            RequestParams params = new RequestParams();
            try {
                params.put("store_image", myFile);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            httpClient.post(resolve(R.string.store_image_url), params, handler);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void uploadImage(AsyncHttpResponseHandler handler, File firstImage) {
        log.verbose("uploadImage()");
        try {
            RequestParams params = new RequestParams();
            try {
                params.put("store_image", firstImage);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            httpClient.post(resolve(R.string.store_image_url), params, handler);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void saveQuestionnaire(Questionnaire questionnaire, Handler handler) {
        log.debug("saveQuestionnaire(()");
        String questionnaireJsonString;
        try {
            questionnaireJsonString = (new ObjectMapper()).writeValueAsString(questionnaire);
        } catch (IOException e) {
            log.error("Json Parse Error", e);
            Message.obtain(handler, RequestStatus.FAILURE, ExceptionResolver.jsonParseError()).sendToTarget();
            return;
        }

        RequestParams params = new RequestParams();
        params.put("jsonObject", questionnaireJsonString);
        httpClient.post(resolve(R.string.questionnaire_url, ""), params, new DefaultAsyncHttpResponseHandler(handler));
    }

    public void saveQuestionnaire(List<QuestionnaireJsonModel> questionnaire, Handler handler) {
        log.debug("saveQuestionnaire(()");
        String questionnaireJsonString;
        try {
            questionnaireJsonString = (new ObjectMapper()).writeValueAsString(questionnaire);//gson.toJson(questionnaire);
        } catch (Exception e) {
            log.error("Json Parse Error", e);
            Message.obtain(handler, RequestStatus.FAILURE, ExceptionResolver.jsonParseError()).sendToTarget();
            return;
        }

        RequestParams params = new RequestParams();
        params.put("jsonObject", questionnaireJsonString);
        httpClient.post(resolve(R.string.questionnaire_url, ""), params, new DefaultAsyncHttpResponseHandler(handler));
    }


    /**
     *
     * */
    public void uploadQuestionnaire(QuestionnaireJsonModel questionnaireJsonModel, String shopType, Handler handler) {
        log.debug("saveQuestionnaire() shopType={}", shopType);
        String questionnaireJsonString;
        try {

            questionnaireJsonString = (new ObjectMapper()).writeValueAsString(questionnaireJsonModel);
        } catch (IOException e) {
            log.error("Json Parse Error", e);
            Message.obtain(handler, RequestStatus.FAILURE, ExceptionResolver.jsonParseError()).sendToTarget();
            return;
        }

        RequestParams params = new RequestParams();
        params.put("jsonObject", questionnaireJsonString);
        {
            if (shopType.contains("RCS_Blue")
                    || shopType.contains("RCS_Green")
                    || shopType.contains("RCS_Purple")
                    || shopType.contains("UCS_Blue")
                    || shopType.contains("UCS_Green")
                    ) {
                params.put("key", HotSpotKey.HOTSPOT_WITH_SKIN_CARE);
            } else if (shopType.contains("UNG")
                    || shopType.contains("RNG")
                    || shopType.contains("RWMG")
                    || shopType.contains("UWMG")
                    ) {
                params.put("key", HotSpotKey.HOTSPOT_WITH_COMPLIENCE);
            } else if (shopType.contains("UGS_Green")
                    || shopType.contains("UGS_Purple")) {
                params.put("key", HotSpotKey.HOTSPOT_WITH_ORAL_CARE);
            }
        }

        httpClient.post(resolve(R.string.upload_questionnaire), params, new DefaultAsyncHttpResponseHandler(handler));
    }

    public void uploadQuestionnaire(Questionnaire questionnaire, AsyncHttpResponseHandler handler) {

        String ispDataJsonString;
        try {
            ispDataJsonString = (new ObjectMapper()).writeValueAsString(questionnaire);
        } catch (IOException e) {
            log.error("Unable to upload file");
            return;
        }

        RequestParams params = new RequestParams();
        params.put("jsonObject", ispDataJsonString);
        httpClient.post(resolve(R.string.questionnaire_url), params, (handler));
    }
}
