package com.feratech.nps.service;

/**
 * User: Bazlur Rahman Rokon
 * Date: 8/18/13 - 12:26 AM
 */
public interface HotSpotKey {
    public static String HOTSPOT_WITH_COMPLIENCE = "1";
    public static String HOTSPOT_WITH_SKIN_CARE = "2";
    public static String HOTSPOT_WITH_ORAL_CARE = "3";
}
