package com.feratech.nps.service;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.*;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import com.feratech.nps.R;
import com.feratech.nps.handler.RequestStatus;
import com.feratech.nps.utils.Logger;
import com.feratech.nps.utils.StringUtils;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

/**
 * User: Bazlur Rahman Rokon
 * Date: 7/7/13 - 2:15 AM
 */
@Singleton
public class LocationService {
    private static final Logger log = Logger.getLogger(LocationService.class);

    private static final int MAX_ADDRESSES_FROM_REVERSE_GEOCODING = 5;
    /**
     * The value set in this constant is empirical. The first few location updates usually provide less accurate info.
     */
    private static final int MAX_NO_OF_LOCATION_UPDATES = 10;

    @Inject
    private LocationManager locationManager;

    private LocationListener locationListener;
    private Location location;
    private int noOfTimesLocationUpdated;
    private boolean dontShowLocationPrompt;

    public LocationService() {
        log.info("on Constructor");
        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location newLocation) {
                if (newLocation != null) {
                    log.info("onLocationChanged. longitude={}, latitude={}, provider={}",
                            newLocation.getLongitude(), newLocation.getLatitude(), newLocation.getProvider());

                    noOfTimesLocationUpdated++;

                    if (isBetterLocation(newLocation, location)) {
                        log.info("Updated location. noOfTimesLocationUpdated={}", noOfTimesLocationUpdated);

                        location = newLocation;

                        if (noOfTimesLocationUpdated >= MAX_NO_OF_LOCATION_UPDATES) {
                            stopListeningForLocationUpdates();
                        }
                    } else {
                        log.info("Discarded new location for not being better from current best location.");
                    }
                }
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {
                //ignore
            }

            @Override
            public void onProviderEnabled(String provider) {
                //ignore
            }

            @Override
            public void onProviderDisabled(String provider) {
                //ignore
            }
        };
    }

    /**
     * Determines whether one Location reading is better than the current Location fix
     *
     * @param newLocation         The new Location that you want to evaluate
     * @param currentBestLocation The current Location fix, to which you want to compare the new one
     */
    private boolean isBetterLocation(Location newLocation, Location currentBestLocation) {
        if (currentBestLocation == null) {
            // A new location is always better than no location
            return true;
        }

        // Check whether the new location fix is newer or older
        long timeDelta = newLocation.getTime() - currentBestLocation.getTime();
        boolean significantlyNewer = timeDelta > 2 * 60 * 1000;   // 2 minutes
        boolean significantlyOlder = timeDelta < -(2 * 60 * 1000);
        boolean newer = timeDelta > 0;

        // If it's been more than two minutes since the current location, use the new location
        // because the user has likely moved
        if (significantlyNewer) {
            return true;
            // If the new location is more than two minutes older, it must be worse
        } else if (significantlyOlder) {
            return false;
        }

        // Check whether the new location fix is more or less accurate
        int accuracyDelta = (int) (newLocation.getAccuracy() - currentBestLocation.getAccuracy());
        boolean lessAccurate = accuracyDelta > 0;
        boolean moreAccurate = accuracyDelta < 0;
        boolean significantlyLessAccurate = accuracyDelta > 200;    // 200 meters

        // Check if the old and new locations are from the same provider
        boolean fromSameProvider = isSameProvider(newLocation.getProvider(), currentBestLocation.getProvider());

        // Determine newLocation quality using a combination of timeliness and accuracy
        if (moreAccurate) {
            return true;
        } else if (newer && !lessAccurate) {
            return true;
        } else if (newer && !significantlyLessAccurate && fromSameProvider) {
            return true;
        }

        return false;
    }

    /**
     * Checks whether two providers are the same
     */
    private boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null) {
            return provider2 == null;
        }

        return provider1.equals(provider2);
    }

    public void startListeningForLocationUpdates(Context context) {
        log.verbose("startListeningForLocationUpdates, dontShowLocationPrompt={}, location={}", dontShowLocationPrompt, location);

        promptForLocationServiceActivation(context);

        noOfTimesLocationUpdated = 0;   // reset count

        log.info("Starting listening for location updates.");

        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
    }

    private boolean needsToPromptUserForLocationServiceActivation() {
        return !dontShowLocationPrompt && location == null && (isGPSProviderDisabled() || isNetworkProviderDisabled());
    }

    public boolean isGPSProviderDisabled() {
        return !locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    public boolean isNetworkProviderDisabled() {
        return !locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    public void promptForLocationServiceActivation(final Context context) {
        if (!needsToPromptUserForLocationServiceActivation()) {
            return;
        }

        View turnOnLocationDialogView =
                ((LayoutInflater) (new ContextThemeWrapper(context, R.style.Theme_Feratech))
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE))
                        .inflate(R.layout.location_alert_dailog, null);

        CheckBox neverShowThisAgainCheckBox =
                (CheckBox) turnOnLocationDialogView.findViewById(R.id.never_show_checkbox);
        neverShowThisAgainCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                dontShowLocationPrompt = isChecked;
            }
        });

        new AlertDialog.Builder(context)
                .setTitle(R.string.turn_on_location_service_title)
                .setView(turnOnLocationDialogView)
                .setIcon(android.R.drawable.ic_dialog_info)
                .setPositiveButton(R.string.go_to_location_settings, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        context.startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton(R.string.skip, null)
                .create()
                .show();
    }

    public void stopListeningForLocationUpdates() {
        log.info("Stopping listening for location updates.");

        locationManager.removeUpdates(locationListener);
    }

    public Location getLocation() {
        return location;
    }

    public void doReverseGeoCoding(Handler handler, Context context) {
        if (location == null) {
            Message.obtain(handler, RequestStatus.FAILURE).sendToTarget();

            return;
        }

        stopListeningForLocationUpdates();

        (new ReverseGeoCodingTask(context, handler)).execute(location);
    }

    public void cleanup() {
        log.verbose("cleanup");

        location = null;
        dontShowLocationPrompt = false;
    }


    private class ReverseGeoCodingTask extends AsyncTask<Location, Void, Void> {
        private Context context;
        private Handler handler;
        private ProgressDialog dialog;

        private ReverseGeoCodingTask(Context context, Handler handler) {
            super();
            this.context = context;
            this.handler = handler;
        }

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(context);
            dialog.setIndeterminate(true);
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            dialog.dismiss();
        }

        @Override
        protected Void doInBackground(Location... locations) {
            Location location = locations[0];

            Geocoder geocoder = new Geocoder(context, Locale.getDefault());
            List<Address> addressList = null;

            try {
                addressList = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(),
                        MAX_ADDRESSES_FROM_REVERSE_GEOCODING);
            } catch (IOException e) {
                log.warn("Exception on reverseGeoCoding.", e);
            }

            if (addressList != null && addressList.size() > 0) {
                Set<String> readableAddresses = new LinkedHashSet<String>();
                for (Address address : addressList) {
                    readableAddresses.add(convertToReadableText(address));
                }

                Message.obtain(handler, RequestStatus.SUCCESS,
                        readableAddresses.toArray(new String[readableAddresses.size()])).sendToTarget();
            } else {
                log.warn("No addresses found by ReverseGeoCoding, lat={}, long={}", location.getLatitude(), location.getLongitude());

                Message.obtain(handler, RequestStatus.FAILURE).sendToTarget();
            }

            return null;
        }

        private String convertToReadableText(Address address) {
            String[] addressLines = new String[address.getMaxAddressLineIndex() + 1];

            for (int i = 0; i < addressLines.length; i++) {
                addressLines[i] = address.getAddressLine(i);
            }

            return StringUtils.join(addressLines);
        }
    }

}
