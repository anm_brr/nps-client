package com.feratech.nps.service;

import android.content.Context;
import android.content.Intent;
import android.widget.Toast;
import com.feratech.nps.utils.NetworkUtil;
import roboguice.receiver.RoboBroadcastReceiver;

/**
 * User: Bazlur Rahman Rokon
 * Date: 8/7/13 - 8:36 AM
 */
public class NetworkChangeReceiver extends RoboBroadcastReceiver {

    @Override
    protected void handleReceive(Context context, Intent intent) {
        super.handleReceive(context, intent);
        String status = NetworkUtil.getConnectivityStatusString(context);
        if (NetworkUtil.isConnected(context)) {
            //start uploading questionnaire here
            //Intent service = new Intent(context, QuestionnaireUploadService.class);
            //context.startService(service);
        }
        Toast.makeText(context, status, Toast.LENGTH_LONG).show();
    }
}
