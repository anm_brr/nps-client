package com.feratech.nps.service;

import android.content.Context;
import com.feratech.nps.utils.Logger;
import com.google.inject.Singleton;

import javax.inject.Inject;

/**
 * User: Bazlur Rahman Rokon
 * Date: 9/29/13 - 6:57 PM
 */
@Singleton
public class VolleyWebService {
    private static final Logger log = Logger.getLogger(WebServices.class);

    private Context context;

    @Inject
    public VolleyWebService(Context context) {
        this.context = context;
    }

}
