package com.feratech.nps.service;

import android.content.Intent;
import android.os.Message;
import android.widget.Toast;
import com.feratech.nps.activity.QuestionnaireListActivity;
import com.feratech.nps.dao.QuestionnaireDao;
import com.feratech.nps.domain.Questionnaire;
import com.feratech.nps.domain.converter.ModelConverter;
import com.feratech.nps.domain.json.QuestionnaireJsonModel;
import com.feratech.nps.error.ErrorDetails;
import com.feratech.nps.handler.DefaultAsyncHandler;
import com.feratech.nps.utils.Logger;
import com.google.inject.Inject;
import roboguice.service.RoboIntentService;

import java.util.ArrayList;
import java.util.List;

/**
 * User: Bazlur Rahman Rokon
 * Date: 8/7/13 - 8:27 AM
 */
public class QuestionnaireUploadService extends RoboIntentService {
    private static final Logger log = Logger.getLogger(QuestionnaireUploadService.class);
    @Inject
    private QuestionnaireDao questionnaireDao;

    @Inject
    private WebServices webServices;

    public QuestionnaireUploadService(String name) {
        super(name);
    }

    public QuestionnaireUploadService() {
        super("QuestionnaireUploadService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        uploadOneByOne();
    }

    private void UploadAllAtOnce() {
        log.debug("onHandleIntent()");
        final List<Questionnaire> list = questionnaireDao.getSavedQuestionnaire(QuestionnaireListActivity.MENU_ITEM_NOT_UPLOADED);

        List<QuestionnaireJsonModel> questionnaireJsonModels = convert(list);

        if (list != null && list.size() > 0) {

            webServices.saveQuestionnaire(questionnaireJsonModels, new DefaultAsyncHandler() {
                @Override
                public void onSuccess(Message msg) {
                    Toast.makeText(getApplicationContext(), "All Questionnaire uploaded", Toast.LENGTH_LONG).show();
                    changeFlag(list);
                    log.debug("Upload Completed, stopping service ");
                    stopSelf();
                }

                @Override
                public void onFailure(ErrorDetails errorDetails) {
                    log.debug("Something went wrong, so stopping services..");
                    stopSelf();
                }
            });
        } else {
            log.debug("Nothing found to upload..stopping self");
            stopSelf();
        }
    }

    private void changeFlag(List<Questionnaire> questionnaireList) {
        log.debug("changeFlag()");
        for (Questionnaire questionnaire : questionnaireList) {
            questionnaire.setSynced(true);
            questionnaireDao.updateQuestionnaire(questionnaire);
        }
    }

    public void uploadOneByOne() {
        final List<Questionnaire> list = questionnaireDao.getSavedQuestionnaire(QuestionnaireListActivity.MENU_ITEM_NOT_UPLOADED);
        List<QuestionnaireJsonModel> questionnaireJsonModels = convert(list);
        if (questionnaireJsonModels != null && questionnaireJsonModels.size() > 0) {
            for (QuestionnaireJsonModel model : questionnaireJsonModels) {
              //  uploadQuestionnaires(model);
            }
        }
    }

    private List<QuestionnaireJsonModel> convert(List<Questionnaire> list) {

        List<QuestionnaireJsonModel> questionnaireJsonModels = new ArrayList<QuestionnaireJsonModel>();
        QuestionnaireJsonModel jsonModel;

        for (Questionnaire questionnaire : list) {
            jsonModel = new QuestionnaireJsonModel();
            jsonModel.setLongitude(questionnaire.getLongitude());
            jsonModel.setLatitude(questionnaire.getLatitude());
            jsonModel.setCellPhone(questionnaire.getCellPhone());
            jsonModel.setDateTime(questionnaire.getDateTime());
            jsonModel.setDmsCode(questionnaire.getDmsCode());
            jsonModel.setNameOfShopOwnerOrSalesMan(questionnaire.getNameOfShopOwnerOrSalesMan());

            jsonModel.setAditionalSkus(ModelConverter.convertAdditionalSkuToJson(questionnaire.getAditionalSkus()));
            jsonModel.setMustHaveSkus(ModelConverter.convertMustHaveSkuToJson(questionnaire.getMustHaveSkus()));
            jsonModel.setWaveDisplays(ModelConverter.convertWaveDisplaySkuToJson(questionnaire.getWaveDisplays()));
            jsonModel.setPointOfPurchases(ModelConverter.convertPointOfPurchaseToJson(questionnaire.getPointOfPurchases()));
            jsonModel.setFixedDisplaySkus(ModelConverter.convertFixedDisplaySkuToJsons(questionnaire.getFixedDisplaySkus()));
            jsonModel.setUnsuccessfulAudits(ModelConverter.convertUnsuccessfulAuditToJson(questionnaire.getUnsuccessfulAudits()));

            questionnaireJsonModels.add(jsonModel);
        }
        return questionnaireJsonModels;
    }


//    private void uploadQuestionnaires(QuestionnaireJsonModel json) {
//        webServices.uploadQuestionnaire(json, new DefaultAsyncHandler() {
//            @Override
//            public void onSuccess(Message msg) {
//                log.verbose("upload successful");
//                Toast.makeText(QuestionnaireUploadService.this, "upload successful 1 questionnaire", Toast.LENGTH_LONG).show();
//            }
//
//            @Override
//            public void onFailure(ErrorDetails errorDetails) {
//                String errorMessage = "Error: (" + errorDetails.getErrorCode() + ") : " + errorDetails.getErrorMsg();
//                Toast.makeText(QuestionnaireUploadService.this, errorMessage, Toast.LENGTH_LONG).show();
//            }
//        });
//
//    }
}
