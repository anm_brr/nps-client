/*
 * ******************************************************
 *  * Copyright (C) 2013 A. N. M. Bazlur Rahman <anmbrr.bit0112@gmail.com>
 *  *
 *  * This file is part of perfect-store.
 *  *
 *  * perfect-store can not be copied and/or distributed without the express
 *  * permission of {A. N. M. Bazlur Rahman}
 *  ******************************************************
 */

package com.feratech.nps.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import com.feratech.nps.R;
import com.feratech.nps.domain.HotSpot;
import com.feratech.nps.domain.HotSpotWithCompliance;

import java.util.List;

/**
 * User: Bazlur Rahman Rokon
 * Date: 6/16/13 - 6:18 AM
 */
public class HotSpotWithComplianceAdapter extends ArrayAdapter<HotSpotWithCompliance> {
    private Context context;
    private int resourceId;
    private List<HotSpotWithCompliance> hotSpotList;

    public HotSpotWithComplianceAdapter(Context context, int textViewResourceId, List<HotSpotWithCompliance> objects) {
        super(context, textViewResourceId, objects);
        this.context = context;
        this.resourceId = textViewResourceId;
        this.hotSpotList = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ViewHolder holder;

        if (convertView == null) {
            convertView = li.inflate(this.resourceId, null);
            holder = new ViewHolder();
            holder.tvHead = (TextView) convertView.findViewById(R.id.tv_head);
            holder.tvDescription = (TextView) convertView.findViewById(R.id.tv_description);
            holder.spnrCompliance = (Spinner) convertView.findViewById(R.id.spn_compliance);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        HotSpot hotSpot = getItem(position);

        holder.tvHead.setText(hotSpot.getHead());
        holder.tvDescription.setText(hotSpot.getDescription());

        return convertView;
    }

    class ViewHolder {
        public TextView tvHead;
        private TextView tvDescription;
        private Spinner spnrCompliance;
    }
}
