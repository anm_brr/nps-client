/*
 * ******************************************************
 *  * Copyright (C) 2013 A. N. M. Bazlur Rahman <anmbrr.bit0112@gmail.com>
 *  *
 *  * This file is part of perfect-store.
 *  *
 *  * perfect-store can not be copied and/or distributed without the express
 *  * permission of {A. N. M. Bazlur Rahman}
 *  ******************************************************
 */

package com.feratech.nps.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import com.feratech.nps.R;
import com.feratech.nps.domain.PointOfPurchase;

import java.util.List;

/**
 * User: Bazlur Rahman Rokon
 * Date: 6/2/13 - 12:49 PM
 */
public class PointOfPurchaseAdapter extends ArrayAdapter<PointOfPurchase> {
    private Context context;
    private int resourceId;
    private List<PointOfPurchase> list;


    public PointOfPurchaseAdapter(Context context, int resourceId, List<PointOfPurchase> objects) {
        super(context, resourceId, objects);
        this.context = context;
        this.resourceId = resourceId;
        this.list = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ViewHolder viewHolder;

        if (convertView == null) {
            convertView = li.inflate(resourceId, null);

            viewHolder = new ViewHolder();
            viewHolder.name = (TextView) convertView.findViewById(R.id.tv_name);
            viewHolder.description = (TextView) convertView.findViewById(R.id.tv_description);
            viewHolder.spnrAvailability = (Spinner) convertView.findViewById(R.id.spnr_availability);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        PointOfPurchase pointOfPurchase = getItem(position);
        viewHolder.name.setText(pointOfPurchase.getName());
        viewHolder.description.setText(pointOfPurchase.getDescription());

        return convertView;
    }

    class ViewHolder {
        TextView name;
        TextView description;
        Spinner spnrAvailability;
    }
}
