/*
 * ******************************************************
 *  * Copyright (C) 2013 A. N. M. Bazlur Rahman <anmbrr.bit0112@gmail.com>
 *  *
 *  * This file is part of perfect-store.
 *  *
 *  * perfect-store can not be copied and/or distributed without the express
 *  * permission of {A. N. M. Bazlur Rahman}
 *  ******************************************************
 */

package com.feratech.nps.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;
import com.feratech.nps.R;
import com.feratech.nps.domain.AdditionalSku;
import com.feratech.nps.utils.Logger;

import java.util.List;

/**
 * User: Bazlur Rahman Rokon
 * Date: 6/4/13 - 3:04 AM
 */
public class AdditionalSkuAdapter extends ArrayAdapter<AdditionalSku> {
    private static final Logger log = Logger.getLogger(AdditionalSkuAdapter.class);

    private Context context;
    private int resourceId;
    private List<AdditionalSku> mustHaveSkuList;

    public AdditionalSkuAdapter(Context context, int resourceId, List<AdditionalSku> objects) {
        super(context, resourceId, objects);
        this.context = context;
        this.resourceId = resourceId;
        this.mustHaveSkuList = objects;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final MustHaveSkuViewHolder viewHolder;
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(
                Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {

            convertView = li.inflate(R.layout.must_have_sku_single_item, null);

            viewHolder = new MustHaveSkuViewHolder();
            viewHolder.skuName = (TextView) convertView.findViewById(R.id.tv_sku);
            viewHolder.skuCode = (TextView) convertView.findViewById(R.id.tv_sku_code);
            viewHolder.skuCount = (EditText) convertView.findViewById(R.id.et_sku_count);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (MustHaveSkuViewHolder) convertView.getTag();
        }

        AdditionalSku additionalSku = mustHaveSkuList.get(position);
        log.debug("getView() ,position={}, mustHaveSku={}", position, additionalSku);

        viewHolder.skuName.setText(additionalSku.getSkuName());
        viewHolder.skuCode.setText(additionalSku.getSkuCode());
        viewHolder.skuCount.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (!hasFocus) {
                    int value = 0;
                    final EditText skuCount = (EditText) view;
                    try {
                        String count = skuCount.getText().toString();
                        value = Integer.parseInt(count);
                    } catch (NumberFormatException ignored) {

                    } finally {
                        getItem(position).setSkuCount(value);
                    }
                }
            }
        });
        viewHolder.skuCount.setId(position);


        return convertView;
    }

    class MustHaveSkuViewHolder {
        TextView skuName;
        TextView skuCode;
        EditText skuCount;
    }
}
