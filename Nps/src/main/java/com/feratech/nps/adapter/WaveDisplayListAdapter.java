package com.feratech.nps.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import com.feratech.nps.R;
import com.feratech.nps.domain.WaveDisplaySku;

import java.util.List;

/**
 * User: Bazlur Rahman Rokon
 * Date: 7/11/13 - 1:52 AM
 */
public class WaveDisplayListAdapter extends ArrayAdapter<WaveDisplaySku> {
    private Context context;
    private int resourceId;
    private List<WaveDisplaySku> list;

    public WaveDisplayListAdapter(Context context, int resourceId, List<WaveDisplaySku> objects) {
        super(context, resourceId, objects);
        this.context = context;
        this.resourceId = resourceId;
        this.list = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        ViewHolder viewHolder;
        View rowView = convertView;
        if (rowView == null) {
            rowView = inflater.inflate(this.resourceId, null);

            viewHolder = new ViewHolder();

            viewHolder.skuName = (TextView) rowView.findViewById(R.id.sku);
            viewHolder.skuCode = (TextView) rowView.findViewById(R.id.skuCode);
            viewHolder.totalDisplayCount = (EditText) rowView.findViewById(R.id.et_total_display_count);
            viewHolder.sequence = (Spinner) rowView.findViewById(R.id.sequence);

            rowView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) rowView.getTag();
        }
        WaveDisplaySku waveDisplaySku = this.list.get(position);


        viewHolder.skuName.setText(waveDisplaySku.getSkuName());
        viewHolder.skuCode.setText(waveDisplaySku.getSkuCode());

        //viewHolder.totalDisplayCount.setText(stockKeepingUnit.getTotalDisplayCount());
        //viewHolder.faceUpCount.setText(stockKeepingUnit.getFaceUpCount());
        //viewHolder.sequence.setChecked(stockKeepingUnit.isSequence());

        return rowView;

    }

    class ViewHolder {
        TextView skuName;
        TextView skuCode;
        EditText totalDisplayCount;
        Spinner sequence;
    }
}
