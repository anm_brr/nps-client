/*
 * ******************************************************
 *  * Copyright (C) 2013 A. N. M. Bazlur Rahman <anmbrr.bit0112@gmail.com>
 *  *
 *  * This file is part of perfect-store.
 *  *
 *  * perfect-store can not be copied and/or distributed without the express
 *  * permission of {A. N. M. Bazlur Rahman}
 *  ******************************************************
 */

package com.feratech.nps.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import com.feratech.nps.R;
import com.feratech.nps.domain.HotSpotWithSkinCare;

import java.util.List;

/**
 * User: Bazlur Rahman Rokon
 * Date: 6/16/13 - 6:19 AM
 */
public class HotSpotWithSkinCareAdapter extends ArrayAdapter<HotSpotWithSkinCare> {
    private Context context;
    private int resourceId;
    private List<HotSpotWithSkinCare> hotSpotList;

    public HotSpotWithSkinCareAdapter(Context context, int textViewResourceId, List<HotSpotWithSkinCare> objects) {
        super(context, textViewResourceId, objects);
        this.context = context;
        this.resourceId = textViewResourceId;
        this.hotSpotList = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ViewHolder holder;


        convertView = li.inflate(this.resourceId, null);
        holder = new ViewHolder();
        holder.tvHead = (TextView) convertView.findViewById(R.id.tv_head);
        holder.tvDescription = (TextView) convertView.findViewById(R.id.tv_description);
        holder.skinCare = (Spinner) convertView.findViewById(R.id.spn_skin_care);
        holder.HairCare = (Spinner) convertView.findViewById(R.id.spn_hair_care);

        HotSpotWithSkinCare hotSpot = getItem(position);

        holder.tvHead.setText(hotSpot.getHead());
        holder.tvDescription.setText(hotSpot.getDescription());

        return convertView;
    }

    class ViewHolder {
        public TextView tvHead;
        private TextView tvDescription;
        private Spinner skinCare;
        private Spinner HairCare;
    }
}
