package com.feratech.nps.adapter;

import android.content.Context;
import android.text.InputFilter;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.*;
import com.feratech.nps.R;
import com.feratech.nps.domain.FixedDisplaySku;
import com.feratech.nps.utils.Logger;
import com.feratech.nps.utils.Utils;

import java.util.List;

/**
 * User: Bazlur Rahman Rokon
 * Date: 7/11/13 - 12:44 AM
 */
public class FixedDisplayListAdapter extends ArrayAdapter<List<FixedDisplaySku>> {
    private final Logger log = Logger.getLogger(FixedDisplayListAdapter.class);
    private Context context;
    private List<List<FixedDisplaySku>> fixedDisplaySkus;
    private int resourceId;

    public FixedDisplayListAdapter(Context context, int textViewResourceId, List<List<FixedDisplaySku>> objects) {
        super(context, textViewResourceId, objects);
        this.context = context;
        this.resourceId = textViewResourceId;
        this.fixedDisplaySkus = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = li.inflate(resourceId, null);
        generateGroupView(convertView, position);
        return convertView;
    }

    private void generateGroupView(View view, int position) {

        LinearLayout llSkuName = (LinearLayout) view.findViewById(R.id.sku_name);
        LinearLayout llSkuCode = (LinearLayout) view.findViewById(R.id.sku_code);
        LinearLayout llAvailability = (LinearLayout) view.findViewById(R.id.availability);
        LinearLayout llTotalDisplayCount = (LinearLayout) view.findViewById(R.id.total_display_count);
        LinearLayout llFaceUpDisplayCount = (LinearLayout) view.findViewById(R.id.total_face_up_count);
        LinearLayout llSSequence = (LinearLayout) view.findViewById(R.id.sequence);

        int availability = 0, sequence = 0;
        List<FixedDisplaySku> fixedDisplaySku = fixedDisplaySkus.get(position);

        int width = Utils.getWindowWidth(context);
        int skuNameWidth = (int) (width * 0.4);
        int skuCodeWidth = (int) (width * 0.1);
        int editTextWidth = (int) (width * 0.1);

        final LinearLayout.LayoutParams skuNameParams = new LinearLayout.LayoutParams(skuNameWidth, 40);
        final LinearLayout.LayoutParams skuCodeParams = new LinearLayout.LayoutParams(skuCodeWidth, 40);
        final LinearLayout.LayoutParams editTextParams = new LinearLayout.LayoutParams(editTextWidth, 40);

        int maxLength = 2;
        InputFilter[] inputFilters = new InputFilter[]{new InputFilter.LengthFilter(maxLength)};

        for (FixedDisplaySku f : fixedDisplaySku) {
            TextView skuName = new TextView(context);

            skuName.setTextSize(14);
            skuName.setText(f.getSkuName());
            skuName.setLayoutParams(skuNameParams);
            skuName.setGravity(Gravity.LEFT);
            llSkuName.addView(skuName);

            TextView skuCode = new TextView(context);
            skuCode.setLayoutParams(skuCodeParams);
            skuCode.setText(f.getSkuCode());
            skuCode.setTextSize(14);
            llSkuCode.addView(skuCode);

            EditText etTotalDisplayCount = new EditText(context);
            etTotalDisplayCount.setLayoutParams(editTextParams);
            etTotalDisplayCount.setTextSize(14);
            etTotalDisplayCount.setFilters(inputFilters);
            if (f.getTotalDisplayCount() > 0)
                etTotalDisplayCount.setText(String.valueOf(f.getTotalDisplayCount()));
            etTotalDisplayCount.setInputType(EditorInfo.TYPE_CLASS_NUMBER);
            llTotalDisplayCount.addView(etTotalDisplayCount);


            EditText totalFaceUpCount = new EditText(context);
            totalFaceUpCount.setTextSize(14);
            totalFaceUpCount.setLayoutParams(editTextParams);
            totalFaceUpCount.setFilters(inputFilters);
            if (f.getFaceUpCount() > 0)
                totalFaceUpCount.setText(String.valueOf(f.getFaceUpCount()));
            totalFaceUpCount.setInputType(EditorInfo.TYPE_CLASS_NUMBER);
            llFaceUpDisplayCount.addView(totalFaceUpCount);

            availability = f.isAvailability() ? 1 : 0;
            sequence = f.isSequence() ? 1 : 0;
        }

        Spinner spnrAvailability = (Spinner) llAvailability.findViewById(R.id.spinner);
        spnrAvailability.setSelection(availability);
        log.debug("availability ={} " + availability);

        Spinner spinnerSequence = (Spinner) llSSequence.findViewById(R.id.spinner);
        spinnerSequence.setSelection(sequence);
        log.debug("sequence ={} " + sequence);
    }
}