package com.feratech.nps.error;

import com.feratech.nps.R;
import com.feratech.nps.utils.Logger;
import com.google.inject.Inject;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpResponseException;
import org.apache.http.conn.ConnectTimeoutException;
import org.codehaus.jackson.map.ObjectMapper;
import roboguice.inject.InjectResource;

import java.io.IOException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Abdullah Al Mamun (Oronno)
 */
public class ExceptionResolver {
    private static final Logger log = Logger.getLogger(ExceptionResolver.class);

    private static final Map<Class<? extends Throwable>, Integer> internetConnectionExceptionCodeMap;
    private static final int ERROR_CODE_NOT_INTERNET_CONNECTION_EXCEPTION = -1;

    @Inject
    private static ObjectMapper objectMapper;

    @InjectResource(R.string.error_internet_connection)
    private static String internetConnectionErrorMsg;
    @InjectResource(R.string.error_session_expired)
    private static String sessionExpiredMsg;
    @InjectResource(R.string.error_validation)
    private static String validationErrorMsg;
    @InjectResource(R.string.error_insufficient_privilege)
    private static String insufficientPrivilegeErrorMsg;
    @InjectResource(R.string.error_incorrect_credential)
    private static String incorrectCredentialMsg;
    @InjectResource(R.string.error_resource_not_found)
    private static String resourceNotFoundMsg;
    @InjectResource(R.string.error_internal_server_error)
    private static String internalServerErrorMsg;
    @InjectResource(R.string.error_unknown)
    private static String unknownErrorMsg;

    static {
        internetConnectionExceptionCodeMap = new HashMap<Class<? extends Throwable>, Integer>();
        internetConnectionExceptionCodeMap.put(UnknownHostException.class, ErrorDetails.UNKNOWN_HOST_ERROR);
        internetConnectionExceptionCodeMap.put(SocketException.class, ErrorDetails.CONNECTION_ERROR);
        internetConnectionExceptionCodeMap.put(SocketTimeoutException.class, ErrorDetails.CONNECTION_TIMEOUT_ERROR);
        internetConnectionExceptionCodeMap.put(ConnectTimeoutException.class, ErrorDetails.CONNECTION_TIMEOUT_ERROR);
    }

    public static ErrorDetails getErrorDetails(Throwable exception, String content) {
        int errorCode = getErrorCode(exception);

        if (errorCode != ERROR_CODE_NOT_INTERNET_CONNECTION_EXCEPTION) {
            return internetConnectionError(errorCode);
        } else {
            if (isExceptionType(HttpResponseException.class, exception)) {
                try {
                    return errorDetailsFromErrorCode(content);
                } catch (IOException e) {
                    switch (((HttpResponseException) exception).getStatusCode()) {
                        case HttpStatus.SC_NOT_FOUND:
                            return new ErrorDetails(ErrorDetails.RESOURCE_NOT_FOUND, resourceNotFoundMsg);
                        case HttpStatus.SC_INTERNAL_SERVER_ERROR:
                            return new ErrorDetails(ErrorDetails.INTERNAL_SERVER_ERROR, internalServerErrorMsg);
                        default:
                            log.error("Error parsing json object.", e);
                            return jsonParseError();
                    }
                }
            } else {
                return new ErrorDetails(ErrorDetails.UNKNOWN_ERROR, unknownErrorMsg);
            }
        }
    }

    private static int getErrorCode(Throwable exception) {
        for (Class exceptionClass : internetConnectionExceptionCodeMap.keySet()) {
            if (isExceptionType(exceptionClass, exception)) {
                return internetConnectionExceptionCodeMap.get(exceptionClass);
            }
        }

        return ERROR_CODE_NOT_INTERNET_CONNECTION_EXCEPTION;
    }

    private static boolean isExceptionType(Class clazz, Throwable throwable) {
        return clazz.isInstance(throwable) || clazz.isInstance(throwable.getCause());
    }

    private static ErrorDetails internetConnectionError(int errorCode) {
        return new ErrorDetails(errorCode, internetConnectionErrorMsg);
    }

    private static ErrorDetails errorDetailsFromErrorCode(String content) throws IOException {
        ErrorDetails errorDetails = objectMapper.readValue(content, ErrorDetails.class);

        switch (errorDetails.getErrorCode()) {
            case ErrorDetails.UNAUTHORIZED:
                errorDetails.setErrorMsg(sessionExpiredMsg);
                break;
            case ErrorDetails.INVALID_CREDENTIAL:
                errorDetails.setErrorMsg(incorrectCredentialMsg);
                break;
            case ErrorDetails.VALIDATION_ERROR:
                errorDetails.setErrorMsg(validationErrorMsg);
                break;
            case ErrorDetails.INSUFFICIENT_PRIVILEGE:
                errorDetails.setErrorMsg(insufficientPrivilegeErrorMsg);
            default:
                return errorDetails;
        }

        return errorDetails;
    }

    public static ErrorDetails jsonParseError() {
        return new ErrorDetails(ErrorDetails.PARSE_ERROR, unknownErrorMsg);
    }
}
