package com.feratech.nps.error;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.Map;

/**
 * @author oronno
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ErrorDetails {
    /* Server-Side Error Codes */
    public static final int UNAUTHORIZED = 1;
    public static final int INVALID_CREDENTIAL = 2;
    public static final int NOT_LOGGED_IN = 3;
    public static final int INSUFFICIENT_PRIVILEGE = 4;
    public static final int INVALID_ARGUMENT = 5;
    public static final int VALIDATION_ERROR = 6;
    public static final int INTERNAL_SERVER_ERROR = 7;
    public static final int RESOURCE_NOT_FOUND = 8;

    /* Client-Side Custom Error Codes */
    public static final int UNKNOWN_ERROR = 1000;
    public static final int PARSE_ERROR = 1001;
    public static final int UNKNOWN_HOST_ERROR = 1002;
    public static final int CONNECTION_TIMEOUT_ERROR = 1003;
    public static final int CONNECTION_ERROR = 1004;

    private int errorCode;
    private String errorMsg;
    private Map<String, String> fieldErrors;
    private Map<Integer, String> generalErrors;

    public ErrorDetails() {
        //default constructor for jackson so that it can instantiate object of this class
    }

    public ErrorDetails(int errorCode, String errorMsg) {
        this.errorCode = errorCode;
        this.errorMsg = errorMsg;
    }

    @JsonProperty("error_code")
    public int getErrorCode() {
        return errorCode;
    }

    @JsonProperty("error_msg")
    public String getErrorMsg() {
        return errorMsg;
    }

    @JsonProperty("fields_error_details")
    public Map<String, String> getFieldErrors() {
        return fieldErrors;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public void setFieldErrors(Map<String, String> fieldErrors) {
        this.fieldErrors = fieldErrors;
    }

    @JsonProperty("general_error_details")
    public Map<Integer, String> getGeneralErrors() {
        return generalErrors;
    }

    public void setGeneralErrors(Map<Integer, String> generalErrors) {
        this.generalErrors = generalErrors;
    }

    @Override
    public String toString() {
        return "ErrorDetails{" +
                "errorCode=" + errorCode +
                ", errorMsg='" + errorMsg + '\'' +
                ", fieldErrors=" + fieldErrors +
                '}';
    }
}
