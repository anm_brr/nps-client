package com.feratech.nps.domain.json;

import java.io.Serializable;

/**
 * User: Bazlur Rahman Rokon
 * Date: 8/7/13 - 11:34 AM
 */
public class FixedDisplaySkuJson implements Serializable {
    private int id;
    private String skuCode;
    private int skuCount;
    private String skuName;
    private boolean availability;
    private int totalDisplayCount;
    private int faceUpCount;
    private boolean sequence;

    public FixedDisplaySkuJson(int id, String skuCode, int skuCount, String skuName, boolean availability, int totalDisplayCount, int faceUpCount, boolean sequence) {
        this.id = id;
        this.skuCode = skuCode;
        this.skuCount = skuCount;
        this.skuName = skuName;
        this.availability = availability;
        this.totalDisplayCount = totalDisplayCount;
        this.faceUpCount = faceUpCount;
        this.sequence = sequence;
    }

    public FixedDisplaySkuJson() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSkuCode() {
        return skuCode;
    }

    public void setSkuCode(String skuCode) {
        this.skuCode = skuCode;
    }

    public int getSkuCount() {
        return skuCount;
    }

    public void setSkuCount(int skuCount) {
        this.skuCount = skuCount;
    }

    public String getSkuName() {
        return skuName;
    }

    public void setSkuName(String skuName) {
        this.skuName = skuName;
    }

    public boolean isAvailability() {
        return availability;
    }

    public void setAvailability(boolean availability) {
        this.availability = availability;
    }

    public int getTotalDisplayCount() {
        return totalDisplayCount;
    }

    public void setTotalDisplayCount(int totalDisplayCount) {
        this.totalDisplayCount = totalDisplayCount;
    }

    public int getFaceUpCount() {
        return faceUpCount;
    }

    public void setFaceUpCount(int faceUpCount) {
        this.faceUpCount = faceUpCount;
    }

    public boolean isSequence() {
        return sequence;
    }

    public void setSequence(boolean sequence) {
        this.sequence = sequence;
    }
}
