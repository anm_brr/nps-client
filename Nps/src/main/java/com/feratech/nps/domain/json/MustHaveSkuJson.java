package com.feratech.nps.domain.json;

import java.io.Serializable;

/**
 * User: Bazlur Rahman Rokon
 * Date: 8/7/13 - 11:40 AM
 */
public class MustHaveSkuJson implements Serializable {
    private int id;
    private String skuName;
    private String skuCode;
    private int skuCount = -1;

    public MustHaveSkuJson(int id, String skuName, String skuCode, int skuCount) {
        this.id = id;
        this.skuName = skuName;
        this.skuCode = skuCode;
        this.skuCount = skuCount;
    }

    public MustHaveSkuJson() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSkuName() {
        return skuName;
    }

    public void setSkuName(String skuName) {
        this.skuName = skuName;
    }

    public String getSkuCode() {
        return skuCode;
    }

    public void setSkuCode(String skuCode) {
        this.skuCode = skuCode;
    }

    public int getSkuCount() {
        return skuCount;
    }

    public void setSkuCount(int skuCount) {
        this.skuCount = skuCount;
    }
}
