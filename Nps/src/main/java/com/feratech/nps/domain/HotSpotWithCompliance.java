/*
 * ******************************************************
 *  * Copyright (C) 2013 A. N. M. Bazlur Rahman <anmbrr.bit0112@gmail.com>
 *  *
 *  * This file is part of perfect-store.
 *  *
 *  * perfect-store can not be copied and/or distributed without the express
 *  * permission of {A. N. M. Bazlur Rahman}
 *  ******************************************************
 */

package com.feratech.nps.domain;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * User: Bazlur Rahman Rokon
 * Date: 6/14/13 - 11:26 PM
 */
@DatabaseTable
@JsonIgnoreProperties(ignoreUnknown = true)
public class HotSpotWithCompliance extends HotSpot {
    @DatabaseField
    public boolean compliance;

    public HotSpotWithCompliance(HotSpot hotSpot) {
        this.setHead(hotSpot.getHead());
        this.setDescription(hotSpot.getDescription());
    }

    public HotSpotWithCompliance() {
    }

    public boolean isCompliance() {
        return compliance;
    }

    public void setCompliance(boolean compliance) {
        this.compliance = compliance;
    }

    @Override
    public String toString() {
        return "HotSpotWithCompliance{" +
                "compliance=" + compliance +
                '}';
    }
}
