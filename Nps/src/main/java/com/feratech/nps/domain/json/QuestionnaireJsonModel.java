package com.feratech.nps.domain.json;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * User: Bazlur Rahman Rokon
 * Date: 8/7/13 - 10:50 AM
 */
@JsonAutoDetect
public class QuestionnaireJsonModel implements Serializable {
    private List<UnsuccessfulAuditJson> unsuccessfulAudits;
    private List<MustHaveSkuJson> mustHaveSkus;
    private List<FixedDisplaySkuJson> fixedDisplaySkus;
    private List<WaveDisplaySkuJson> waveDisplays;
    private List<? extends HotSpotJson> hotSpots;
    private List<PointOfPurchaseJson> pointOfPurchases;
    private List<AdditionalSkuJson> aditionalSkus;

    private String dmsCode;
    private String nameOfShopOwnerOrSalesMan;
    private String cellPhone;
    private String longitude;
    private String latitude;
    private Date dateTime;

    public List<UnsuccessfulAuditJson> getUnsuccessfulAudits() {
        return unsuccessfulAudits;
    }

    public void setUnsuccessfulAudits(List<UnsuccessfulAuditJson> unsuccessfulAudits) {
        this.unsuccessfulAudits = unsuccessfulAudits;
    }

    public List<MustHaveSkuJson> getMustHaveSkus() {
        return mustHaveSkus;
    }

    public void setMustHaveSkus(List<MustHaveSkuJson> mustHaveSkus) {
        this.mustHaveSkus = mustHaveSkus;
    }

    public List<FixedDisplaySkuJson> getFixedDisplaySkus() {
        return fixedDisplaySkus;
    }

    public void setFixedDisplaySkus(List<FixedDisplaySkuJson> fixedDisplaySkus) {
        this.fixedDisplaySkus = fixedDisplaySkus;
    }

    public List<WaveDisplaySkuJson> getWaveDisplays() {
        return waveDisplays;
    }

    public void setWaveDisplays(List<WaveDisplaySkuJson> waveDisplays) {
        this.waveDisplays = waveDisplays;
    }

    public List<? extends HotSpotJson> getHotSpots() {
        return hotSpots;
    }

    public void setHotSpots(List<? extends HotSpotJson> hotSpots) {
        this.hotSpots = hotSpots;
    }

    public List<AdditionalSkuJson> getAditionalSkus() {
        return aditionalSkus;
    }

    public void setAditionalSkus(List<AdditionalSkuJson> aditionalSkus) {
        this.aditionalSkus = aditionalSkus;
    }

    public List<PointOfPurchaseJson> getPointOfPurchases() {
        return pointOfPurchases;
    }

    public void setPointOfPurchases(List<PointOfPurchaseJson> pointOfPurchases) {
        this.pointOfPurchases = pointOfPurchases;
    }

    public String getDmsCode() {
        return dmsCode;
    }

    public void setDmsCode(String dmsCode) {
        this.dmsCode = dmsCode;
    }

    public String getNameOfShopOwnerOrSalesMan() {
        return nameOfShopOwnerOrSalesMan;
    }

    public void setNameOfShopOwnerOrSalesMan(String nameOfShopOwnerOrSalesMan) {
        this.nameOfShopOwnerOrSalesMan = nameOfShopOwnerOrSalesMan;
    }

    public String getCellPhone() {
        return cellPhone;
    }

    public void setCellPhone(String cellPhone) {
        this.cellPhone = cellPhone;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    @JsonSerialize(using=JsonDateSerializer.class)
    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("QuestionnaireJsonModel{");
        sb.append("unsuccessfulAudits=").append(unsuccessfulAudits);
        sb.append(", mustHaveSkus=").append(mustHaveSkus);
        sb.append(", fixedDisplaySkus=").append(fixedDisplaySkus);
        sb.append(", waveDisplays=").append(waveDisplays);
        sb.append(", hotSpots=").append(hotSpots);
        sb.append(", pointOfPurchases=").append(pointOfPurchases);
        sb.append(", aditionalSkus=").append(aditionalSkus);
        sb.append(", dmsCode='").append(dmsCode).append('\'');
        sb.append(", nameOfShopOwnerOrSalesMan='").append(nameOfShopOwnerOrSalesMan).append('\'');
        sb.append(", cellPhone='").append(cellPhone).append('\'');
        sb.append(", longitude='").append(longitude).append('\'');
        sb.append(", latitude='").append(latitude).append('\'');
        sb.append(", dateTime=").append(dateTime);
        sb.append('}');
        return sb.toString();
    }
}
