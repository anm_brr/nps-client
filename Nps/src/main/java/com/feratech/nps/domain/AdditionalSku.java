/*
 * ******************************************************
 *  * Copyright (C) 2013 A. N. M. Bazlur Rahman <anmbrr.bit0112@gmail.com>
 *  *
 *  * This file is part of perfect-store.
 *  *
 *  * perfect-store can not be copied and/or distributed without the express
 *  * permission of {A. N. M. Bazlur Rahman}
 *  ******************************************************
 */

package com.feratech.nps.domain;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.io.Serializable;

/**
 * User: Bazlur Rahman Rokon
 * Date: 5/31/13 - 12:10 AM
 */
@DatabaseTable
@JsonIgnoreProperties(ignoreUnknown = true)
public class AdditionalSku implements Serializable {
    @DatabaseField(generatedId = true)
    public int id;
    @DatabaseField
    public String skuName;
    @DatabaseField
    public String skuCode;
    @DatabaseField
    public int skuCount;
    @DatabaseField(foreign = true, foreignAutoCreate = true, foreignAutoRefresh = true)
    public ShopType shopType;
    @DatabaseField(foreign = true, foreignAutoCreate = true, foreignAutoRefresh = true)
    public Questionnaire questionnaire;


    @DatabaseField()
    private boolean rawData;

    public boolean isRawData() {
        return rawData;
    }

    public void setRawData(boolean rawData) {
        this.rawData = rawData;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Questionnaire getQuestionnaire() {
        return questionnaire;
    }

    public void setQuestionnaire(Questionnaire questionnaire) {
        this.questionnaire = questionnaire;
    }

    public ShopType getShopType() {
        return shopType;
    }

    public void setShopType(ShopType shopType) {
        this.shopType = shopType;
    }

    public String getSkuCode() {
        return skuCode;
    }

    public void setSkuCode(String skuCode) {
        this.skuCode = skuCode;
    }

    public int getSkuCount() {
        return skuCount;
    }

    public void setSkuCount(int skuCount) {
        this.skuCount = skuCount;
    }

    public String getSkuName() {
        return skuName;
    }

    public void setSkuName(String skuName) {
        this.skuName = skuName;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("AdditionalSkuJson{");
        sb.append("id=").append(id);
        sb.append(", skuName='").append(skuName).append('\'');
        sb.append(", skuCode='").append(skuCode).append('\'');
        sb.append(", skuCount=").append(skuCount);
        sb.append(", shopType=").append(shopType);
        sb.append(", questionnaire=").append(questionnaire);
        sb.append(", rawData=").append(rawData);
        sb.append('}');
        return sb.toString();
    }
}
