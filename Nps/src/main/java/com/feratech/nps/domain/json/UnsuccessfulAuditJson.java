package com.feratech.nps.domain.json;

import java.io.Serializable;

/**
 * User: Bazlur Rahman Rokon
 * Date: 8/7/13 - 11:42 AM
 */
public class UnsuccessfulAuditJson implements Serializable {
    private int id;
    private String reason;
    private int code;
    private String dealersCode;
    private String month;
    private String businessName;


    public UnsuccessfulAuditJson(int id, String reason, int code, String dealersCode, String month, String businessName) {
        this.id = id;
        this.reason = reason;
        this.code = code;
        this.dealersCode = dealersCode;
        this.month = month;
        this.businessName = businessName;
    }

    public UnsuccessfulAuditJson() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDealersCode() {
        return dealersCode;
    }

    public void setDealersCode(String dealersCode) {
        this.dealersCode = dealersCode;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }
}
