/*
 * ******************************************************
 *  * Copyright (C) 2013 A. N. M. Bazlur Rahman <anmbrr.bit0112@gmail.com>
 *  *
 *  * This file is part of perfect-store.
 *  *
 *  * perfect-store can not be copied and/or distributed without the express
 *  * permission of {A. N. M. Bazlur Rahman}
 *  ******************************************************
 */

package com.feratech.nps.domain;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.io.Serializable;

/**
 * User: Bazlur Rahman Rokon
 * Date: 6/14/13 - 4:48 AM
 */
@DatabaseTable
@JsonIgnoreProperties(ignoreUnknown = true)
public class UnsuccessfulAudit implements Serializable {
    @DatabaseField(generatedId = true)
    public int Id;
    @DatabaseField
    public String reason;
    @DatabaseField
    public int code;
    @DatabaseField
    public String dealersCode;
    @DatabaseField
    public String month;
    @DatabaseField
    public String businessName;
    @DatabaseField(foreign = true, canBeNull = true, foreignAutoCreate = true, foreignAutoRefresh = true, columnName = "questionnaire_id")
    public Questionnaire Questionnaire;

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDealersCode() {
        return dealersCode;
    }

    public void setDealersCode(String dealersCode) {
        this.dealersCode = dealersCode;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public Questionnaire getQuestionnaire() {
        return Questionnaire;
    }

    public void setQuestionnaire(Questionnaire questionnaire) {
        Questionnaire = questionnaire;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("UnsuccessfulAudit{");
        sb.append("Id=").append(Id);
        sb.append(", reason='").append(reason).append('\'');
        sb.append(", code=").append(code);
        sb.append(", dealersCode='").append(dealersCode).append('\'');
        sb.append(", month='").append(month).append('\'');
        sb.append(", businessName='").append(businessName).append('\'');
        sb.append(", Questionnaire=").append(Questionnaire);
        sb.append('}');
        return sb.toString();
    }
}
