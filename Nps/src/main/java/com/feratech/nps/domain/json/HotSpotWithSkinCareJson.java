package com.feratech.nps.domain.json;

/**
 * User: Bazlur Rahman Rokon
 * Date: 8/7/13 - 11:39 AM
 */
public class HotSpotWithSkinCareJson extends HotSpotJson {
    private boolean hairCare;
    private boolean skinCare;

    public HotSpotWithSkinCareJson(int id, String head, String description, boolean hairCare, boolean skinCare) {
        super(id, head, description);
        this.hairCare = hairCare;
        this.skinCare = skinCare;
    }

    public HotSpotWithSkinCareJson() {
    }

    public boolean isHairCare() {
        return hairCare;
    }

    public void setHairCare(boolean hairCare) {
        this.hairCare = hairCare;
    }

    public boolean isSkinCare() {
        return skinCare;
    }

    public void setSkinCare(boolean skinCare) {
        this.skinCare = skinCare;
    }
}
