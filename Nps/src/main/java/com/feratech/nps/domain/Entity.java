package com.feratech.nps.domain;

import com.j256.ormlite.field.DatabaseField;

import java.io.Serializable;

/**
 * User: Bazlur Rahman Rokon
 * Date: 8/3/13 - 4:22 AM
 */


public class Entity implements Serializable {
    @DatabaseField(generatedId = true)
    public int id;

}
