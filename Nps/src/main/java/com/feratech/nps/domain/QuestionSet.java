/*
 * ******************************************************
 *  * Copyright (C) 2013 A. N. M. Bazlur Rahman <anmbrr.bit0112@gmail.com>
 *  *
 *  * This file is part of perfect-store.
 *  *
 *  * perfect-store can not be copied and/or distributed without the express
 *  * permission of {A. N. M. Bazlur Rahman}
 *  ******************************************************
 */

package com.feratech.nps.domain;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.io.Serializable;

/**
 * User: Bazlur Rahman Rokon
 * Date: 6/14/13 - 5:01 AM
 */
@DatabaseTable
@JsonIgnoreProperties(ignoreUnknown = true)
public class QuestionSet implements Serializable {
    @DatabaseField(generatedId = true)
    public int id;
    @DatabaseField
    public int number;
    @DatabaseField
    public String set;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getSet() {
        return set;
    }

    public void setSet(String set) {
        this.set = set;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("QuestionSet{");
        sb.append("id=").append(id);
        sb.append(", number=").append(number);
        sb.append(", set='").append(set).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
