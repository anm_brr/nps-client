package com.feratech.nps.domain;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.io.Serializable;

/**
 * User: Bazlur Rahman Rokon
 * Date: 7/10/13 - 11:06 PM
 */
@DatabaseTable
@JsonIgnoreProperties(ignoreUnknown = true)
public class ShopType implements Serializable {
    @DatabaseField(generatedId = true)
    private int id;
    @DatabaseField
    private String type;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ShopType{");
        sb.append("id=").append(id);
        sb.append(", type='").append(type).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
