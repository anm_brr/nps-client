package com.feratech.nps.domain.json;

/**
 * User: Bazlur Rahman Rokon
 * Date: 8/7/13 - 11:37 AM
 */
public class HotSpotWithComplianceJson extends HotSpotJson {
    private boolean compliance;

    public HotSpotWithComplianceJson(int id, String head, String description, boolean compliance) {
        super(id, head, description);
        this.compliance = compliance;
    }

    public HotSpotWithComplianceJson() {
    }

    public boolean isCompliance() {
        return compliance;
    }

    public void setCompliance(boolean compliance) {
        this.compliance = compliance;
    }

}
