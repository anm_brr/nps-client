/*
 * ******************************************************
 *  * Copyright (C) 2013 A. N. M. Bazlur Rahman <anmbrr.bit0112@gmail.com>
 *  *
 *  * This file is part of perfect-store.
 *  *
 *  * perfect-store can not be copied and/or distributed without the express
 *  * permission of {A. N. M. Bazlur Rahman}
 *  ******************************************************
 */

package com.feratech.nps.domain;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.io.Serializable;

/**
 * User: Bazlur Rahman Rokon
 * Date: 6/14/13 - 4:44 AM
 */

@DatabaseTable
@JsonIgnoreProperties(ignoreUnknown = true)
public class QuestionnaireMeta implements Serializable {
    @DatabaseField(generatedId = true)
    private int id;
    @DatabaseField
    private int fixedDisplaySetSize;
    @DatabaseField
    private boolean fixedDisplayAllRequired;
    @DatabaseField
    private boolean waveDisplayAllRequired;
    @DatabaseField
    private int waveDisplayQuestionSize;
    @DatabaseField(foreign = true, foreignAutoCreate = true, foreignAutoRefresh = true)
    private ShopType shopType;

    public boolean isFixedDisplayAllRequired() {
        return fixedDisplayAllRequired;
    }

    public void setFixedDisplayAllRequired(boolean fixedDisplayAllRequired) {
        this.fixedDisplayAllRequired = fixedDisplayAllRequired;
    }

    public int getFixedDisplaySetSize() {
        return fixedDisplaySetSize;
    }

    public void setFixedDisplaySetSize(int fixedDisplaySetSize) {
        this.fixedDisplaySetSize = fixedDisplaySetSize;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ShopType getShopType() {
        return shopType;
    }

    public void setShopType(ShopType shopType) {
        this.shopType = shopType;
    }

    public boolean isWaveDisplayAllRequired() {
        return waveDisplayAllRequired;
    }

    public void setWaveDisplayAllRequired(boolean waveDisplayAllRequired) {
        this.waveDisplayAllRequired = waveDisplayAllRequired;
    }

    public int getWaveDisplayQuestionSize() {
        return waveDisplayQuestionSize;
    }

    public void setWaveDisplayQuestionSize(int waveDisplayQuestionSize) {
        this.waveDisplayQuestionSize = waveDisplayQuestionSize;
    }

    @Override
    public String toString() {
        return "QuestionnaireMeta{" +
                "fixedDisplayAllRequired=" + fixedDisplayAllRequired +
                ", id=" + id +
                ", fixedDisplaySetSize=" + fixedDisplaySetSize +
                ", waveDisplayAllRequired=" + waveDisplayAllRequired +
                ", waveDisplayQuestionSize=" + waveDisplayQuestionSize +
                ", shopType=" + shopType +
                '}' + "\n";
    }
}
