package com.feratech.nps.domain.json;

import java.io.Serializable;

/**
 * User: Bazlur Rahman Rokon
 * Date: 8/7/13 - 11:36 AM
 */
public class HotSpotJson implements Serializable {
    private int id;
    private String head;
    private String description;

    public HotSpotJson(int id, String head, String description) {
        this.id = id;
        this.head = head;
        this.description = description;
    }

    public HotSpotJson() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getHead() {
        return head;
    }

    public void setHead(String head) {
        this.head = head;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
