/*
 * ******************************************************
 *  * Copyright (C) 2013 A. N. M. Bazlur Rahman <anmbrr.bit0112@gmail.com>
 *  *
 *  * This file is part of perfect-store.
 *  *
 *  * perfect-store can not be copied and/or distributed without the express
 *  * permission of {A. N. M. Bazlur Rahman}
 *  ******************************************************
 */

package com.feratech.nps.domain;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;


/**
 * User: Bazlur Rahman Rokon
 * Date: 5/23/13 - 9:00 PM
 */
@DatabaseTable
@JsonIgnoreProperties(ignoreUnknown = true)
public class Questionnaire implements Serializable {
    @DatabaseField(generatedId = true)
    private int Id;

    @DatabaseField(foreign = true, foreignAutoRefresh = true, canBeNull = true,
            maxForeignAutoRefreshLevel = 3)
    private ShopType shopType;

    @DatabaseField(foreign = true)
    private QuestionnaireMeta questionnaireMeta;

    @ForeignCollectionField(eager = true)
    private Collection<QuestionMeta> questionMetas;

    @ForeignCollectionField(eager = true)
    private Collection<UnsuccessfulAudit> unsuccessfulAudits;

    @ForeignCollectionField(eager = true)
    private Collection<MustHaveSku> mustHaveSkus;

    @ForeignCollectionField(eager = true)
    private Collection<FixedDisplaySku> fixedDisplaySkus;

    @ForeignCollectionField(eager = true)
    private Collection<WaveDisplaySku> waveDisplays;

    @ForeignCollectionField(eager = true)
    private Collection<HotSpot> hotSpots;

    @ForeignCollectionField(eager = true)
    private Collection<PointOfPurchase> pointOfPurchases;

    @ForeignCollectionField(eager = true)
    private Collection<AdditionalSku> aditionalSkus;

    @DatabaseField
    private String dmsCode;

    @DatabaseField
    private boolean synced;
    @DatabaseField
    private boolean newData;

    @DatabaseField
    private String nameOfShopOwnerOrSalesMan;
    @DatabaseField
    private String cellPhone;
    @DatabaseField
    private String longitude;
    @DatabaseField
    private String latitude;
    @DatabaseField
    private String image1Url;
    @DatabaseField
    private String imageUrl2;

    @DatabaseField
    private Date dateTime;

    public Collection<AdditionalSku> getAditionalSkus() {
        return aditionalSkus;
    }

    public void setAditionalSkus(Collection<AdditionalSku> aditionalSkus) {
        this.aditionalSkus = aditionalSkus;
    }

    public Collection<FixedDisplaySku> getFixedDisplaySkus() {
        return fixedDisplaySkus;
    }

    public void setFixedDisplaySkus(Collection<FixedDisplaySku> fixedDisplaySkus) {
        this.fixedDisplaySkus = fixedDisplaySkus;
    }

    public Collection<HotSpot> getHotSpots() {
        return hotSpots;
    }

    public void setHotSpots(Collection<HotSpot> hotSpots) {
        this.hotSpots = hotSpots;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        this.Id = id;
    }

    public Collection<MustHaveSku> getMustHaveSkus() {
        return mustHaveSkus;
    }

    public void setMustHaveSkus(Collection<MustHaveSku> mustHaveSkus) {
        this.mustHaveSkus = mustHaveSkus;
    }

    public Collection<PointOfPurchase> getPointOfPurchases() {
        return pointOfPurchases;
    }

    public void setPointOfPurchases(Collection<PointOfPurchase> pointOfPurchases) {
        this.pointOfPurchases = pointOfPurchases;
    }

    public Collection<QuestionMeta> getQuestionMetas() {
        return questionMetas;
    }

    public void setQuestionMetas(Collection<QuestionMeta> questionMetas) {
        this.questionMetas = questionMetas;
    }

    public QuestionnaireMeta getQuestionnaireMeta() {
        return questionnaireMeta;
    }

    public void setQuestionnaireMeta(QuestionnaireMeta questionnaireMeta) {
        this.questionnaireMeta = questionnaireMeta;
    }

    public ShopType getShopType() {
        return shopType;
    }

    public void setShopType(ShopType shopType) {
        this.shopType = shopType;
    }

    public Collection<UnsuccessfulAudit> getUnsuccessfulAudits() {
        return unsuccessfulAudits;
    }

    public void setUnsuccessfulAudits(Collection<UnsuccessfulAudit> unsuccessfulAudits) {
        this.unsuccessfulAudits = unsuccessfulAudits;
    }

    public Collection<WaveDisplaySku> getWaveDisplays() {
        return waveDisplays;
    }

    public void setWaveDisplays(Collection<WaveDisplaySku> waveDisplays) {
        this.waveDisplays = waveDisplays;
    }

    public boolean isSynced() {
        return synced;
    }

    public void setSynced(boolean synced) {
        this.synced = synced;
    }

    public boolean isNewData() {
        return newData;
    }

    public void setNewData(boolean newData) {
        this.newData = newData;
    }

    public String getNameOfShopOwnerOrSalesMan() {
        return nameOfShopOwnerOrSalesMan;
    }

    public void setNameOfShopOwnerOrSalesMan(String nameOfShopOwnerOrSalesMan) {
        this.nameOfShopOwnerOrSalesMan = nameOfShopOwnerOrSalesMan;
    }

    public String getCellPhone() {
        return cellPhone;
    }

    public void setCellPhone(String cellPhone) {
        this.cellPhone = cellPhone;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getImage1Url() {
        return image1Url;
    }

    public void setImage1Url(String image1Url) {
        this.image1Url = image1Url;
    }

    public String getImageUrl2() {
        return imageUrl2;
    }

    public void setImageUrl2(String imageUrl2) {
        this.imageUrl2 = imageUrl2;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Questionnaire{");
        sb.append("id=").append(Id);
//        sb.append(", shopType=").append(shopType);
//        sb.append(", questionnaireMeta=").append(questionnaireMeta);
//        sb.append(", questionMetas=").append(questionMetas);
//        sb.append(", unsuccessfulAudits=").append(unsuccessfulAudits);
//        sb.append(", mustHaveSkus=").append(mustHaveSkus);
//        sb.append(", fixedDisplaySkus=").append(fixedDisplaySkus);
//        sb.append(", waveDisplays=").append(waveDisplays);
//        sb.append(", hotSpots=").append(hotSpots);
//        sb.append(", pointOfPurchases=").append(pointOfPurchases);
//        sb.append(", aditionalSkus=").append(aditionalSkus);
        sb.append(", dmsCode='").append(dmsCode).append('\'');
        sb.append(", synced=").append(synced);
        sb.append(", newData=").append(newData);
        sb.append(", nameOfShopOwnerOrSalesMan='").append(nameOfShopOwnerOrSalesMan).append('\'');
        sb.append(", cellPhone='").append(cellPhone).append('\'');
        sb.append(", longitude='").append(longitude).append('\'');
        sb.append(", latitude='").append(latitude).append('\'');
        sb.append(", image1Url='").append(image1Url).append('\'');
        sb.append(", imageUrl2='").append(imageUrl2).append('\'');
        sb.append(", dateTime=").append(dateTime);
        sb.append('}');
        return sb.toString();
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public String getDmsCode() {
        return dmsCode;
    }

    public void setDmsCode(String dmsCode) {
        this.dmsCode = dmsCode;
    }
}
