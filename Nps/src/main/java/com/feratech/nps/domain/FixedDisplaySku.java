package com.feratech.nps.domain;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.io.Serializable;

/**
 * User: Bazlur Rahman Rokon
 * Date: 7/10/13 - 11:04 PM
 */
@DatabaseTable
@JsonIgnoreProperties(ignoreUnknown = true)
public class FixedDisplaySku implements Serializable {
    @DatabaseField(generatedId = true)
    private int id;
    @DatabaseField
    public String skuCode;
    @DatabaseField
    private int skuCount;
    @DatabaseField(foreign = true)
    private ShopType shopType;
    @DatabaseField
    private String skuName;
    @DatabaseField
    private boolean availability;
    @DatabaseField
    private int totalDisplayCount;
    @DatabaseField
    private int faceUpCount;
    @DatabaseField
    private boolean sequence;
    @DatabaseField
    private int Group;
    private int questionSetId;
    @DatabaseField(foreign = true, foreignAutoCreate = true, foreignAutoRefresh = true)
    private QuestionSet questionSet;
    @DatabaseField(foreign = true)
    private Questionnaire questionnaire;

    @DatabaseField()
    private boolean rawData;

    public boolean isRawData() {
        return rawData;
    }

    public void setRawData(boolean rawData) {
        this.rawData = rawData;
    }
    public int getQuestionSetId() {
        return questionSetId;
    }

    public void setQuestionSetId(int questionSetId) {
        this.questionSetId = questionSetId;
    }

    public boolean isAvailability() {
        return availability;
    }

    public void setAvailability(boolean availability) {
        this.availability = availability;
    }

    public int getFaceUpCount() {
        return faceUpCount;
    }

    public void setFaceUpCount(int faceUpCount) {
        this.faceUpCount = faceUpCount;
    }

    public int getGroup() {
        return Group;
    }

    public void setGroup(int group) {
        Group = group;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Questionnaire getQuestionnaire() {
        return questionnaire;
    }

    public void setQuestionnaire(Questionnaire questionnaire) {
        this.questionnaire = questionnaire;
    }

    public QuestionSet getQuestionSet() {
        return questionSet;
    }

    public void setQuestionSet(QuestionSet questionSet) {
        this.questionSet = questionSet;
    }

    public boolean isSequence() {
        return sequence;
    }

    public void setSequence(boolean sequence) {
        this.sequence = sequence;
    }

    public ShopType getShopType() {
        return shopType;
    }

    public void setShopType(ShopType shopType) {
        this.shopType = shopType;
    }

    public String getSkuCode() {
        return skuCode;
    }

    public void setSkuCode(String skuCode) {
        this.skuCode = skuCode;
    }

    public int getSkuCount() {
        return skuCount;
    }

    public void setSkuCount(int skuCount) {
        this.skuCount = skuCount;
    }

    public String getSkuName() {
        return skuName;
    }

    public void setSkuName(String skuName) {
        this.skuName = skuName;
    }

    public int getTotalDisplayCount() {
        return totalDisplayCount;
    }

    public void setTotalDisplayCount(int totalDisplayCount) {
        this.totalDisplayCount = totalDisplayCount;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("FixedDisplaySku{");
        sb.append("id=").append(id);
        sb.append(", skuCode='").append(skuCode).append('\'');
        sb.append(", skuCount=").append(skuCount);
        sb.append(", shopType=").append(shopType);
        sb.append(", skuName='").append(skuName).append('\'');
        sb.append(", availability=").append(availability);
        sb.append(", totalDisplayCount=").append(totalDisplayCount);
        sb.append(", faceUpCount=").append(faceUpCount);
        sb.append(", sequence=").append(sequence);
        sb.append(", Group=").append(Group);
        sb.append(", questionSetId=").append(questionSetId);
        sb.append(", questionSet=").append(questionSet);
        sb.append(", questionnaire=").append(questionnaire);
        sb.append(", rawData=").append(rawData);
        sb.append('}');
        return sb.toString();
    }
}