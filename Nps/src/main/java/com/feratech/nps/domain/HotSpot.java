/*
 * ******************************************************
 *  * Copyright (C) 2013 A. N. M. Bazlur Rahman <anmbrr.bit0112@gmail.com>
 *  *
 *  * This file is part of perfect-store.
 *  *
 *  * perfect-store can not be copied and/or distributed without the express
 *  * permission of {A. N. M. Bazlur Rahman}
 *  ******************************************************
 */

package com.feratech.nps.domain;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.io.Serializable;

/**
 * User: Bazlur Rahman Rokon
 * Date: 5/31/13 - 12:03 AM
 */
@DatabaseTable
@JsonIgnoreProperties(ignoreUnknown = true)
public class HotSpot implements Serializable {
    @DatabaseField(generatedId = true)
    public int id;
    @DatabaseField
    public String head;
    @DatabaseField
    public String description;
    @DatabaseField(foreign = true)
    public ShopType shopType;
    @DatabaseField(foreign = true, foreignAutoCreate = true, foreignAutoRefresh = true)
    public Questionnaire questionnaire;
    @DatabaseField()
    private boolean rawData;

    public boolean isRawData() {
        return rawData;
    }

    public void setRawData(boolean rawData) {
        this.rawData = rawData;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getHead() {
        return head;
    }

    public void setHead(String head) {
        this.head = head;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Questionnaire getQuestionnaire() {
        return questionnaire;
    }

    public void setQuestionnaire(Questionnaire questionnaire) {
        this.questionnaire = questionnaire;
    }

    public ShopType getShopType() {
        return shopType;
    }

    public void setShopType(ShopType shopType) {
        this.shopType = shopType;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("HotSpotJson{");
        sb.append("id=").append(id);
        sb.append(", head='").append(head).append('\'');
        sb.append(", description='").append(description).append('\'');
        sb.append(", shopType=").append(shopType);
        sb.append(", questionnaire=").append(questionnaire);
        sb.append(", rawData=").append(rawData);
        sb.append('}');
        return sb.toString();
    }
}
