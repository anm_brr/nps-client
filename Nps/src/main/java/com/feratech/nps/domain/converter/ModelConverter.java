package com.feratech.nps.domain.converter;

import android.util.Log;
import com.feratech.nps.domain.*;
import com.feratech.nps.domain.json.*;
import com.feratech.nps.utils.Logger;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * User: Bazlur Rahman Rokon
 * Date: 8/7/13 - 11:46 AM
 */
public class ModelConverter {
    private static final Logger log = Logger.getLogger(ModelConverter.class);

    public static AdditionalSkuJson convert(AdditionalSku additionalSku) {
        return new AdditionalSkuJson(additionalSku.getId(), additionalSku.getSkuName(), additionalSku.getSkuCode(), additionalSku.getSkuCount());
    }

    public static List<AdditionalSkuJson> convertAdditionalSkuToJson(Collection<AdditionalSku> additionalSkus) {
        log.debug("convertAdditionalSkuToJson() additionalSkus.size()={}", additionalSkus.size());
        List<AdditionalSkuJson> additionalSkuJsonList = new ArrayList<AdditionalSkuJson>();
        for (AdditionalSku a : additionalSkus) {
            additionalSkuJsonList.add(convert(a));
        }
        return additionalSkuJsonList;
    }

    public static FixedDisplaySkuJson convert(FixedDisplaySku f) {
        return new FixedDisplaySkuJson(
                f.getId(),
                f.getSkuCode(),
                f.getSkuCount(),
                f.getSkuName(),
                f.isAvailability(),
                f.getTotalDisplayCount(),
                f.getFaceUpCount(),
                f.isSequence());
    }

    public static List<FixedDisplaySkuJson> convertFixedDisplaySkuToJsons(Collection<FixedDisplaySku> fixedDisplaySkus) {
        log.verbose("convertFixedDisplaySkuToJsons()");
        List<FixedDisplaySkuJson> fixedDisplaySkuJsons = new ArrayList<FixedDisplaySkuJson>();
        for (FixedDisplaySku f : fixedDisplaySkus) fixedDisplaySkuJsons.add(convert(f));
        return fixedDisplaySkuJsons;
    }

    public static HotSpotWithComplianceJson convert(HotSpotWithCompliance h) {
        return new HotSpotWithComplianceJson(h.getId(), h.getHead(), h.getDescription(), h.isCompliance());
    }

    public static List<HotSpotWithComplianceJson> convertHotSpotWithComplianceToJsons(Collection<HotSpotWithCompliance> hotSpotWithCompliances) {
        log.verbose("convertHotSpotWithComplianceToJsons()");
        List<HotSpotWithComplianceJson> list = new ArrayList<HotSpotWithComplianceJson>();
        for (HotSpotWithCompliance h : hotSpotWithCompliances) list.add(convert(h));
        return list;
    }

    public static HotSpotWithOralCareJson convert(HotSpotWithOralCare h) {
        return new HotSpotWithOralCareJson(h.getId(), h.getHead(), h.getDescription(), h.isHairCare(), h.isSkinCare(), h.isOralCare());
    }

    public static List<HotSpotWithOralCareJson> hotSpotWithOralCareToJsons(Collection<HotSpotWithOralCare> hotSpotWithOralCares) {
        log.verbose("hotSpotWithOralCareToJsons()");
        List<HotSpotWithOralCareJson> list = new ArrayList<HotSpotWithOralCareJson>();
        for (HotSpotWithOralCare h : hotSpotWithOralCares) list.add(convert(h));
        return list;
    }

    public static HotSpotWithSkinCareJson convert(HotSpotWithSkinCare h) {
        return new HotSpotWithSkinCareJson(h.getId(), h.getHead(), h.getDescription(), h.isHairCare(), h.isSkinCare());
    }

    public static List<HotSpotWithSkinCareJson> convertHotSpotWithSkinCareToJsons(Collection<HotSpotWithSkinCare> hotSpotWithSkinCares) {
        log.verbose("convertHotSpotWithSkinCareToJsons()");
        List<HotSpotWithSkinCareJson> list = new ArrayList<HotSpotWithSkinCareJson>();
        for (HotSpotWithSkinCare h : hotSpotWithSkinCares) list.add(convert(h));
        return list;
    }

    public static MustHaveSkuJson convert(MustHaveSku m) {
        return new MustHaveSkuJson(m.getId(), m.getSkuName(), m.getSkuCode(), m.getSkuCount());
    }

    public static List<MustHaveSkuJson> convertMustHaveSkuToJson(Collection<MustHaveSku> mustHaveSkus) {
        log.verbose("convertMustHaveSkuToJson()");
        List<MustHaveSkuJson> list = new ArrayList<MustHaveSkuJson>();
        for (MustHaveSku m : mustHaveSkus) list.add(convert(m));
        return list;
    }

    public static PointOfPurchaseJson convert(PointOfPurchase p) {
        return new PointOfPurchaseJson(p.getId(), p.getName(), p.getDescription(), p.isAvailability());
    }

    public static List<PointOfPurchaseJson> convertPointOfPurchaseToJson(Collection<PointOfPurchase> pointOfPurchases) {
        log.verbose("convertPointOfPurchaseToJson() pointOfPurchases.size()={}", pointOfPurchases.size());
        List<PointOfPurchaseJson> list = new ArrayList<PointOfPurchaseJson>();
        for (PointOfPurchase p : pointOfPurchases) list.add(convert(p));

        log.verbose("convertPointOfPurchaseToJson() convertedList.size()={}", list.size());
        return list;
    }

    public static UnsuccessfulAuditJson convert(UnsuccessfulAudit u) {
        return new UnsuccessfulAuditJson(u.getId(), u.getReason(), u.getCode(), u.getDealersCode(), u.getMonth(), u.getBusinessName());
    }

    public static List<UnsuccessfulAuditJson> convertUnsuccessfulAuditToJson(Collection<UnsuccessfulAudit> unsuccessfulAudits) {
        log.verbose("convertUnsuccessfulAuditToJson()");
        List<UnsuccessfulAuditJson> list = new ArrayList<UnsuccessfulAuditJson>();
        for (UnsuccessfulAudit unsuccessfulAudit : unsuccessfulAudits) list.add(convert(unsuccessfulAudit));
        return list;
    }

    public static WaveDisplaySkuJson convert(WaveDisplaySku w) {
        return new WaveDisplaySkuJson(w.getId(), w.getSkuName(), w.getSkuCode(), w.getSkuCount(), w.isSequence());
    }

    public static List<WaveDisplaySkuJson> convertWaveDisplaySkuToJson(Collection<WaveDisplaySku> waveDisplaySkus) {
        log.verbose("convertWaveDisplaySkuToJson()");
        List<WaveDisplaySkuJson> list = new ArrayList<WaveDisplaySkuJson>();
        for (WaveDisplaySku w : waveDisplaySkus) list.add(convert(w));
        return list;
    }

    public static List<? extends HotSpotJson> convertToHotSpotJson(List<? extends HotSpot> hotSpots) {
        log.verbose("convertToHotSpotJson()");
        List hotSpotsJson = null;
        if (hotSpots == null || hotSpots.size() == 0) return new ArrayList<HotSpotJson>();
        Log.d("feratech", "hotSpots.size() =" + hotSpots.size());

        Object hotSpot = new ArrayList(hotSpots).get(0);
        if (hotSpot instanceof HotSpotWithOralCare) {
            hotSpotsJson = new ArrayList<HotSpotWithOralCare>();
            for (HotSpot h : hotSpots) hotSpotsJson.add(convert((HotSpotWithOralCare) h));
        } else if (hotSpot instanceof HotSpotWithSkinCare) {
            hotSpotsJson = new ArrayList<HotSpotWithSkinCare>();
            for (HotSpot h : hotSpots) hotSpotsJson.add(convert((HotSpotWithSkinCare) h));
        } else if (hotSpot instanceof HotSpotWithCompliance) {
            hotSpotsJson = new ArrayList<HotSpotWithCompliance>();
            for (HotSpot h : hotSpots) hotSpotsJson.add(convert((HotSpotWithCompliance) h));
        }
        return hotSpotsJson;
    }

}
