package com.feratech.nps.domain.json;

import java.io.Serializable;

/**
 * User: Bazlur Rahman Rokon
 * Date: 8/7/13 - 11:43 AM
 */
public class WaveDisplaySkuJson implements Serializable {
    private int id;
    private String skuName;
    private String skuCode;
    private int skuCount;
    private boolean sequence;

    public WaveDisplaySkuJson(int id, String skuName, String skuCode, int skuCount, boolean sequence) {
        this.id = id;
        this.skuName = skuName;
        this.skuCode = skuCode;
        this.skuCount = skuCount;
        this.sequence = sequence;
    }

    public WaveDisplaySkuJson() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSkuName() {
        return skuName;
    }

    public void setSkuName(String skuName) {
        this.skuName = skuName;
    }

    public String getSkuCode() {
        return skuCode;
    }

    public void setSkuCode(String skuCode) {
        this.skuCode = skuCode;
    }

    public int getSkuCount() {
        return skuCount;
    }

    public void setSkuCount(int skuCount) {
        this.skuCount = skuCount;
    }

    public boolean isSequence() {
        return sequence;
    }

    public void setSequence(boolean sequence) {
        this.sequence = sequence;
    }
}
