package com.feratech.nps.domain;

import java.io.Serializable;

/**
 * User: Bazlur Rahman Rokon
 * Date: 8/3/13 - 11:33 PM
 */
public class Picture  implements Serializable{
    private String name;
    private byte[] data;
    private String contentType;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }
}
