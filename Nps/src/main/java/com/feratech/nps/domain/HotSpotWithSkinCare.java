/*
 * ******************************************************
 *  * Copyright (C) 2013 A. N. M. Bazlur Rahman <anmbrr.bit0112@gmail.com>
 *  *
 *  * This file is part of perfect-store.
 *  *
 *  * perfect-store can not be copied and/or distributed without the express
 *  * permission of {A. N. M. Bazlur Rahman}
 *  ******************************************************
 */

package com.feratech.nps.domain;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * User: Bazlur Rahman Rokon
 * Date: 6/14/13 - 11:27 PM
 */
@DatabaseTable
@JsonIgnoreProperties(ignoreUnknown = true)
public class HotSpotWithSkinCare extends HotSpot {
    @DatabaseField
    public boolean hairCare;
    @DatabaseField
    public boolean skinCare;


    public HotSpotWithSkinCare() {
    }

    public HotSpotWithSkinCare(HotSpot hotSpot) {
        this.setHead(hotSpot.getHead());
        this.setDescription(hotSpot.getDescription());
    }


    public boolean isHairCare() {
        return hairCare;
    }

    public void setHairCare(boolean hairCare) {
        this.hairCare = hairCare;
    }

    public boolean isSkinCare() {
        return skinCare;
    }

    public void setSkinCare(boolean skinCare) {
        this.skinCare = skinCare;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("HotSpotWithSkinCare{");
        sb.append("hairCare=").append(hairCare);
        sb.append(", skinCare=").append(skinCare);
        sb.append('}');
        return sb.toString();
    }
}
