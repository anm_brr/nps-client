package com.feratech.nps.domain.json;

/**
 * User: Bazlur Rahman Rokon
 * Date: 8/7/13 - 11:38 AM
 */
public class HotSpotWithOralCareJson extends HotSpotJson {
    private boolean hairCare;
    private boolean skinCare;
    private boolean oralCare;

    public HotSpotWithOralCareJson(int id, String head, String description, boolean hairCare, boolean skinCare, boolean oralCare) {
        super(id, head, description);
        this.hairCare = hairCare;
        this.skinCare = skinCare;
        this.oralCare = oralCare;
    }

    public HotSpotWithOralCareJson() {
    }

    public boolean isHairCare() {
        return hairCare;
    }

    public void setHairCare(boolean hairCare) {
        this.hairCare = hairCare;
    }

    public boolean isSkinCare() {
        return skinCare;
    }

    public void setSkinCare(boolean skinCare) {
        this.skinCare = skinCare;
    }

    public boolean isOralCare() {
        return oralCare;
    }

    public void setOralCare(boolean oralCare) {
        this.oralCare = oralCare;
    }
}
