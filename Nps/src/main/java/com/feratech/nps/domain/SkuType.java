/*
 * ******************************************************
 *  * Copyright (C) 2013 A. N. M. Bazlur Rahman <anmbrr.bit0112@gmail.com>
 *  *
 *  * This file is part of perfect-store.
 *  *
 *  * perfect-store can not be copied and/or distributed without the express
 *  * permission of {A. N. M. Bazlur Rahman}
 *  ******************************************************
 */

package com.feratech.nps.domain;

/**
 * User: Bazlur Rahman Rokon
 * Date: 5/29/13 - 9:44 PM
 */
public enum SkuType {

    MUST_HAVE_SKU(1), FIXED_DISPLAY(2), WAVE_DISPLAY(3),;
    private int skuType;

    SkuType(int i) {
        this.skuType = i;
    }

    public int getSkuType() {
        return skuType;
    }
}
