package com.feratech.nps.domain;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.io.Serializable;

/**
 * User: Bazlur Rahman Rokon
 * Date: 7/9/13 - 3:13 AM
 */

@DatabaseTable
@JsonIgnoreProperties(ignoreUnknown = true)
public class StoreInfo implements Serializable {

    public static final String DMS_CODE = "dms_code";
    public static final String REGION = "region";
    public static final String TOWN = "town";
    public static final String TERRITORY = "territory";
    public static final String SHOP_NAME = "shop_name";
    public static final String SHOP_TYPE = "shop_type";

    @DatabaseField(columnName = REGION)
    public String region;
    @DatabaseField(columnName = TERRITORY)
    public String territory;
    @DatabaseField(columnName = TOWN)
    public String town;
    @DatabaseField(columnName = SHOP_TYPE)
    public String shopType;
    @DatabaseField(columnName = DMS_CODE)
    public String dmsCode;
    @DatabaseField(generatedId = true)
    private int id;
    @DatabaseField
    private String storeName;
    @DatabaseField
    private String slab;
    @DatabaseField
    private String phoneNumber;
    @DatabaseField
    private String storeAddress;
    @DatabaseField
    private String longitude;
    @DatabaseField
    private String latitude;
    @DatabaseField
    private String dateCreated;
    @DatabaseField
    private String dateLastUpdated;
    @DatabaseField
    private boolean visited;

    public StoreInfo() {
    }

    public StoreInfo(String region, String dmsCode, String shopType, String territory, String town) {
        this.region = region;
        this.dmsCode = dmsCode;
        this.shopType = shopType;
        this.territory = territory;
        this.town = town;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getTerritory() {
        return territory;
    }

    public void setTerritory(String territory) {
        this.territory = territory;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getShopType() {
        return shopType;
    }

    public void setShopType(String shopType) {
        this.shopType = shopType;
    }

    public String getDmsCode() {
        return dmsCode;
    }

    public void setDmsCode(String dmsCode) {
        this.dmsCode = dmsCode;
    }


    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getSlab() {
        return slab;
    }

    public void setSlab(String slab) {
        this.slab = slab;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getStoreAddress() {
        return storeAddress;
    }

    public void setStoreAddress(String storeAddress) {
        this.storeAddress = storeAddress;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getDateLastUpdated() {
        return dateLastUpdated;
    }

    public void setDateLastUpdated(String dateLastUpdated) {
        this.dateLastUpdated = dateLastUpdated;
    }

    public boolean getVisited() {
        return visited;
    }

    public void setVisited(boolean visited) {
        this.visited = visited;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("StoreInfo{");
        sb.append("region='").append(region).append('\'');
        sb.append(", territory='").append(territory).append('\'');
        sb.append(", town='").append(town).append('\'');
        sb.append(", shopType='").append(shopType).append('\'');
        sb.append(", dmsCode='").append(dmsCode).append('\'');
        sb.append(", id=").append(id);
        sb.append(", storeName='").append(storeName).append('\'');
        sb.append(", slab='").append(slab).append('\'');
        sb.append(", phoneNumber='").append(phoneNumber).append('\'');
        sb.append(", storeAddress='").append(storeAddress).append('\'');
        sb.append(", longitude='").append(longitude).append('\'');
        sb.append(", latitude='").append(latitude).append('\'');
        sb.append(", dateCreated='").append(dateCreated).append('\'');
        sb.append(", dateLastUpdated='").append(dateLastUpdated).append('\'');
        sb.append(", visited=").append(visited);
        sb.append('}');
        return sb.toString();
    }


}
