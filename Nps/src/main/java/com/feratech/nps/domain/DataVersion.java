package com.feratech.nps.domain;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * User: Bazlur Rahman Rokon
 * Date: 9/11/13 - 9:13 PM
 */

public class DataVersion implements Serializable, Parcelable {

    public static final Parcelable.Creator<DataVersion> CREATOR = new Parcelable.Creator<DataVersion>() {
        @Override
        public DataVersion createFromParcel(Parcel parcel) {
            return new DataVersion(parcel);
        }

        @Override
        public DataVersion[] newArray(int size) {
            return new DataVersion[size];
        }
    };
    private int id;
    private int month;
    private int year;
    private int currentVersion;

    public DataVersion(Parcel parcel) {
        id = parcel.readInt();
        month = parcel.readInt();
        year = parcel.readInt();
        currentVersion = parcel.readInt();
    }

    public DataVersion() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getCurrentVersion() {
        return currentVersion;
    }

    public void setCurrentVersion(int currentVersion) {
        this.currentVersion = currentVersion;
    }

    @Override
    public String toString() {
        return "DataVersion{" +
                "id=" + id +
                ", month=" + month +
                ", year=" + year +
                ", currentVersion=" + currentVersion +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeInt(month);
        parcel.writeInt(year);
        parcel.writeInt(currentVersion);
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        if (this == o) return true;
        if (!(o instanceof DataVersion)) return false;

        DataVersion that = (DataVersion) o;

        if (currentVersion != that.currentVersion) return false;
        if (id != that.id) return false;
        if (month != that.month) return false;
        if (year != that.year) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + month;
        result = 31 * result + year;
        result = 31 * result + currentVersion;
        return result;
    }
}
