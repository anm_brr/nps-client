package com.feratech.nps.domain;

/**
 * User: Bazlur Rahman Rokon
 * Date: 8/3/13 - 8:33 AM
 */


public class StoreImage {
    private int id;
    private String location_1;
    private String location_2;
    private boolean uploaded;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLocation_1() {
        return location_1;
    }

    public void setLocation_1(String location_1) {
        this.location_1 = location_1;
    }

    public String getLocation_2() {
        return location_2;
    }

    public void setLocation_2(String location_2) {
        this.location_2 = location_2;
    }

    public boolean isUploaded() {
        return uploaded;
    }

    public void setUploaded(boolean uploaded) {
        this.uploaded = uploaded;
    }
}
