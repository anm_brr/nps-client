package com.feratech.nps.domain.json;

import java.io.Serializable;

/**
 * User: Bazlur Rahman Rokon
 * Date: 8/7/13 - 11:41 AM
 */
public class PointOfPurchaseJson implements Serializable {
    private int id;
    private String name;
    private String description;
    private boolean availability;

    public PointOfPurchaseJson(int id, String name, String description, boolean availability) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.availability = availability;
    }

    public PointOfPurchaseJson() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isAvailability() {
        return availability;
    }

    public void setAvailability(boolean availability) {
        this.availability = availability;
    }
}
