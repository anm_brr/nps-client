/*
 * ******************************************************
 *  * Copyright (C) 2013 A. N. M. Bazlur Rahman <anmbrr.bit0112@gmail.com>
 *  *
 *  * This file is part of perfect-store.
 *  *
 *  * perfect-store can not be copied and/or distributed without the express
 *  * permission of {A. N. M. Bazlur Rahman}
 *  ******************************************************
 */

package com.feratech.nps.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.Point;
import android.os.Build;
import android.os.Handler;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.view.Display;
import android.view.MotionEvent;
import android.view.WindowManager;
import android.widget.EditText;
import com.feratech.nps.domain.DataVersion;

import java.io.File;

/**
 * User: Bazlur Rahman Rokon
 * Date: 3/6/13 - 9:44 AM
 */
public class Utils {
    public static final Logger log = Logger.getLogger(Utils.class);
    public static final String DATA_VERSION_KEY = "__data_version_key__";
    private static final String DATA_VERSION_KEY_ID = "__id__";
    private static final String DATA_VERSION_KEY_YEAR = "__year__";
    private static final String DATA_VERSION_KEY_MONTH = "__month__";
    private static final String DATA_VERSION_KEY_CURRENT_VERSION = "__current_version__";
    private static final String QUESTIONNAIRE_FETCHED = "__questionnaire__";
    private static final String STORE_FETCHED = "__store__";

    public static void setKeyboardFocus(final EditText primaryTextField) {
        (new Handler()).postDelayed(new Runnable() {
            public void run() {
                primaryTextField.dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_DOWN, 0, 0, 0));
                primaryTextField.dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_UP, 0, 0, 0));
            }
        }, 100);
    }

    public static int getWindowWidth(Context context) {
        int value;
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();

        int orientation = context.getResources().getConfiguration().orientation;

        if (Build.VERSION.SDK_INT > 12) {
            Point size = new Point();
            display.getSize(size);
            if (orientation == Configuration.ORIENTATION_PORTRAIT) {
                value = size.x;
            } else if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
                value = size.y;
            } else value = size.x;

        } else {
            if (orientation == Configuration.ORIENTATION_PORTRAIT) {
                value = display.getWidth();
            } else if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
                value = display.getHeight();
            } else
                value = display.getWidth();  // deprecated
        }
        return value;
    }

    public static boolean isEmptyString(String str) {
        return str == null || str.length() == 0;
    }

    public static int getScreenOrientation(Context context) {
        Display getOrient = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        int orientation = Configuration.ORIENTATION_UNDEFINED;
        if (getOrient.getWidth() == getOrient.getHeight()) {
            orientation = Configuration.ORIENTATION_SQUARE;
        } else {
            if (getOrient.getWidth() < getOrient.getHeight()) {
                orientation = Configuration.ORIENTATION_PORTRAIT;
            } else {
                orientation = Configuration.ORIENTATION_LANDSCAPE;
            }
        }
        return orientation;
    }

    public static Bitmap getRotatedBitmap(Bitmap bitmap, int angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(bitmap, 0, 0,
                bitmap.getWidth(), bitmap.getHeight(),
                matrix, true);

    }

    public static long upperPowerOfTwo(long v) {
        v--;
        v |= v >> 1;
        v |= v >> 2;
        v |= v >> 4;
        v |= v >> 8;
        v |= v >> 16;
        v |= v >> 32;
        v++;
        return v;
    }

    public static void calculateSize(File file) {
        log.info("calculateSize() of {}", file.getName());
        if (file.exists()) {
            double bytes = file.length();
            double kilobytes = (bytes / 1024);
            double megabytes = (kilobytes / 1024);
            log.info("bytes={}, kilobytes={}, megabytes={}", bytes, kilobytes, megabytes);
        } else log.error("file not exist");
    }

    public static void calculateSize(Bitmap bitmap) {
        log.info("calculateSize() of given bitmap");
        double bytes = ImageLoader.getSizeInBytes(bitmap);
        double kilobytes = (bytes / 1024);
        double megabytes = (kilobytes / 1024);
        log.info("bytes={}, kilobytes={}, megabytes={}", bytes, kilobytes, megabytes);
    }

    public static void calculateSize(byte[] bitmap) {
        log.info("calculateSize() of given bitmap byte array");
        double bytes = bitmap.length;
        double kilobytes = (bytes / 1024);
        double megabytes = (kilobytes / 1024);
        log.info("bytes={}, kilobytes={}, megabytes={}", bytes, kilobytes, megabytes);
    }

    /**
     * Returns 24-hour based hour from given 12-hour base hour and amPm value.
     * Returns null if given hour and amPm field is empty or invalid.
     *
     * @param hour The string representing hour in 12-hour clock
     * @param amPm The amPm value in 12-hour clock
     * @return An integer representing the hour in 24-hour clock
     */
    public static Integer getIntHour24FromHour12(String hour, String amPm) {
        if (!isEmpty(hour, amPm)) {
            int hourInt = Integer.parseInt(hour);
            if (amPm.equalsIgnoreCase("am") && (hourInt == 12)) {
                hourInt = 0;
            } else if (amPm.equalsIgnoreCase("pm") && (hourInt != 12)) {
                hourInt += 12;
            }
            return hourInt;
        }
        return null;
    }

    //returns true if any of the values is Empty, false if all the values are NonEmpty
    public static boolean isEmpty(String... values) {
        for (String value : values)
            if (isEmptyString(value)) return true;
        return false;
    }

    public static String getFormatedTime(String hour, String minute, String ampm) {
        return hour + " : " + minute + ampm.toUpperCase();
    }

    public static String getFormattedTaskPrefix(int position) {
        return String.format("Task %d: ", (position));
    }

    public static DataVersion getDataVersion(Context context) {
        String id = getData(context, DATA_VERSION_KEY_ID, null);
        String month = getData(context, DATA_VERSION_KEY_MONTH, null);
        String year = getData(context, DATA_VERSION_KEY_YEAR, null);
        String currentVersion = getData(context, DATA_VERSION_KEY_CURRENT_VERSION, null);

        if (id != null && month != null && year != null && currentVersion != null) {
            DataVersion dataVersion = new DataVersion();
            dataVersion.setId(Integer.parseInt(id));
            dataVersion.setMonth(Integer.parseInt(month));
            dataVersion.setYear(Integer.parseInt(year));
            dataVersion.setCurrentVersion(Integer.parseInt(currentVersion));
            return dataVersion;
        } else
            return null;
    }

    public static void saveData(Context con, String variable, String data) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(con);
        prefs.edit().putString(variable, data).commit();
    }

    public static String getData(Context con, String variable, String defaultValue) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(con);
        String data = prefs.getString(variable, defaultValue);
        return data;
    }

    public static void questionnaireFetched(Context context) {
        log.debug("questionnaireFetched()");
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        prefs.edit().putBoolean(QUESTIONNAIRE_FETCHED, true).commit();
    }

    public static void storeFetched(Context context) {
        log.verbose("storeFetched()");
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        prefs.edit().putBoolean(STORE_FETCHED, true).commit();
    }

    public static boolean isQuestionnaireAndStoreFetched(Context context) {
        log.verbose("isQuestionnaireAndStoreFetched()");

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        boolean questionnaire = prefs.getBoolean(QUESTIONNAIRE_FETCHED, false);
        boolean store = prefs.getBoolean(STORE_FETCHED, false);
        return questionnaire && store;
    }

    public static void clearFetchHistory(Context context) {
        log.verbose("clearFetchHistory()");
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(QUESTIONNAIRE_FETCHED, false);
        editor.putBoolean(STORE_FETCHED, false);
        editor.commit();
    }

    public static void clearStoreFetchHistory(Context context) {
        log.verbose("clearStoreFetchHistory()");
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(STORE_FETCHED, false);
        editor.commit();
    }

    public static void clearQuestionnaireFetchHistory(Context context) {
        log.verbose("clearQuestionnaireFetchHistory()");
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(QUESTIONNAIRE_FETCHED, false);
        editor.commit();
    }

    public static void saveDataVersion(Context context, DataVersion dataVersion) {
        log.debug("saveDataVersion() dataVersion={}", dataVersion);
        saveData(context, DATA_VERSION_KEY_ID, String.valueOf(dataVersion.getId()));
        saveData(context, DATA_VERSION_KEY_MONTH, String.valueOf(dataVersion.getMonth()));
        saveData(context, DATA_VERSION_KEY_YEAR, String.valueOf(dataVersion.getYear()));
        saveData(context, DATA_VERSION_KEY_CURRENT_VERSION, String.valueOf(dataVersion.getCurrentVersion()));
    }

}
