package com.feratech.nps.utils.media;

import android.graphics.Bitmap;
import com.feratech.nps.utils.Logger;

import java.lang.ref.SoftReference;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * User: Bazlur Rahman Rokon
 * Date: 8/14/13 - 7:17 PM
 */
public class MemoryCache {
    private static final Logger log = Logger.getLogger(MemoryCache.class);
    private Map<String, SoftReference<Bitmap>> cache = Collections.synchronizedMap(new HashMap<String, SoftReference<Bitmap>>());

    public Bitmap get(String id) {
        log.verbose("get() id={}", id);
        if (!cache.containsKey(id)) return null;
        SoftReference<Bitmap> ref = cache.get(id);
        return ref.get();
    }

    public void put(String id, Bitmap bitmap) {
        log.verbose("put() id={}", id);
        cache.put(id, new SoftReference<Bitmap>(bitmap));
    }

    public void clear() {
        cache.clear();
    }
}
