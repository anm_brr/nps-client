package com.feratech.nps.utils;

import android.widget.EditText;

/**
 * User: Bazlur Rahman Rokon
 * Date: 7/10/13 - 7:34 PM
 */
public class Validator {
    public static boolean validateRequired(EditText editText, String errorMsg) {
        if ("".equals(editText.getText().toString().trim())) {
            editText.setError(errorMsg);
            return false;
        } else {
            editText.setError(null);
            return true;
        }
    }
}
