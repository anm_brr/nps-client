package com.feratech.nps.utils.media.util;

import android.content.Context;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.security.AccessControlException;

public class MediaHelper {
    private static final String TAG ="feratech";

    public static File getExternalFilesDir(Context context) {
        return context.getExternalFilesDir("data");
    }

    public static File getExternalFile(Context context, String fileName) throws ExternalStorageNotAccessibleException {
        verifyExternalStorageAccessibility();
        File externalFilesDir = getExternalFilesDir(context);
        verifyExternalFilesDirIsWritable(externalFilesDir);

        return new File(externalFilesDir, fileName);
    }

    public static Uri getExternalFileUri(Context context, String fileName) throws ExternalStorageNotAccessibleException {
        return Uri.fromFile(getExternalFile(context, fileName));
    }

    private static void verifyExternalStorageAccessibility() throws ExternalStorageNotAccessibleException {
        String externalStorageState = Environment.getExternalStorageState();

        if (!Environment.MEDIA_MOUNTED.equals(externalStorageState)) {
            throw new ExternalStorageNotAccessibleException(externalStorageState);
        }
    }

    private static void verifyExternalFilesDirIsWritable(File externalFilesDir) {
        if (!externalFilesDir.canWrite()) {
            throw new AccessControlException("ExternalFilesDir is not writable. " +
                    "Did you forget to include android.permission.WRITE_EXTERNAL_STORAGE in AndroidManifest.xml?");
        }
    }

    public static Uri getInternalFileUri(Context context, String fileName) {
        File file = context.getFileStreamPath(fileName);
        Log.d(TAG, "getInternalFileUri, " + file.getAbsoluteFile());
        return Uri.fromFile(file);
    }

    /**
     * @return Whether the file was successfully deleted
     */
    public static boolean deleteFile(Uri fileUri) {
        if (fileUri != null) {
            File file = new File(fileUri.getPath());
            if (file.exists()) {
                boolean status = file.delete();
                Log.d(TAG, fileUri.getPath() + " file deleted = " + status);
                return status;
            }
        }

        return false;
    }
}
