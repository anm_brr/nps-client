package com.feratech.nps.utils.media;

import android.graphics.Bitmap;
import com.google.inject.Singleton;

/**
 * User: Bazlur Rahman Rokon
 * Date: 7/7/13 - 4:42 PM
 */
@Singleton
public class ImageHolder {

    private Bitmap image;

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }
}
