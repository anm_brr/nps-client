package com.feratech.nps.utils.media;

/**
 * User: Bazlur Rahman Rokon
 * Date: 7/7/13 - 1:23 PM
 */

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import com.feratech.nps.utils.Logger;
import com.feratech.nps.utils.media.imagecrop.ImageCrop;
import com.feratech.nps.utils.media.util.ExternalStorageNotAccessibleException;
import com.feratech.nps.utils.media.util.MediaHelper;

import java.io.File;

/**
 * Usage Example:
 * <p/>
 * <code>
 * PhotoTaker photoTaker = new PhotoTaker(this);    // 'this' means activity context
 * <p/>
 * // Optional - Override default settings:
 * photoTaker.setCropImage(true);
 * photoTaker.setAspectXForCropping(1);
 * photoTaker.setAspectYForCropping(1);
 * photoTaker.setImageWidthAfterCropping(100);
 * photoTaker.setImageHeightAfterCropping(100);
 * photoTaker.setScaleImageAfterCropping(true);
 * <p/>
 * // Prepare OnPhotoTakeListener
 * OnPhotoTakeListener onPhotoTakeListener = new OnPhotoTakeListener() {
 * \@Override
 * public void onPhotoTake(Bitmap photo) {
 * //do something with photo
 * }
 * }
 * <p/>
 * // To take photo:
 * photoTaker.takePhotoFromCamera(onPhotoTakeListener);
 * <p/>
 * // To pick image from gallery:
 * photoTaker.pickImageFromGallery(onPhotoTakeListener);
 * <p/>
 * // Override onActivityResult() like below:
 * \Override
 * public void onActivityResult(int requestCode, int resultCode, Intent data) {
 * photoTaker.onActivityResult(requestCode, resultCode, data);
 * }
 * </code>
 *
 * @author sharafat
 */
public class PhotoTaker {
    private static final Logger log = Logger.getLogger(PhotoTaker.class);

    public static final int REQ_TAKE_PHOTO_FROM_CAMERA = 8271;
    public static final int REQ_PICK_IMAGE_FROM_GALLERY = 8272;

    private static final String PREF_REQUEST_CODE = "request_code";
    private static final String PREF_PHOTO_FILE_PATH = "photo_file_path";

    private static final String PHOTO_FILE_NAME_PREFIX = "PhotoTaker_img_";
    private static final String SHARED_PREF_NAME = "PhotoTaker";

    private static final String LOG_TAG = PhotoTaker.class.getCanonicalName();

    private boolean cropImage = true;
    private int aspectXForCropping = 0;
    private int aspectYForCropping = 0;
    private int imageWidthAfterCropping = 0;
    private int imageHeightAfterCropping = 0;
    private boolean scaleImageAfterCropping = true;

    private Activity activity;
    private SharedPreferences sharedPreferences;
    private OnPhotoTakeListener onPhotoTakeListener;
    private Uri photoFileUri;

    public PhotoTaker(Activity activity, OnPhotoTakeListener onPhotoTakeListener) {
        this.activity = activity;
        this.onPhotoTakeListener = onPhotoTakeListener;
        this.sharedPreferences = activity.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
    }

    public void takePhotoFromCamera(int requestCode)
            throws ExternalStorageNotAccessibleException, CameraUnavailableException {

        photoFileUri = MediaHelper.getExternalFileUri(activity, PHOTO_FILE_NAME_PREFIX + String.valueOf(System.currentTimeMillis()));

        sharedPreferences.edit()
                .putString(PREF_PHOTO_FILE_PATH, photoFileUri.getPath())
                .putInt(PREF_REQUEST_CODE, requestCode)
                .commit();

        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoFileUri);

        try {
            activity.startActivityForResult(cameraIntent, REQ_TAKE_PHOTO_FROM_CAMERA);
        } catch (ActivityNotFoundException e) {    // device has no camera
            throw new CameraUnavailableException(e);
        }
    }

    public void pickImageFromGallery(int requestCode) {
        sharedPreferences.edit().putInt(PREF_REQUEST_CODE, requestCode).commit();

        Intent intent = new Intent(Intent.ACTION_GET_CONTENT).setType("image/*");
        activity.startActivityForResult(intent, REQ_PICK_IMAGE_FROM_GALLERY);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        log.verbose("onActivityResult() requestCode={}, resultCode={}", requestCode, resultCode);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case REQ_TAKE_PHOTO_FROM_CAMERA:
                    restorePhotoFileUri();
                    cropPhotoOrNotifyPhotoTakenIfCroppingCannotBeDone(photoFileUri);
                    break;
                case REQ_PICK_IMAGE_FROM_GALLERY:
                    cropPhotoOrNotifyPhotoTakenIfCroppingCannotBeDone(data.getData());
                    break;
                case ImageCrop.REQ_CROP_IMAGE:
                    restorePhotoFileUri();
                    onPhotoTakeListener.onPhotoTake(getUserRequestCode(),
                            Uri.parse(photoFileUri.getPath() + ImageCrop.CROPPED_IMAGE_FILE_SUFFIX));
                    break;
            }
        }
    }

    private void restorePhotoFileUri() {
        if (photoFileUri == null) {
            photoFileUri = Uri.fromFile(new File(sharedPreferences.getString(PREF_PHOTO_FILE_PATH, null)));
        }
    }

    private int getUserRequestCode() {
        return sharedPreferences.getInt(PREF_REQUEST_CODE, -1);
    }

    private void cropPhotoOrNotifyPhotoTakenIfCroppingCannotBeDone(Uri photoFileUri) {
        if (cropImage) {
            if (ImageCrop.isImageCroppingAppAvailable(activity)) {
                this.photoFileUri = photoFileUri;
                sharedPreferences.edit().putString(PREF_PHOTO_FILE_PATH, photoFileUri.getPath()).commit();

                ImageCrop.cropImage(photoFileUri, activity, imageWidthAfterCropping, imageHeightAfterCropping,
                        aspectXForCropping, aspectYForCropping, scaleImageAfterCropping);
                return;
            } else {
                Log.w(LOG_TAG, "No crop app found.");
            }
        }

        onPhotoTakeListener.onPhotoTake(getUserRequestCode(), photoFileUri);
    }

    public boolean shouldCropImage() {
        return cropImage;
    }

    public void setCropImage(boolean cropImage) {
        this.cropImage = cropImage;
    }

    public int getImageWidthAfterCropping() {
        return imageWidthAfterCropping;
    }

    public void setImageWidthAfterCropping(int imageWidthAfterCropping) {
        this.imageWidthAfterCropping = imageWidthAfterCropping;
    }

    public int getImageHeightAfterCropping() {
        return imageHeightAfterCropping;
    }

    public void setImageHeightAfterCropping(int imageHeightAfterCropping) {
        this.imageHeightAfterCropping = imageHeightAfterCropping;
    }

    public int getAspectXForCropping() {
        return aspectXForCropping;
    }

    public void setAspectXForCropping(int aspectXForCropping) {
        this.aspectXForCropping = aspectXForCropping;
    }

    public int getAspectYForCropping() {
        return aspectYForCropping;
    }

    public void setAspectYForCropping(int aspectYForCropping) {
        this.aspectYForCropping = aspectYForCropping;
    }

    public boolean shouldScaleImageAfterCropping() {
        return scaleImageAfterCropping;
    }

    public void setScaleImageAfterCropping(boolean scaleImageAfterCropping) {
        this.scaleImageAfterCropping = scaleImageAfterCropping;
    }


    public interface OnPhotoTakeListener {
        void onPhotoTake(int requestCode, Uri photoFileUri);
    }
}
