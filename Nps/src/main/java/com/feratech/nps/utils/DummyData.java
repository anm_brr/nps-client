package com.feratech.nps.utils;

import com.feratech.nps.domain.MustHaveSku;

import java.util.ArrayList;
import java.util.List;

/**
 * User: Bazlur Rahman Rokon
 * Date: 6/24/13 - 2:08 AM
 */
public class DummyData {

    public static List<MustHaveSku> getDummyList() {
        return new ArrayList<MustHaveSku>() {{
            add(new MustHaveSku("asdfasdf  adfadfasdf", "123"));
            add(new MustHaveSku("asdfasdfas asdfas ", "123"));
            add(new MustHaveSku("adfasdfasd asdf asd", "1231"));
            add(new MustHaveSku("adfsasdf asdfasdf", "123"));
            add(new MustHaveSku("adfsasdf a sdfas df", "123"));
            add(new MustHaveSku("adsfasdf asdf ads", "345"));
            add(new MustHaveSku("adfasdf asdf asdf ", "345"));
            add(new MustHaveSku("adsfasdf asdf asdf ", "345"));
            add(new MustHaveSku("asdfasdf asdfas ", "345"));
            add(new MustHaveSku("asdfasdf  adfadfasdf", "123"));
            add(new MustHaveSku("asdfasdfas asdfas ", "123"));
            add(new MustHaveSku("adfasdfasd asdf asd", "1231"));
            add(new MustHaveSku("adfsasdf asdfasdf", "123"));
            add(new MustHaveSku("adfsasdf a sdfas df", "123"));
            add(new MustHaveSku("adsfasdf asdf ads", "345"));
            add(new MustHaveSku("adfasdf asdf asdf ", "345"));
            add(new MustHaveSku("adsfasdf asdf asdf ", "345"));
            add(new MustHaveSku("asdfasdf asdfas ", "345"));
            add(new MustHaveSku("asdfasdf  adfadfasdf", "123"));
            add(new MustHaveSku("asdfasdfas asdfas ", "123"));
            add(new MustHaveSku("adfasdfasd asdf asd", "1231"));
            add(new MustHaveSku("adfsasdf asdfasdf", "123"));
            add(new MustHaveSku("adfsasdf a sdfas df", "123"));
            add(new MustHaveSku("adsfasdf asdf ads", "345"));
            add(new MustHaveSku("adfasdf asdf asdf ", "345"));
            add(new MustHaveSku("adsfasdf asdf asdf ", "345"));
            add(new MustHaveSku("asdfasdf asdfas ", "345"));
        }};
    }
}
