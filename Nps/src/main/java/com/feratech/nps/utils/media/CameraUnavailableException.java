package com.feratech.nps.utils.media;

/**
 * User: Bazlur Rahman Rokon
 * Date: 7/7/13 - 1:24 PM
 */
public class CameraUnavailableException extends Exception {
    public CameraUnavailableException(Throwable throwable) {
        super(throwable);
    }
}
