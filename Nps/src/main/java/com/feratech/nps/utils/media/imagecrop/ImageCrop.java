package com.feratech.nps.utils.media.imagecrop;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.provider.MediaStore;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Lorensius W. L. T <lorenz@londatiga.net> https://github.com/lorensiuswlt/AndroidImageCrop
 * @author Sharafat Ibn Mollah Mosharraf <sharafat_8271@yahoo.co.uk>
 */
public class ImageCrop {
    public static final int REQ_CROP_IMAGE = 8273;
    public static final String CROPPED_IMAGE_FILE_SUFFIX = "_cropped";

    public static final String CHOOSE_CROP_APP_MSG = "Choose Crop App";

    public static boolean isImageCroppingAppAvailable(Activity activity) {
        return getImageCroppingAppList(activity).size() > 0;
    }

    private static List<ResolveInfo> getImageCroppingAppList(Activity activity) {
        return activity.getPackageManager().queryIntentActivities(getImageCroppingIntent(), 0);
    }

    private static Intent getImageCroppingIntent() {
        return new Intent("com.android.camera.action.CROP").setType("image/*");
    }

    public static void cropImage(final Uri imageCaptureUri, final Activity activity, int imageWidth, int imageHeight,
                                 int aspectX, int aspectY, boolean scale) {

        List<ResolveInfo> imageCroppingAppList = getImageCroppingAppList(activity);
        int noOfImageCroppingAppsAvailable = imageCroppingAppList.size();

        if (noOfImageCroppingAppsAvailable == 0) {
            throw new RuntimeException("Cannot crop image: no image cropping app available.");
        }

        Intent imageCroppingIntent = getImageCroppingIntent();

        imageCroppingIntent.setData(imageCaptureUri);

        if (imageWidth != 0) {
            imageCroppingIntent.putExtra("outputX", imageWidth);
        }
        if (imageHeight != 0) {
            imageCroppingIntent.putExtra("outputY", imageHeight);
        }
        if (aspectX != 0) {
            imageCroppingIntent.putExtra("aspectX", aspectX);
        }
        if (aspectY != 0) {
            imageCroppingIntent.putExtra("aspectY", aspectY);
        }
        imageCroppingIntent.putExtra("scale", scale);
        imageCroppingIntent.putExtra("return-data", false);
        imageCroppingIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                Uri.parse(imageCaptureUri.getPath() + CROPPED_IMAGE_FILE_SUFFIX));

        if (noOfImageCroppingAppsAvailable == 1) {
            setIntentComponent(imageCroppingIntent, imageCroppingAppList.get(0));
            activity.startActivityForResult(imageCroppingIntent, REQ_CROP_IMAGE);

            return;
        }

        /* display options to user for choosing crop app */

        final ArrayList<CropApp> cropApps = new ArrayList<CropApp>();

        for (ResolveInfo cropAppResolveInfo : imageCroppingAppList) {
            CropApp cropApp = new CropApp();

            cropApp.title = activity.getPackageManager().getApplicationLabel(
                    cropAppResolveInfo.activityInfo.applicationInfo);
            cropApp.icon = activity.getPackageManager().getApplicationIcon(
                    cropAppResolveInfo.activityInfo.applicationInfo);
            cropApp.appIntent = new Intent(imageCroppingIntent);

            setIntentComponent(cropApp.appIntent, cropAppResolveInfo);

            cropApps.add(cropApp);
        }

        CropAppChooserAdapter adapter = new CropAppChooserAdapter(activity, cropApps);

        new AlertDialog.Builder(activity)
                .setTitle(CHOOSE_CROP_APP_MSG)
                .setAdapter(adapter,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int item) {
                                activity.startActivityForResult(cropApps.get(item).appIntent, REQ_CROP_IMAGE);
                            }
                        })
                .create()
                .show();
    }

    private static Intent setIntentComponent(Intent imageCroppingIntent, ResolveInfo cropAppResolveInfo) {
        return imageCroppingIntent.setComponent(new ComponentName(
                cropAppResolveInfo.activityInfo.packageName, cropAppResolveInfo.activityInfo.name));
    }
}
