/*
 * ******************************************************
 *  * Copyright (C) 2013 A. N. M. Bazlur Rahman <anmbrr.bit0112@gmail.com>
 *  *
 *  * This file is part of perfect-store.
 *  *
 *  * perfect-store can not be copied and/or distributed without the express
 *  * permission of {A. N. M. Bazlur Rahman}
 *  ******************************************************
 */

package com.feratech.nps.utils;

import android.content.ContentResolver;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;

import java.io.BufferedInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/**
 * User: Bazlur Rahman Rokon
 * Date: 3/21/13 - 1:10 PM
 */

public class ImageLoader {
    public static final Logger log = Logger.getLogger(ImageLoader.class);

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        log.debug("calculateInSampleSize() ");
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            // Calculate ratios of height and width to requested height and width
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);

            log.debug("calculateInSampleSize() heightRatio={} , widthRatio={}", heightRatio, widthRatio);
            // Choose the smallest ratio as inSampleSize value, this will guarantee
            // a final image with both dimensions larger than or equal to the
            // requested height and width.
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
            log.debug("calculateInSampleSize() inSampleSize={}", inSampleSize);
        }
        int inSampleSizeRounded = (int) Utils.upperPowerOfTwo(inSampleSize);
        log.debug("calculateInSampleSize() -> inSampleSizeRounded() ={}", inSampleSizeRounded);
        return inSampleSizeRounded;
    }


    public static Bitmap decodeFromStream(Context context, Uri contentURI, int reqWidth, int reqHeight) {
        log.debug("decodeFromStream() -> contentURI= {}, reqWidth ={}, reqHeight={}", contentURI.toString(), reqWidth, reqHeight);
        ContentResolver cr = context.getContentResolver();

        try {
            InputStream inputStream = new BufferedInputStream(cr.openInputStream(contentURI));
            inputStream.mark(inputStream.available());

            log.debug("decodeFromStream() -> inputStream = {}", "is available " + inputStream.available());
            // First decode with inJustDecodeBounds=true to check dimensions
            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;

            BitmapFactory.decodeStream(inputStream, null, options);
            // Calculate inSampleSize
            options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
            // Decode bitmap with inSampleSize set

            options.inJustDecodeBounds = false;
            inputStream = new BufferedInputStream(cr.openInputStream(contentURI));
            Bitmap bitmap = BitmapFactory.decodeStream(inputStream, null, options);

            log.debug("decodeFromStream() -> bitmap == {}", (bitmap == null) ? "is null" : "found");
            return bitmap;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException io) {
            io.printStackTrace();
        }
        return null;
    }

    public static Bitmap getScaledBitmap(Bitmap bitmap, int imageSize) {
        log.debug("getScaledBitmap()");

        float ratio = ((float) bitmap.getHeight() / (float) bitmap.getWidth());
        log.debug("getScaledBitmap() -> ratio ={}", ratio);
        return Bitmap.createScaledBitmap(bitmap, imageSize, (int) (ratio * imageSize), true);
    }

    public static long getSizeInBytes(Bitmap bitmap) {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.HONEYCOMB_MR1) {
            return bitmap.getByteCount();
        } else {
            return bitmap.getRowBytes() * bitmap.getHeight();
        }
    }

    public static byte[] bitmapToByteArray(Bitmap bm) {
        // Create the buffer with the correct size
        int iBytes = bm.getWidth() * bm.getHeight() * 4;
        ByteBuffer buffer = ByteBuffer.allocate(iBytes);

        // Log.e("DBG", buffer.remaining()+""); -- Returns a correct number based on dimensions
        // Copy to buffer and then into byte array
        bm.copyPixelsToBuffer(buffer);
        // Log.e("DBG", buffer.remaining()+""); -- Returns 0
        return buffer.array();
    }

}
