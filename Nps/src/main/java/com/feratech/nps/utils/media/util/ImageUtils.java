package com.feratech.nps.utils.media.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.util.Log;
import android.util.TypedValue;
import com.feratech.nps.utils.Logger;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;


public class ImageUtils {
    private static final Logger log = Logger.getLogger(ImageUtils.class);
    private static final String TAG = ImageUtils.class.getName();

    public static Bitmap compress(Bitmap bitmap) {
        return getBitmap(getByteArray(bitmap));
    }

    public static byte[] getByteArray(Bitmap bitmap) {
        return getByteArray(bitmap, 100);
    }

    public static byte[] getByteArray(Bitmap bitmap, int quality) {
        if (bitmap == null) {
            return null;
        }

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, quality, out);
        return out.toByteArray();
    }

    public static byte[] getByteArray(Drawable drawable) {
        return getByteArray(getBitmap(drawable));
    }

    public static Bitmap getBitmap(byte[] byteArray) {
        return byteArray == null ? null : BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
    }

    public static Bitmap getBitmap(Drawable drawable) {
        return drawable == null ? null : ((BitmapDrawable) drawable).getBitmap();
    }

    public static Drawable getDrawable(byte[] byteArray, Context context) {
        return byteArray == null ? null : getDrawable(getBitmap(byteArray), context);
    }

    public static Drawable getDrawable(Bitmap bitmap, Context context) {
        return bitmap == null ? null : new BitmapDrawable(context.getResources(), bitmap);
    }

    public static Bitmap getNonEmptyImage(Bitmap image, Bitmap defaultImage) {
        return image != null ? image : defaultImage;
    }

    public static Bitmap getNonEmptyImage(Bitmap image, byte[] defaultImage) {
        return image != null ? image : getBitmap(defaultImage);
    }

    public static Bitmap getNonEmptyImage(Bitmap image, Drawable defaultImage) {
        return image != null ? image : getBitmap(defaultImage);
    }

    public static Bitmap getNonEmptyImage(byte[] image, byte[] defaultImage) {
        return image != null ? getBitmap(image) : getBitmap(defaultImage);
    }

    public static Bitmap getNonEmptyImage(byte[] image, Bitmap defaultImage) {
        return image != null ? getBitmap(image) : defaultImage;
    }

    public static Bitmap getNonEmptyImage(byte[] image, Drawable defaultImage) {
        return image != null ? getBitmap(image) : getBitmap(defaultImage);
    }

    public static Bitmap getNonEmptyImage(Drawable image, Drawable defaultImage) {
        return image != null ? getBitmap(image) : getBitmap(defaultImage);
    }

    public static Bitmap getNonEmptyImage(Drawable image, byte[] defaultImage) {
        return image != null ? getBitmap(image) : getBitmap(defaultImage);
    }

    public static Bitmap getNonEmptyImage(Drawable image, Bitmap defaultImage) {
        return image != null ? getBitmap(image) : defaultImage;
    }

    public static int dp2px(float dp, Context context) {
        return toPx(TypedValue.COMPLEX_UNIT_DIP, dp, context);
    }

    public static int sp2px(float sp, Context context) {
        return toPx(TypedValue.COMPLEX_UNIT_SP, sp, context);
    }

    public static int pt2px(float pt, Context context) {
        return toPx(TypedValue.COMPLEX_UNIT_PT, pt, context);
    }

    public static int in2px(float in, Context context) {
        return toPx(TypedValue.COMPLEX_UNIT_IN, in, context);
    }

    public static int mm2px(float mm, Context context) {
        return toPx(TypedValue.COMPLEX_UNIT_MM, mm, context);
    }

    private static int toPx(int unit, float value, Context context) {
        //the 0.5 below rounds the expression to the nearest whole when converted to integer
        return (int) ((TypedValue.applyDimension(unit, value, context.getResources().getDisplayMetrics())) + 0.5);
    }

    /*
     * c.f.: http://stackoverflow.com/questions/8309354/formula-px-to-dp-dp-to-px-android
     */
    public static int px2dp(int px, Context context) {
        //the 0.5 below rounds the expression to the nearest whole when converted to integer
        return (int) (px / context.getResources().getDisplayMetrics().density + 0.5);
    }

    public static Bitmap resize(Bitmap bitmap, float scale) {
        return bitmap == null ? null : Bitmap.createScaledBitmap(bitmap,
                (int) (bitmap.getWidth() * scale), (int) (bitmap.getHeight() * scale), false);
    }

    public static Bitmap resize(Bitmap bitmap, int desiredWidth, int desiredHeight) {
        return bitmap == null ? null : Bitmap.createScaledBitmap(bitmap, desiredWidth, desiredHeight, false);
    }

    public static boolean encodeIntoStream(Bitmap bitmap, Uri contentUri) {
        Log.d(TAG, "encodeIntoStream() contentUri.getPath()=" + contentUri.getPath());
        FileOutputStream fileOutputStream = null;
        boolean encodeSuccess = false;

        try {
            fileOutputStream = new FileOutputStream(contentUri.getPath());
            encodeSuccess = bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);
        } catch (IOException e) {
            Log.e(TAG, "Exception while encoding into stream.", e);
        } finally {
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (IOException ignore) {
                }
            }
        }

        return encodeSuccess;
    }

    /**
     * c.f.: http://developer.android.com/training/displaying-bitmaps/load-bitmap.html
     */
    public static Bitmap decodeFromStream(Context context, Uri contentURI, int desiredWidth, int desiredHeight) {
        log.verbose("decodeFromStream() desiredHeight={}, desiredWidth={}", desiredHeight, desiredWidth);

        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(contentURI.getPath(), options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, desiredWidth, desiredHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(contentURI.getPath(), options);
    }

    private static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            // Calculate ratios of height and width to requested height and width
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);

            // Choose the smallest ratio as inSampleSize value, this will guarantee a final image with both dimensions
            // larger than or equal to the requested height and width.
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }

        return upperPowerOfTwo(inSampleSize);
    }

    /**
     * c.f.: http://stackoverflow.com/questions/466204/rounding-off-to-nearest-power-of-2#466278
     */
    private static int upperPowerOfTwo(int value) {
        value--;

        value |= value >> 1;
        value |= value >> 2;
        value |= value >> 4;
        value |= value >> 8;
        value |= value >> 16;

        value++;

        return value;
    }

    public static Bitmap getBitmapWithOrientationFix(Context context, Uri uri, int desiredWidth, int desiredHeight) {
        Matrix matrix = new Matrix();
        matrix.postRotate(getImageOrientationAngle(uri));

        Bitmap bitmap = decodeFromStream(context, uri, desiredWidth, desiredHeight);

        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }

    public static int getImageOrientationAngle(Uri imageUri) {
        File imageFile = new File(imageUri.getPath());

        try {
            ExifInterface exif = new ExifInterface(imageFile.getAbsolutePath());

            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    return 270;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    return 180;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    return 90;
            }
        } catch (IOException e) {
            Log.e(TAG, "Unable to get image orientation info", e);
        }

        return 0;
    }
}
