package com.feratech.nps.utils.media.imagecrop;

import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

/**
 * Adapter for crop app choice list.
 */
class CropAppChooserAdapter extends ArrayAdapter<CropApp> {
    private static final int ID_IMAGE_VIEW = 1;
    private static final int ID_TEXT_VIEW = 2;
    private static final int LAYOUT_PADDING = 10;

    private Context context;
    private List<CropApp> cropApps;

    public CropAppChooserAdapter(Context context, List<CropApp> cropApps) {
        super(context, 0, cropApps);

        this.context = context;
        this.cropApps = cropApps;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup group) {
        if (convertView == null) {
            convertView = createCropSelectionView();
        }

        CropApp item = cropApps.get(position);

        if (item != null) {
            ((ImageView) convertView.findViewById(ID_IMAGE_VIEW)).setImageDrawable(item.icon);
            ((TextView) convertView.findViewById(ID_TEXT_VIEW)).setText(item.title);

            return convertView;
        }

        return null;
    }

    private View createCropSelectionView() {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        ImageView imageView = new ImageView(context);
        imageView.setId(ID_IMAGE_VIEW);
        imageView.setLayoutParams(layoutParams);

        TextView textView = new TextView(context);
        textView.setId(ID_TEXT_VIEW);
        textView.setLayoutParams(layoutParams);

        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setPadding(LAYOUT_PADDING, LAYOUT_PADDING, LAYOUT_PADDING, LAYOUT_PADDING);
        linearLayout.setGravity(Gravity.CENTER_VERTICAL);

        linearLayout.addView(imageView);
        linearLayout.addView(textView);

        return linearLayout;
    }
}
