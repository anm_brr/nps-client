package com.feratech.nps.utils.media;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.widget.ImageView;
import com.feratech.nps.R;
import com.feratech.nps.utils.Logger;
import com.feratech.nps.utils.media.util.ImageUtils;
import com.google.inject.Inject;

import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * User: Bazlur Rahman Rokon
 * Date: 8/14/13 - 7:16 PM
 */
public class ImageLoader {
    private static final Logger log = Logger.getLogger(ImageLoader.class);

    @Inject
    private Application application;

    private MemoryCache memoryCache = new MemoryCache();
    private FileCache fileCache;
    private Map<ImageView, String> imageViews = Collections.synchronizedMap(new WeakHashMap<ImageView, String>());
    private Context context;
    private ExecutorService executorService;
    final int stub_id = R.drawable.no_image;

    public ImageLoader(Context context) {
        log.verbose("Constructor--");
        fileCache = new FileCache(context);
        this.context = context;
        this.executorService = Executors.newFixedThreadPool(5);
    }

    public void displayImage(String url, ImageView imageView) {
        log.verbose("displayImage()");
        imageViews.put(imageView, url);
        Bitmap bitmap = memoryCache.get(url);
        if (bitmap != null) {
            log.verbose("bitmap is not null");
            imageView.setImageBitmap(bitmap);
        } else {
            queuePhoto(url, imageView);
            // imageView.setImageResource(stub_id);
        }
    }

    private void queuePhoto(String url, ImageView imageView) {
        log.verbose("queuePhoto()");
        PhotoToLoad p = new PhotoToLoad(url, imageView);
        executorService.submit(new PhotosLoader(p));
    }

    private Bitmap getBitmap(String url) {
        log.verbose("getBitmap() url={}", url);
        Bitmap bitmap = ImageUtils.decodeFromStream(context, Uri.parse(url), 96, 96);
        log.verbose("bitmap is null? ={}", (bitmap == null));
        return bitmap;
    }

    private class PhotoToLoad {
        public String url;
        public ImageView imageView;

        public PhotoToLoad(String url, ImageView imageView) {
            this.url = url;
            this.imageView = imageView;
        }
    }

    class PhotosLoader implements Runnable {
        PhotoToLoad photoToLoad;

        PhotosLoader(PhotoToLoad photoToLoad) {
            this.photoToLoad = photoToLoad;
        }

        @Override
        public void run() {
            log.verbose("PhotosLoader-- run()");
            if (imageViewReused(photoToLoad))
                return;
            Bitmap bmp = getBitmap(photoToLoad.url);
            memoryCache.put(photoToLoad.url, bmp);
//            if (imageViewReused(photoToLoad))
//                return;
            BitmapDisplayer bd = new BitmapDisplayer(bmp, photoToLoad);
            Activity a = (Activity) photoToLoad.imageView.getContext();
            a.runOnUiThread(bd);
        }
    }

    boolean imageViewReused(PhotoToLoad photoToLoad) {
        log.verbose("imageViewReused()");
        String tag = imageViews.get(photoToLoad.imageView);
        if (tag == null || !tag.equals(photoToLoad.url))
            return true;
        return false;
    }

    //Used to display bitmap in the UI thread
    class BitmapDisplayer implements Runnable {
        Bitmap bitmap;
        PhotoToLoad photoToLoad;

        public BitmapDisplayer(Bitmap b, PhotoToLoad p) {
            log.verbose("BitmapDisplayer-- constructor");
            bitmap = b;
            photoToLoad = p;
        }

        @Override
        public void run() {
            log.verbose("BitmapDisplayer-- run()");
            if (imageViewReused(photoToLoad)) {
                log.verbose("imageViewReused ={}", true);
                return;
            }
            if (bitmap != null) {
                photoToLoad.imageView.setImageBitmap(bitmap);
            } else {
                photoToLoad.imageView.setImageResource(stub_id);
            }
        }
    }

    public void clearCache() {
        log.verbose("clearCache()");
        memoryCache.clear();
        fileCache.clear();
    }
}
