package com.feratech.nps.utils;

import org.joda.time.DateMidnight;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * User: Bazlur Rahman Rokon
 * Date: 7/7/13 - 2:48 AM
 */
public class DateTimeUtils {
    public static final int INVALID_TIME = -1;

    public static final String DATE_FORMAT = "MM/dd/yyyy";
    public static final String TIME_FORMAT = "hh:mm a";
    public static final String DISPLAY_DATE_FORMAT = "MMM dd, yyyy";

    public static final Map<String, String> TIME_PATTERN_MAP = new HashMap<String, String>();

    static {
        TIME_PATTERN_MAP.put("\\d{1,2}:\\d{2} [aApP][mM]", "hh:mm a");
        TIME_PATTERN_MAP.put("\\d{1,2}: \\d{2} [aApP][mM]", "hh: mm a");
        TIME_PATTERN_MAP.put("\\d{1,2}:\\d{2}[aApP][mM]", "hh:mma");
        TIME_PATTERN_MAP.put("\\d{1,2}: \\d{2}[aApP][mM]", "hh: mma");
    }

    public static DateTimeFormatter getJodaTimeFormatter(String pattern, Locale locale) {
        if (StringUtils.isEmpty(pattern)) {
            pattern = DATE_FORMAT;
        }

        DateTimeFormatter formatter = DateTimeFormat.forPattern(pattern);

        if (locale != null) {
            formatter = formatter.withLocale(locale);
        }

        return formatter;
    }

    public static int getDayOfMonth(Date date, String tz) {
        return new DateMidnight(date, DateTimeZone.forID(tz)).getDayOfMonth();
    }

    public static Date createDateWithZone(int day, int month, int year, String tz) {
        DateMidnight dt = new DateMidnight(year, month, day, DateTimeZone.forID(tz));

        return dt.toDate();
    }

    public static int getDateInt(Date date, String tz) {
        DateTimeFormatter dateTimeFormatter = getJodaTimeFormatter("yyyyMMdd", null);
        dateTimeFormatter = dateTimeFormatter.withZone(DateTimeZone.forID(tz));

        return Integer.parseInt(dateTimeFormatter.print(new DateTime(date)));
    }

    public static String dateToStr(Date date, String tz, String pattern) {
        DateTimeFormatter formatter = (StringUtils.isEmpty(pattern))
                ? DateTimeFormat.forPattern(DATE_FORMAT) : DateTimeFormat.forPattern(pattern);
        formatter = formatter.withZone(DateTimeZone.forID(tz));

        return (date == null) ? null : formatter.print(date.getTime());
    }

    public static Date getDate(String tz) {
        return new DateTime(DateTimeZone.forID(tz)).toDate();
    }

    public static boolean isCurrentMonth(int month, int year, String tz) {
        DateTime dt = getDateTimeWithZone(tz);

        return (dt.getMonthOfYear() == month && dt.getYear() == year);
    }

    public static DateTime getDateTimeWithZone(String tz) {
        return new DateTime(DateTimeZone.forID(tz));
    }

    public static boolean isValidTimeFormat(String time) {
        return !StringUtils.isEmpty(findTimePattern(time));
    }

    public static boolean isValidTime(String time) {
        if (isValidTimeFormat(time)) {
            String t = time.trim();
            String timeDigit = t.substring(0, (t.length() - 2)).trim();
            int hour = Integer.parseInt(timeDigit.substring(0, timeDigit.indexOf(":")).trim());
            int minute = Integer.parseInt(timeDigit.substring(timeDigit.indexOf(":") + 1).trim());

            return (hour >= 1 && hour <= 12 && minute >= 0 && minute <= 59);
        }

        return false;
    }

    public static String findTimePattern(String timeStr) {
        if (!StringUtils.isEmpty(timeStr)) {
            for (String key : TIME_PATTERN_MAP.keySet()) {
                if (timeStr.matches(key)) {
                    return TIME_PATTERN_MAP.get(key);
                }
            }
        }

        return null;
    }

    public static int getTimeInMinute(String timeStr) {
        String pattern = findTimePattern(timeStr);
        if (pattern == null) {
            throw new RuntimeException("Invalid time format");
        }

        DateTimeFormatter fmt = DateTimeFormat.forPattern(pattern);
        DateTime dt = fmt.parseDateTime(timeStr.toLowerCase());

        return dt.getMinuteOfDay();
    }

    public static int getHourOfDay(String time) {
        if (isValidTime(time)) {
            String timePattern = findTimePattern(time);
            DateTimeFormatter formatter = DateTimeFormat.forPattern(timePattern);

            return formatter.parseDateTime(time).getHourOfDay();
        }

        return INVALID_TIME;
    }

    public static int getMinuteOfHour(String time) {
        if (isValidTime(time)) {
            String timePattern = findTimePattern(time);
            DateTimeFormatter formatter = DateTimeFormat.forPattern(timePattern);

            return formatter.parseDateTime(time).getMinuteOfHour();
        }

        return INVALID_TIME;
    }

    public static int getCurrentHourOfDay() {
        return new DateTime().getHourOfDay();
    }

    public static int getCurrentMinuteOfHour() {
        return new DateTime().getMinuteOfHour();
    }

    public static String format(int year, int month, int day, int hourOfDay, int minute, int second, String format) {
        Calendar calendar = Calendar.getInstance();

        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, day);
        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
        calendar.set(Calendar.MINUTE, minute);
        calendar.set(Calendar.SECOND, second);

        return format(calendar, format);
    }

    public static String format(Calendar calendar, String format) {
        return format(calendar.getTime(), format);
    }

    public static String format(Date date, String format) {
        return new SimpleDateFormat(format).format(date);
    }

    /**
     * Return Calendar by setting provided date string with default DATE_FORMAT
     * @param dateTime Date string
     * @return Return calendar by setting provided date string with default DATE_FORMAT
     */
    public static Calendar parse(String dateTime) {
        return parse(dateTime, DATE_FORMAT);
    }

    public static Calendar parse(String dateTime, String pattern) {
        Calendar calendar = Calendar.getInstance();

        try {
            calendar.setTime(new SimpleDateFormat(pattern).parse(dateTime));
        } catch (ParseException e) {
            return null;
        }

        return calendar;
    }

    public static int getHourOfDay(String hour, String amPm) {
        return getHourOfDay(Integer.parseInt(hour), amPm);
    }

    /**
     * c.f.: http://stackoverflow.com/questions/6531632/conversion-fromINVALID_TIME2-hours-ti-to-24-hours-time-in-java#6531706
     */
    public static int getHourOfDay(int hour, String amPm) {
        return "PM".equalsIgnoreCase(amPm) ? hour + 12 : hour == 12 ? 0 : hour;
    }

    /**
     * c.f.: http://stackoverflow.com/questions/14554940/converting-24-hour-clock-toINVALID_TIME2-hour-java
     * c.f.: http://stackoverflow.com/questions/473282/left-padding-integers-with-zeros-in-java
     */
    public static TimeParts convertToString(int hourOfDay, int minute, int second) {
        TimeParts timeParts = new TimeParts();

        timeParts.amPm = hourOfDay / 12 == 0 ? "AM" : "PM";
        timeParts.hour = String.format("%02d", hourOfDay % 12 == 0 ? 12 : hourOfDay % 12);
        timeParts.minute = String.format("%02d", minute);
        timeParts.second = String.format("%02d", second);

        return timeParts;
    }


    public static class TimeParts {
        public String hour, minute, second, amPm;
    }
}
