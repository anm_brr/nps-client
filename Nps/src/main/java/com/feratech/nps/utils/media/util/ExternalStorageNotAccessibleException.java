package com.feratech.nps.utils.media.util;

public class ExternalStorageNotAccessibleException extends Exception {
    private String externalStorageState;

    public ExternalStorageNotAccessibleException(String externalStorageState) {
        this.externalStorageState = externalStorageState;
    }

    public String getExternalStorageState() {
        return externalStorageState;
    }
}
