package com.feratech.nps.utils;

import java.util.List;

/**
 * User: Bazlur Rahman Rokon
 * Date: 7/7/13 - 2:47 AM
 */
public class StringUtils {
    public static boolean isEachStringNotEmpty(String... strings) {
        for (String string : strings) {
            if (isEmpty(string)) {
                return false;
            }
        }

        return true;
    }

    public static boolean isEmpty(String str) {
        return str == null || str.trim().length() == 0;
    }

    public static boolean isNotEmpty(String str) {
        return !isEmpty(str);
    }

    public static String join(Object... values) {
        return joinWithDelimiter(", ", values);
    }

    public static String joinWithDelimiter(String delimiter, Object... values) {
        List elements = CollectionUtils.toList(values);

        StringBuilder sb = new StringBuilder("");
        for (Object element : elements) {
            String str = (element == null) ? null : String.valueOf(element);

            if (isEmpty(str)) {
                continue;
            }

            if (sb.length() > 0) {
                sb.append(delimiter);
            }

            sb.append(str);
        }

        return sb.toString();
    }
}
