package com.feratech.nps.utils;

/**
 * User: Bazlur Rahman Rokon
 * Date: 7/9/13 - 2:44 AM
 */
public class Constants {
    public static final String DATABASE_NAME = "ups_perfect_shop.db";
    public static final int DATABASE_VERSION = 12;
    public static final String DMS_CODE = "dms_code";
    public static final String SHOP_TYPE = "shop_type";
}
