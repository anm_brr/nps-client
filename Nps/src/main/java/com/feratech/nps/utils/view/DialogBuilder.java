package com.feratech.nps.utils.view;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;

public class DialogBuilder {

    public static Dialog buildOkDialog(Context context, int msgResourceId) {
        return buildOkDialog(context, context.getString(msgResourceId));
    }

    public static Dialog buildOkDialog(Context context, String message) {
        return buildOkDialog(context, message, null);
    }

    public static Dialog buildOkDialog(Context context, String message,
                                       DialogInterface.OnClickListener onOkButtonClickListener) {
        return buildOkDialog(context, message, true, onOkButtonClickListener);
    }

    public static Dialog buildOkDialog(Context context, String message, boolean cancelable,
                                       DialogInterface.OnClickListener onOkButtonClickListener) {
        return new AlertDialog.Builder(context)
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, onOkButtonClickListener)
                .setCancelable(cancelable)
                .create();
    }

    public static Dialog buildYesNoDialog(Context context, String message,
                                          DialogInterface.OnClickListener onClickListener) {
        return new AlertDialog.Builder(context)
                .setMessage(message)
                .setPositiveButton(android.R.string.yes, onClickListener)
                .setNegativeButton(android.R.string.no, null)
                .create();
    }

    public static void showOkDialogForFinishingActivity(final Context context, String msg) {
        buildOkDialog(context, msg,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ((Activity) context).finish();
                    }
                }).show();
    }
}
