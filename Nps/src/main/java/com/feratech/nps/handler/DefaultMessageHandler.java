package com.feratech.nps.handler;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import com.feratech.nps.R;
import com.feratech.nps.error.ErrorDetails;
import com.feratech.nps.utils.view.DialogBuilder;

/**
 * @author Abdullah AL Mamun (Oronno)
 * @author Sharafat Ibn Mollah Mosharraf
 */
public class DefaultMessageHandler extends Handler {
    private Context context;
    private boolean finishActivityOnPostError;
    private ProgressDialog progressDialog;

    public DefaultMessageHandler(Context context) {
        this(context, false);
    }

    public DefaultMessageHandler(Context context, boolean finishActivityOnPostError) {
        this(context, null, finishActivityOnPostError, false);
    }

    public DefaultMessageHandler(Context context, boolean finishActivityOnPostError, boolean noProgressDialog) {
        this(context, null, finishActivityOnPostError, noProgressDialog);
    }

    public DefaultMessageHandler(Context context, boolean finishActivityOnPostError, String activityIndicatorMsg) {
        this(context, activityIndicatorMsg, finishActivityOnPostError, false);
    }

    private DefaultMessageHandler(Context context, String activityIndicatorMsg, boolean finishActivityOnPostError,
                                  boolean noProgressDialog) {

        this.context = context;
        this.finishActivityOnPostError = finishActivityOnPostError;

        if (!noProgressDialog) {
            progressDialog = new ProgressDialog(context, R.style.Theme_Nps_Dialog);
            progressDialog.setMessage(activityIndicatorMsg == null
                    && Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB
                    ? context.getString(R.string.loading)
                    : activityIndicatorMsg);
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }
    }

    @Override
    public void handleMessage(Message msg) {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }

        if (msg.what == RequestStatus.SUCCESS) {
            onSuccess(msg);
        } else {
            onFailure((ErrorDetails) msg.obj);
        }
    }

    public void onSuccess(Message msg) {
        //empty implementation
    }

    public void onFailure(final ErrorDetails errorDetails) {

        DialogBuilder.buildOkDialog(context, errorDetails.getErrorMsg(), false, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (errorDetails.getErrorCode() == ErrorDetails.UNAUTHORIZED) {
                  //  LogoutHelper.logout(context);
                } else {
                    postErrorDisplay(errorDetails);
                }
            }
        }).show();
    }

    public void postErrorDisplay(ErrorDetails errorDetails) {
        if (finishActivityOnPostError) {
            ((Activity) context).finish();
        }
    }
}
