package com.feratech.nps.handler;

import android.os.Handler;
import android.os.Message;
import com.feratech.nps.error.ExceptionResolver;
import com.feratech.nps.utils.Logger;
import com.loopj.android.http.AsyncHttpResponseHandler;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.JavaType;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Collection;

/**
 * @author Abdullah Al Mamun (Oronno)
 * @author Sharafat Ibn Mollah Mosharraf
 */
public class DefaultAsyncHttpResponseHandler extends AsyncHttpResponseHandler {
    private static final Logger log = Logger.getLogger(DefaultAsyncHttpResponseHandler.class);

    private static final ObjectMapper objectMapper = new ObjectMapper();

    private Handler handler;
    private JavaType valueType;
    private boolean voidType = false;


    public DefaultAsyncHttpResponseHandler(Handler handler, boolean voidType) {
        this.handler = handler;
        this.voidType = voidType;
    }

    public DefaultAsyncHttpResponseHandler(Handler handler) {
        this(handler, Void.class);
        voidType = true;
    }

    public DefaultAsyncHttpResponseHandler(Handler handler, Type valueType) {
        this.handler = handler;
        this.valueType = objectMapper.getTypeFactory().constructType(valueType);
    }

    public DefaultAsyncHttpResponseHandler(Handler handler, Type valueType,
                                           Class<? extends Collection> collectionClass) {
        this.handler = handler;
        this.valueType = objectMapper.getTypeFactory().constructCollectionType(collectionClass, (Class) valueType);
    }

    @Override
    public void onSuccess(int statusCode, String content) {
        log.debug("onSuccess statusCode={}, contentLength={}", statusCode, content == null ? "null" : content.length());

        try {
            if (voidType) sendMessageToTarget(RequestStatus.SUCCESS, statusCode);
            else
                sendMessageToTarget(RequestStatus.SUCCESS, objectMapper.readValue(content, valueType));
        } catch (Exception e) {
            log.warn("JSON Parse Exception.", e);
            sendMessageToTarget(RequestStatus.FAILURE, ExceptionResolver.jsonParseError());
        }
    }

    @Override
    public void onFailure(Throwable error, String content) {
        log.debug("onFailure content={}", content, error);

        sendMessageToTarget(RequestStatus.FAILURE, ExceptionResolver.getErrorDetails(error, content));
    }

    private void sendMessageToTarget(int what, Object obj) {
        Message.obtain(handler, what, obj).sendToTarget();
    }
}
