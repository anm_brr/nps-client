package com.feratech.nps.handler;

/**
 * User: Bazlur Rahman Rokon
 * Date: 7/7/13 - 2:18 AM
 */
public interface RequestStatus {
    int FAILURE = 0;
    int SUCCESS = 1;
}
