package com.feratech.nps.handler;

import android.os.Handler;
import android.os.Message;
import com.feratech.nps.error.ExceptionResolver;
import com.feratech.nps.utils.Logger;
import com.loopj.android.http.BinaryHttpResponseHandler;

/**
 * @author sharafat
 */
public class DefaultBinaryHttpResponseHandler extends BinaryHttpResponseHandler {
    private static final Logger log = Logger.getLogger(DefaultBinaryHttpResponseHandler.class);

    private Handler handler;

    public DefaultBinaryHttpResponseHandler(Handler handler) {
        this.handler = handler;
    }

    @Override
    public void onSuccess(int statusCode, byte[] bytes) {
        log.debug("onSuccess statusCode={}, contentLength={}", statusCode, bytes == null ? "null" : bytes.length);

        Message.obtain(handler, RequestStatus.SUCCESS, bytes).sendToTarget();
    }

    /*
     * You might be expecting the onFailure() method to be overridden here. But the Loopj async-http-client has a bug
     * that doesn't call the onFailure() method in BinaryHttpResponseHandler in case the server response status
     * is >= 300. Therefore, sendFailureMessage() has been overridden here to handle failures.
     */
    @Override
    protected void sendFailureMessage(Throwable throwable, byte[] responseBody) {
        String content = new String(responseBody);
        log.debug("onFailure responseBody={}", content, throwable);

        Message.obtain(handler, RequestStatus.FAILURE, ExceptionResolver.getErrorDetails(throwable, content)).sendToTarget();
    }
}
