package com.feratech.nps.handler;

import android.os.Handler;
import android.os.Message;
import com.feratech.nps.error.ErrorDetails;

/**
 * User: Bazlur Rahman Rokon
 * Date: 8/4/13 - 5:38 PM
 */
public class DefaultAsyncHandler extends Handler {
    @Override
    public void handleMessage(Message msg) {
        if (msg.what == RequestStatus.SUCCESS) {
            onSuccess(msg);
        } else {
            onFailure((ErrorDetails) msg.obj);
        }
    }

    public void onSuccess(Message msg) {
        //empty implementation
    }

    public void onFailure(final ErrorDetails errorDetails) {

    }
}
