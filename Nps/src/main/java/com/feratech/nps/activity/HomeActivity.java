package com.feratech.nps.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;
import com.feratech.nps.R;
import com.feratech.nps.utils.Logger;
import com.feratech.nps.utils.TestFragment;
import com.viewpagerindicator.TabPageIndicator;
import roboguice.activity.RoboFragmentActivity;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

@ContentView(R.layout.main)
public class HomeActivity extends RoboFragmentActivity {
    private static final Logger log = Logger.getLogger(HomeActivity.class);

    private static final int MENU_ITEM_NEXT = 1;
    private static final int MENU_ITEM_BACK = 2;

    private static final String[] CONTENT = new String[]{"Must Have Sku", "Fixed Display Sku", "Wave Display Sku", "Hot Spot", "Point of Purchase", "Additional Sku"};

    @InjectView(R.id.pager)
    private ViewPager viewPager;

    @InjectView(R.id.indicator)
    private TabPageIndicator indicator;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FragmentPagerAdapter adapter = new FragmentAdapter(getSupportFragmentManager());

        viewPager.setAdapter(adapter);
        indicator.setViewPager(viewPager);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, MENU_ITEM_BACK, 0, "Back")
                .setIcon(R.drawable.navigation_back)
                .setShowAsAction(MenuItem.SHOW_AS_ACTION_WITH_TEXT | MenuItem.SHOW_AS_ACTION_ALWAYS);

        menu.setQwertyMode(true);
        menu.add(0, MENU_ITEM_NEXT, 0, "Next")
                .setIcon(R.drawable.navigation_forward)
                .setShowAsAction(MenuItem.SHOW_AS_ACTION_WITH_TEXT | MenuItem.SHOW_AS_ACTION_ALWAYS);
        return true;
    }

    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        log.debug("onMenuItemSelected()");

        if (item.getItemId() == MENU_ITEM_BACK) {
            log.debug(" MENU_ITEM_BACK selected, viewPager.getCurrentItem()={}", viewPager.getCurrentItem());

            if (viewPager.getCurrentItem() > 0 && viewPager.getCurrentItem() <= (CONTENT.length - 1)) {
                viewPager.setCurrentItem((viewPager.getCurrentItem() - 1));
            }

        } else if (item.getItemId() == MENU_ITEM_NEXT) {
            log.debug(" MENU_ITEM_NEXT selected, viewPager.getCurrentItem()={}", viewPager.getCurrentItem());

            if (viewPager.getCurrentItem() >= 0 && viewPager.getCurrentItem() < (CONTENT.length - 1)) {
                viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
            }
        }

        return super.onMenuItemSelected(featureId, item);
    }

    class FragmentAdapter extends FragmentPagerAdapter {
        public FragmentAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return TestFragment.newInstance(CONTENT[position % CONTENT.length]);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return CONTENT[position % CONTENT.length].toUpperCase();
        }

        @Override
        public int getCount() {
            return CONTENT.length;
        }
    }
}
