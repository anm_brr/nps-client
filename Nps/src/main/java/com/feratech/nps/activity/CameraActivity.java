package com.feratech.nps.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Location;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.feratech.nps.R;
import com.feratech.nps.dao.*;
import com.feratech.nps.domain.*;
import com.feratech.nps.helper.QuestionnaireHelper;
import com.feratech.nps.service.LocationService;
import com.feratech.nps.service.WebServices;
import com.feratech.nps.utils.Logger;
import com.feratech.nps.utils.media.CameraUnavailableException;
import com.feratech.nps.utils.media.ImageHolder;
import com.feratech.nps.utils.media.PhotoTaker;
import com.feratech.nps.utils.media.util.ExternalStorageNotAccessibleException;
import com.feratech.nps.utils.media.util.ImageUtils;
import com.feratech.nps.utils.media.util.MediaHelper;
import com.feratech.nps.utils.view.DialogBuilder;
import com.github.rtyley.android.sherlock.roboguice.activity.RoboSherlockActivity;
import com.google.inject.Inject;
import com.loopj.android.http.AsyncHttpResponseHandler;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

/**
 * User: Bazlur Rahman Rokon
 * Date: 7/7/13 - 10:47 AM
 */
@ContentView(R.layout.camera_and_location)
public class CameraActivity extends RoboSherlockActivity implements PhotoTaker.OnPhotoTakeListener, View.OnClickListener {

    private static final Logger log = Logger.getLogger(CameraActivity.class);

    public static final String FIRST_IMAGE_TAKEN = "first_image_taken";

    private static final int MENU_CAMERA = 54;

    public static String PHOTO_FILE_NAME_1 = "image_name_1.jgp";
    public static String PHOTO_FILE_NAME_2 = "image_name_2.jgp";

    private static final int IMAGE_WIDTH_ACTUAL = 768;
    private static final int IMAGE_HEIGHT_ACTUAL = 1024;
    private static final int IMAGE_WIDTH_THUMBNAIL = 360;
    private static final int IMAGE_HEIGHT_THUMBNAIL = 520;

    private static final int REQ_CAPTURE_PHOTO = 0;
    private static final int REQ_CAPTURE_PHOTO_1 = 1;
    private static final int REQ_CAPTURE_PHOTO_2 = 2;

    @InjectView(R.id.imageView1)
    private ImageView firstImage;
    @InjectView(R.id.imageView2)
    private ImageView secondImage;
    @InjectView(R.id.btn_change_image_1)
    private Button firstImageChangeButton;
    @InjectView(R.id.btn_change_image_2)
    private Button secondImageChangeButton;
    @InjectView(R.id.tv_geo_tag)
    private TextView geoTag;

    private Uri firstImageUri, secondImageUri;
    private PhotoTaker photoTaker;
    private boolean isFirstImageTaken = false;

    @Inject
    private LocationService locationService;
    @Inject
    private ImageHolder imageHolder;

    @Inject
    private StoreInfoDao storeInfoDao;
    @Inject
    private WebServices webServices;
    @Inject
    private QuestionnaireHelper questionnaireHelper;
    @Inject
    private QuestionnaireDao questionnaireDao;

    @Inject
    private HotSpotWithSkinCareDao hotSpotWithSkinCareDao;
    @Inject
    private HotSpotWithComplianceDao hotSpotWithComplianceDao;
    @Inject
    private HotSpotWithOralCareDao hotSpotWithOralCareDao;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        log.debug("onCreate()");

        PHOTO_FILE_NAME_1 = questionnaireHelper.getStoreInfo().getDmsCode() + "_1.jpg";
        PHOTO_FILE_NAME_2 = questionnaireHelper.getStoreInfo().getDmsCode() + "_2.jpg";

        log.debug("PHOTO_FILE_NAME_1={}", PHOTO_FILE_NAME_1);
        log.debug("PHOTO_FILE_NAME_2={}", PHOTO_FILE_NAME_2);

        locationService.startListeningForLocationUpdates(this);

        photoTaker = new PhotoTaker(this, this);
        photoTaker.setCropImage(false);

        firstImage.setOnClickListener(this);
        secondImage.setOnClickListener(this);

        if (savedInstanceState == null) {
            log.debug("savedInstanceState is null");
            firstImageChangeButton.setVisibility(View.GONE);
            secondImageChangeButton.setVisibility(View.GONE);
        } else {
            log.debug("savedInstanceState is not null");
            isFirstImageTaken = savedInstanceState.getBoolean(FIRST_IMAGE_TAKEN);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putBoolean(FIRST_IMAGE_TAKEN, isFirstImageTaken);
        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, MENU_CAMERA, 0, getString(R.string.camera))
                .setIcon(R.drawable.device_access_camera)
                .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == MENU_CAMERA) {
            takePhoto(REQ_CAPTURE_PHOTO);
        }
        return super.onOptionsItemSelected(item);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        log.debug("onActivityResult, requestCode={}, resultCode={}", requestCode, resultCode);
        if (resultCode == RESULT_OK) {
            photoTaker.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onPhotoTake(int requestCode, Uri photoFileUri) {
        log.debug("onPhotoTake() -> requestCode={}", requestCode);

        Bitmap photoBitmap = ImageUtils.getBitmapWithOrientationFix(this, photoFileUri, IMAGE_WIDTH_ACTUAL, IMAGE_HEIGHT_ACTUAL);

        if (requestCode == REQ_CAPTURE_PHOTO) {
            try {
                if (!isFirstImageTaken) {
                    Uri internalPhotoFileUri = MediaHelper.getExternalFileUri(this, PHOTO_FILE_NAME_1);
                    if (ImageUtils.encodeIntoStream(photoBitmap, internalPhotoFileUri)) {
                        firstImageUri = internalPhotoFileUri;
                        log.debug("encodeIntoStream()={},firstImageUri={}", true, firstImageUri.getPath());
                    }
                    isFirstImageTaken = true;
                    firstImageChangeButton.setVisibility(View.VISIBLE);
                    setThumbnailImage(firstImage, firstImageUri);
                } else {
                    Uri internalPhotoFileUri = MediaHelper.getExternalFileUri(this, PHOTO_FILE_NAME_2);
                    if (ImageUtils.encodeIntoStream(photoBitmap, internalPhotoFileUri)) {
                        secondImageUri = internalPhotoFileUri;
                        log.debug("encodeIntoStream()={},secondImageUri={}", true, secondImageUri.getPath());
                    }
                    isFirstImageTaken = false;
                    secondImageChangeButton.setVisibility(View.VISIBLE);
                    setThumbnailImage(secondImage, secondImageUri);
                }
            } catch (ExternalStorageNotAccessibleException e) {
                log.error("External Storage not Accessible", e);
            }
        }

        if (requestCode == REQ_CAPTURE_PHOTO_1) {
            Uri internalPhotoFileUri = MediaHelper.getInternalFileUri(this, PHOTO_FILE_NAME_1);
            if (ImageUtils.encodeIntoStream(photoBitmap, internalPhotoFileUri)) {
                firstImageUri = internalPhotoFileUri;
            }
            isFirstImageTaken = true;
            firstImageChangeButton.setVisibility(View.VISIBLE);
            setThumbnailImage(firstImage, firstImageUri);
        }
        if (requestCode == REQ_CAPTURE_PHOTO_2) {
            Uri internalPhotoFileUri = MediaHelper.getInternalFileUri(this, PHOTO_FILE_NAME_2);
            if (ImageUtils.encodeIntoStream(photoBitmap, internalPhotoFileUri)) {
                secondImageUri = internalPhotoFileUri;
            }
            isFirstImageTaken = false;
            secondImageChangeButton.setVisibility(View.VISIBLE);
            setThumbnailImage(secondImage, secondImageUri);
        }
        MediaHelper.deleteFile(photoFileUri);
        //takePhotoActionInput.setText(R.string.retake_photo);
    }

    private void setThumbnailImage(ImageView imageView, Uri uri) {
        if (uri != null) {
            imageView.setImageBitmap(ImageUtils.decodeFromStream(this, uri, IMAGE_WIDTH_THUMBNAIL, IMAGE_HEIGHT_THUMBNAIL));
        } else {
            imageView.setImageBitmap(null);
        }
    }

    private Bitmap getActualImageBitmap(Uri uri) {
        if (uri == null) {
            return null;
        }
        return ImageUtils.decodeFromStream(this, uri, IMAGE_WIDTH_ACTUAL, IMAGE_HEIGHT_ACTUAL);
    }

    private void showEnlargedImage(Bitmap image) {
        imageHolder.setImage(image);
        startActivity(new Intent(this, ImageViewerActivity.class));
    }

    public void geoTag(View v) {
        Location location = locationService.getLocation();
        if (location != null) {
            String locationString = String.format(getString(R.string.latitude_longitude_label), location.getLatitude(), location.getLongitude());
            geoTag.setText(locationString);
        }
    }

    private void takePhoto(int requestCode) {
        try {
            photoTaker.takePhotoFromCamera(requestCode);
        } catch (ExternalStorageNotAccessibleException e) {
            DialogBuilder.buildOkDialog(this, R.string.camera_inaccessible_msg).show();
        } catch (CameraUnavailableException e) {
            DialogBuilder.buildOkDialog(this, R.string.camera_unavailable_msg).show();
            //takePhotoActionInput.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.imageView1) {
            showEnlargedImage(getActualImageBitmap(firstImageUri));
        } else if (v.getId() == R.id.imageView2) {
            showEnlargedImage(getActualImageBitmap(secondImageUri));
        } else if (v.getId() == R.id.btn_change_image_1) {
            takePhoto(REQ_CAPTURE_PHOTO_1);
        } else if (v.getId() == R.id.btn_change_image_2) {
            takePhoto(REQ_CAPTURE_PHOTO_2);
        }
    }

    private void putExifDataToImage(Location location, Uri imageUri) {
        if (location != null) {
            try {
                ExifInterface exif = new ExifInterface(imageUri.getPath());
                double latitude = location.getLatitude();
                double longitude = location.getLongitude();

                int num1Lat = (int) Math.floor(latitude);
                int num2Lat = (int) Math.floor((latitude - num1Lat) * 60);
                double num3Lat = (latitude - ((double) num1Lat + ((double) num2Lat / 60))) * 3600000;

                int num1Lon = (int) Math.floor(longitude);
                int num2Lon = (int) Math.floor((longitude - num1Lon) * 60);
                double num3Lon = (longitude - ((double) num1Lon + ((double) num2Lon / 60))) * 3600000;

                exif.setAttribute(ExifInterface.TAG_GPS_LATITUDE, num1Lat + "/1," + num2Lat + "/1," + num3Lat + "/1000");
                exif.setAttribute(ExifInterface.TAG_GPS_LONGITUDE, num1Lon + "/1," + num2Lon + "/1," + num3Lon + "/1000");

                if (latitude > 0) {
                    exif.setAttribute(ExifInterface.TAG_GPS_LATITUDE_REF, "N");
                } else {
                    exif.setAttribute(ExifInterface.TAG_GPS_LATITUDE_REF, "S");
                }

                if (longitude > 0) {
                    exif.setAttribute(ExifInterface.TAG_GPS_LONGITUDE_REF, "E");
                } else {
                    exif.setAttribute(ExifInterface.TAG_GPS_LONGITUDE_REF, "W");
                }
                exif.setAttribute(ExifInterface.TAG_DATETIME, new Date().toString());

                exif.saveAttributes();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void uploadLater(View view) {
        saveQuestionnaire();
        Intent intent = new Intent(this, QuestionnaireListActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    private void saveQuestionnaire() {
        log.debug("saveQuestionnaire()");
        Questionnaire questionnaire = questionnaireHelper.getQuestionnaire();
        Location location = locationService.getLocation();

        if (location != null) {
            questionnaire.setLongitude(questionnaire.getLongitude());
            questionnaire.setLongitude(String.valueOf(location.getLongitude()));
            questionnaire.setLatitude(String.valueOf(location.getLatitude()));
        }
        if (firstImageUri != null) questionnaire.setImage1Url(firstImageUri.getPath());
        if (secondImageUri != null) questionnaire.setImageUrl2(secondImageUri.getPath());

        questionnaire.setMustHaveSkus(questionnaireHelper.getMustHaveSkus());
        questionnaire.setFixedDisplaySkus(questionnaireHelper.getFixedDisplaySkus());
        questionnaire.setWaveDisplays(questionnaireHelper.getWaveDisplaySkus());
        questionnaire.setPointOfPurchases(questionnaireHelper.getPointOfPurchases());
        questionnaire.setAditionalSkus(questionnaireHelper.getAdditionalSkus());
        questionnaire.setUnsuccessfulAudits(questionnaireHelper.getUnsuccessfulAudits());
        ///questionnaire.setHotSpots(questionnaireHelper.getHotSpots());

        questionnaire.setShopType(questionnaireHelper.getShopType());
        questionnaire.setDmsCode(questionnaireHelper.getStoreInfo().getDmsCode());

        log.debug("questionnaire = {}", questionnaire.toString());
        Questionnaire q = questionnaireDao.saveQuestionnaire(questionnaire);
        saveHotSpots(questionnaireHelper.getShopType().getType(), q);

        //questionnaireDao.saveHotSpots(questionnaireHelper.getHotSpots(), q);

        StoreInfo storeInfo = questionnaireHelper.getStoreInfo();
        storeInfo.setVisited(true);
        storeInfoDao.updateStoreInfo(storeInfo);

    }


    private void saveHotSpots(String shopType, Questionnaire questionnaire) {
        log.verbose("saveHotSpots() shopType={}", shopType);
        try {
            if (shopType.contains("RCS_Blue")
                    || shopType.contains("RCS_Green")
                    || shopType.contains("RCS_Purple")
                    || shopType.contains("UCS_Blue")
                    || shopType.contains("UCS_Green")
                    || shopType.contains("UCS_Purple")
                    ) {
                List<HotSpotWithSkinCare> skinCareList = (List<HotSpotWithSkinCare>) questionnaireHelper.getHotSpots();
                for (HotSpotWithSkinCare hotSpotWithSkinCare : skinCareList)
                    hotSpotWithSkinCare.setQuestionnaire(questionnaire);
                try {
                    hotSpotWithSkinCareDao.saveBulkData(skinCareList);
                } catch (SQLException e) {
                    log.error("Unable to save hotspot with skin Cares", e);
                }
            } else if (shopType.contains("UNG")
                    || shopType.contains("RNG")
                    || shopType.contains("RWMG")
                    || shopType.contains("UWMG")
                    ) {

                List<HotSpotWithCompliance> skinCareList = (List<HotSpotWithCompliance>) questionnaireHelper.getHotSpots();
                if (skinCareList == null) {
                    log.warn("skinCareList is null , so returning");
                    return;
                }

                for (HotSpotWithCompliance hotSpotWithSkinCare : skinCareList)
                    hotSpotWithSkinCare.setQuestionnaire(questionnaire);
                try {
                    hotSpotWithComplianceDao.saveBulkData(skinCareList);
                } catch (SQLException e) {
                    log.error("Unable to save hotspot with compliance", e);
                }
            } else if (shopType.contains("UGS_Green")
                    || shopType.contains("UGS_Purple")) {
                List<HotSpotWithOralCare> skinCareList = (List<HotSpotWithOralCare>) questionnaireHelper.getHotSpots();
                for (HotSpotWithOralCare hotSpotWithSkinCare : skinCareList)
                    hotSpotWithSkinCare.setQuestionnaire(questionnaire);
                try {
                    hotSpotWithOralCareDao.saveBulkData(skinCareList);
                } catch (SQLException e) {
                    log.error("Unable to save hotspot with oral care", e);
                }
            }
        } catch (Exception e) {
            log.error("Something went wrong, I knew, but only God knows", e);
        }
    }

    public void uploadNow(View view) {
        log.debug("uploadNow");


        File file1 = new File(firstImageUri.getPath());
        File file2 = new File(secondImageUri.getPath());

        if (!file1.exists() && !file2.exists()) return;

        putExifDataToImage(locationService.getLocation(), firstImageUri);
        putExifDataToImage(locationService.getLocation(), secondImageUri);

        final ProgressDialog dialog = ProgressDialog.show(this, null, null);

        webServices.uploadImage(new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(String s) {
                super.onSuccess(s);
                log.debug("image one uploaded");

                webServices.uploadImage(new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(String s) {
                        super.onSuccess(s);
                        log.debug("image two uploaded");
                        dialog.dismiss();
                    }

                    @Override
                    public void onFailure(Throwable throwable, String s) {
                        super.onFailure(throwable, s);
                        dialog.dismiss();
                    }
                }, secondImageUri);
            }

            @Override
            public void onFailure(Throwable throwable, String s) {
                super.onFailure(throwable, s);
                dialog.dismiss();
            }
        }, firstImageUri);
    }

    public void gotoMainMenu(View view) {
        Intent intent = new Intent(this, DashboardActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }
}