package com.feratech.nps.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import com.feratech.nps.R;
import com.feratech.nps.dao.*;
import com.feratech.nps.domain.Questionnaire;
import com.feratech.nps.domain.QuestionnaireMeta;
import com.feratech.nps.domain.ShopType;
import com.feratech.nps.helper.QuestionnaireHelper;
import com.feratech.nps.utils.Constants;
import com.feratech.nps.utils.Logger;
import com.google.inject.Inject;
import roboguice.activity.RoboActivity;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

/**
 * User: Bazlur Rahman Rokon
 * Date: 7/10/13 - 11:29 PM
 */


@ContentView(R.layout.store_details)
public class StoreInfoDetailsActivity extends RoboActivity {
    private static final Logger log = Logger.getLogger(StoreSelectionActivity.class);

    @Inject
    private QuestionnaireHelper questionnaireHelper;

    @InjectView(R.id.region_name)
    private TextView regionName;
    @InjectView(R.id.territory_name)
    private TextView territoryName;
    @InjectView(R.id.town_name)
    private TextView town;
    @InjectView(R.id.store_address)
    private TextView storeAddress;
    @InjectView(R.id.shop_type)
    private TextView storeType;
    @InjectView(R.id.dmsCode)
    private TextView dmsCode;
    @InjectView(R.id.store_name)
    private TextView storeName;

    @Inject
    private MustHaveSkuDao mustHaveSkuDao;
    @Inject
    private FixedDisplaySkuDao fixedDisplaySkuDao;
    @Inject
    private AdditionalSkuDao additionalSkuDao;
    @Inject
    private ShopTypeDao shopTypeDao;
    @Inject
    private QuestionSetDao questionSetDao;
    @Inject
    private QuestionMetaDao questionMetaDao;
    @Inject
    private QuestionnaireMeta questionnaireMeta;
    @Inject
    private QuestionnaireMetaDao questionnaireMetaDao;


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String shopTypeName = getIntent().getExtras().getString(Constants.SHOP_TYPE);
        String dmscode = getIntent().getExtras().getString(Constants.DMS_CODE);

        Questionnaire questionnaire = new Questionnaire();

        try {
            if (questionnaireHelper.getMustHaveSkus() != null && questionnaireHelper.getMustHaveSkus().size() > 0) {
                questionnaireHelper.getMustHaveSkus().clear();
            }

            ShopType shopType = shopTypeDao.findByType(shopTypeName);
            questionnaire.setMustHaveSkus(mustHaveSkuDao.getMustHaveSkus(shopType));

            questionnaireHelper.setShopType(shopType);
            questionnaireHelper.setQuestionMetas(questionMetaDao.getQuestionMetas(shopType));
            questionnaireHelper.setQuestionnaireMeta(questionnaireMetaDao.findByShopType(shopType));
            questionnaireHelper.setAdditionalSkus(additionalSkuDao.getAdditionalSku(shopType));
            questionnaireHelper.setMustHaveSkus(mustHaveSkuDao.getMustHaveSkus(shopType));

            questionnaire.setShopType(shopType);
            questionnaire.setQuestionnaireMeta(questionnaireMeta);
            questionnaire.setQuestionMetas(questionMetaDao.getQuestionMetas(shopType));

            questionnaireHelper.setQuestionnaire(questionnaire);
            questionnaireHelper.setDmsCode(dmscode);

            regionName.setText(questionnaireHelper.getStoreInfo().getRegion());
            territoryName.setText(questionnaireHelper.getStoreInfo().getTerritory());
            town.setText(questionnaireHelper.getStoreInfo().getTown());
            storeName.setText(questionnaireHelper.getStoreInfo().getStoreName());
            storeAddress.setText(questionnaireHelper.getStoreInfo().getStoreAddress());
            storeType.setText(questionnaireHelper.getStoreInfo().getShopType());
            dmsCode.setText(questionnaireHelper.getStoreInfo().getDmsCode());
        } catch (Exception e) {
            log.error("Something went wrong", e);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add("Go");
        return super.onCreateOptionsMenu(menu);
    }

    public void onClick(View view) {
        Intent intent = new Intent(this, QuestionnaireActivity.class);
        startActivity(intent);
    }

    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        return super.onMenuItemSelected(featureId, item);
    }
}