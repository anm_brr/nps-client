package com.feratech.nps.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import com.feratech.nps.R;
import com.feratech.nps.dao.ShopTypeDao;
import com.feratech.nps.dao.StoreInfoDao;
import com.feratech.nps.domain.StoreInfo;
import com.feratech.nps.helper.QuestionnaireHelper;
import com.feratech.nps.utils.Constants;
import com.feratech.nps.utils.Logger;
import com.feratech.nps.utils.Validator;
import com.feratech.nps.utils.view.DialogBuilder;
import com.google.inject.Inject;
import roboguice.activity.RoboActivity;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

import java.util.List;

/**
 * User: Bazlur Rahman Rokon
 * Date: 7/9/13 - 3:35 AM
 */
@ContentView(R.layout.store_selection)
public class StoreSelectionActivity extends RoboActivity {
    private static final Logger log = Logger.getLogger(StoreSelectionActivity.class);

    @Inject
    private StoreInfoDao storeInfoDao;

    @InjectView(R.id.et_region)
    private EditText region;
    @InjectView(R.id.et_territory)
    private EditText territory;
    @InjectView(R.id.et_town)
    private EditText town;
    @InjectView(R.id.et_shop_name)
    private EditText shopType;
    @InjectView(R.id.et_dms_code)
    private EditText dmsCode;

    @Inject
    private ShopTypeDao shopTypeDao;

    @Inject
    private QuestionnaireHelper questionnaireHelper;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // storeInfoDao = new StoreInfoDaoImpl(new DatabaseHelper(this));
    }

    public void searchRegion(View view) {
        showDialog(storeInfoDao.getRegion(), region, "Region");
    }

    public void searchTerritory(View view) {
        showDialog(storeInfoDao.getTerritory(region.getText().toString()), territory, "Territory");
    }

    public void searchTown(View view) {
        showDialog(storeInfoDao.getTown(territory.getText().toString()), town, "Town");
    }

    public void searchShopType(View view) {
        showDialog(shopTypeDao.getAllShopType(), shopType, "Shop Type");
    }

    public void btnCheckInClicked(View view) {
        if (!validate()) return;

        String shopTypeText = shopType.getText().toString().trim();

        log.debug("shopTypeText={}", shopTypeText);
        if (shopTypeText.contains("_")) shopTypeText = shopTypeText.substring(0, shopTypeText.indexOf('_'));
        log.debug("shopTypeText={}", shopTypeText);

        if (!storeInfoDao.isValidDmsCode(
                region.getText().toString().trim(),
                territory.getText().toString().trim(),
                town.getText().toString().trim(),
                shopTypeText,
                dmsCode.getText().toString().trim().toUpperCase())) {
            DialogBuilder.buildOkDialog(this, "Validation Error. The form values are not correct, Please check again. ").show();
            dmsCode.setError("Incorrect Information");
            return;
        } else {
            StoreInfo storeInfo = storeInfoDao.findStore(region.getText().toString().trim(),
                    territory.getText().toString().trim(),
                    town.getText().toString().trim(),
                    shopTypeText,
                    dmsCode.getText().toString().trim().toUpperCase());

            if (storeInfo != null) {
                if (storeInfo.getVisited()) {
                    DialogBuilder.buildOkDialog(this, "This store is already visited, Please try another store.").show();
                    return;
                } else {
                    questionnaireHelper.setStoreInfo(storeInfo);
                }
            }
        }

        Intent intent = new Intent(this, StoreInfoDetailsActivity.class);
        intent.putExtra(Constants.DMS_CODE, dmsCode.getText().toString());
        intent.putExtra(Constants.SHOP_TYPE, shopType.getText().toString());
        startActivity(intent);
    }

    private boolean validate() {
        log.verbose("validate()");
        String required = getString(R.string.required);
        return (Validator.validateRequired(region, required)
                & Validator.validateRequired(territory, required)
                & Validator.validateRequired(town, required)
                & Validator.validateRequired(shopType, required)
                & Validator.validateRequired(dmsCode, required)
        );
    }

    public void showDialog(List<String> list, final EditText view, String title) {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(
                this);
        builderSingle.setCancelable(false);
        builderSingle.setIcon(R.drawable.ic_launcher);
        builderSingle.setTitle("Select One " + title);
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                this,
                android.R.layout.select_dialog_singlechoice);
        arrayAdapter.addAll(list);
        builderSingle.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        builderSingle.setAdapter(arrayAdapter,
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String strName = arrayAdapter.getItem(which);
                        view.setText(strName);
                    }
                });
        builderSingle.show();
    }
}