package com.feratech.nps.activity;

/**
 * User: Bazlur Rahman Rokon
 * Date: 8/1/13 - 5:42 PM
 */
public enum SkuLayout {
    UNSUCCESSFUL_AUDIT("Unsuccessful Audit"),
    MUST_HAVE_SKU("Must Have Sku"),
    FIXED_DISPLAY_SKU("Fixed Display Sku"),
    WAVE_DISPLAY_SKU("Wave Display Sku"),
    HOT_SPOT_AND_POINT_OF_PURCHASE("Hot Spot & Point of Purchase"),
    ADDITIONAL_INFO("Additional Sku");

    private String title;

    private SkuLayout(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }
}
