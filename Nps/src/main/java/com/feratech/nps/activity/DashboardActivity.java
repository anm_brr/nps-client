package com.feratech.nps.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Spannable;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import com.feratech.nps.R;
import com.feratech.nps.dao.QuestionnaireDao;
import com.feratech.nps.dao.StoreInfoDao;
import com.feratech.nps.domain.DataVersion;
import com.feratech.nps.handler.DefaultMessageHandler;
import com.feratech.nps.service.LocationService;
import com.feratech.nps.service.WebServices;
import com.feratech.nps.utils.Logger;
import com.feratech.nps.utils.Utils;
import com.feratech.nps.utils.view.DialogBuilder;
import com.google.inject.Inject;
import roboguice.activity.RoboActivity;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

/**
 * User: Bazlur Rahman Rokon
 * Date: 7/9/13 - 1:46 AM
 */

@ContentView(R.layout.dashboard)
public class DashboardActivity extends RoboActivity {

    private static final int BACK_PRESSED_FLAG_RESET_TIME_INTERVAL_MILLIS = 3500;
    private Logger log = Logger.getLogger(DashboardActivity.class);
    @InjectView(R.id.application_name)
    private TextView applicationName;
    private boolean backButtonPressedOnce;
    @Inject
    private WebServices webServices;
    @Inject
    private QuestionnaireDao questionnaireDao;
    @Inject
    private StoreInfoDao storeInfoDao;
    @javax.inject.Inject
    private LocationService locationService;


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        log.verbose("onCreate()");

        applicationName.setText(getString(R.string.application_name), TextView.BufferType.SPANNABLE);
        Spannable spannable = (Spannable) applicationName.getText();
        StyleSpan styleSpan = new StyleSpan(Typeface.BOLD);
        spannable.setSpan(styleSpan, 0, 7, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
    }

    public void gotoQuestionnaire(View v) {
        log.verbose("gotoQuestionnaire()");
        if (!isQuestionnairesAndStoreDownloaded()) {
            DialogBuilder.buildOkDialog(this, "No StoreInfo & Questionnaires are downloaded, please download.").show();
            return;
        }
        Intent intent = new Intent(this, StoreSelectionActivity.class);
        startActivity(intent);
    }

    private boolean isQuestionnairesAndStoreDownloaded() {
        log.verbose("isQuestionnairesAndStoreDownloaded()");
        return (storeInfoDao.count() > 0 & questionnaireDao.count() > 0);
    }

    public void checkDataVersion() {
        log.verbose("checkDataVersion()");
        webServices.retrieveCurrentDataVersion(new DefaultMessageHandler(this, false) {
            @Override
            public void onSuccess(Message msg) {
                log.debug("onSuccess() ={}", msg.obj);
                DataVersion dataVersion = (DataVersion) msg.obj;

                if (dataVersion == null) {
                    DialogBuilder.buildOkDialog(DashboardActivity.this, "Something went wrong, Please contact with server administrator ").show();
                    return;
                }

                DataVersion dataVersionSaved = Utils.getDataVersion(DashboardActivity.this);
                if (dataVersion.equals(dataVersionSaved)) {
                    DialogBuilder.buildOkDialog(DashboardActivity.this, "Your data is up-to-date, no download required").show();
                    return;
                } else {
                    log.verbose("download()");
                    Utils.clearQuestionnaireFetchHistory(DashboardActivity.this);
                    Intent intent = new Intent(DashboardActivity.this, DownloadActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putParcelable(Utils.DATA_VERSION_KEY, dataVersion);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
            }
        });
    }

    public void download(View view) {
        checkDataVersion();
    }

    public void upload(View view) {
        log.verbose("upload()");
        Intent intent = new Intent(this, QuestionnaireListActivity.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        if (backButtonPressedOnce) {
            finish();
            return;
        }

        backButtonPressedOnce = true;
        Toast.makeText(this, getString(R.string.press_back_to_exit), Toast.LENGTH_LONG).show();
        resetBackPressedFlagOnIdle();
    }

    private void resetBackPressedFlagOnIdle() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                backButtonPressedOnce = false;
            }
        }, BACK_PRESSED_FLAG_RESET_TIME_INTERVAL_MILLIS);
    }

}