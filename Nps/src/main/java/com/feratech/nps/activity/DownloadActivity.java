package com.feratech.nps.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Message;
import android.provider.Settings;
import android.text.Spannable;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.view.View;
import android.widget.TextView;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.feratech.nps.R;
import com.feratech.nps.dao.QuestionnaireDao;
import com.feratech.nps.dao.StoreInfoDao;
import com.feratech.nps.db.DatabaseHelper;
import com.feratech.nps.domain.DataVersion;
import com.feratech.nps.domain.Questionnaire;
import com.feratech.nps.domain.StoreInfo;
import com.feratech.nps.handler.DefaultMessageHandler;
import com.feratech.nps.helper.DefaultAsyncTask;
import com.feratech.nps.service.WebServices;
import com.feratech.nps.utils.Constants;
import com.feratech.nps.utils.Logger;
import com.feratech.nps.utils.Utils;
import com.github.rtyley.android.sherlock.roboguice.activity.RoboSherlockActivity;
import com.google.inject.Inject;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

import java.io.*;
import java.nio.channels.FileChannel;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * User: Bazlur Rahman Rokon
 * Date: 7/30/13 - 7:50 AM
 */
@ContentView(R.layout.download)
public class DownloadActivity extends RoboSherlockActivity {
    private static final int SEND_EMAIL = 12;
    private static final int BACKUP_DB = 13;
    private Logger log = Logger.getLogger(DownloadActivity.class);
    @InjectView(R.id.application_name)
    private TextView applicationName;
    @Inject
    private StoreInfoDao storeInfoDao;
    @Inject
    private WebServices webServices;
    @Inject
    private DatabaseHelper databaseHelper;
    @Inject
    private QuestionnaireDao questionnaireDao;
    private DataVersion dataVersion;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        dataVersion = getIntent().getExtras().getParcelable(Utils.DATA_VERSION_KEY);

        applicationName.setText(getString(R.string.application_name), TextView.BufferType.SPANNABLE);
        Spannable spannable = (Spannable) applicationName.getText();
        StyleSpan styleSpan = new StyleSpan(Typeface.BOLD);
        spannable.setSpan(styleSpan, 0, 7, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

//        log.debug("attempting to delete previous database ...");
//        backupDatabase();
//
//        this.deleteDatabase(Constants.DATABASE_NAME);
//        //creating new database again
//        SQLiteDatabase sqLiteDatabase = this.openOrCreateDatabase(Constants.DATABASE_NAME, MODE_PRIVATE, null);
//
//        sqLiteDatabase.close();
    }

    public void downloadStores(View view) {
        webServices.retrieveStoreList(new DefaultMessageHandler(this, true) {
            @Override
            public void onSuccess(Message msg) {
                log.debug("retrieveStoreList onSuccess. storeListSize={}", ((List) msg.obj).size());
                //ObjectSerializer.write(DownloadActivity.this, (List<StoreInfo>) msg.obj);
                try {
                    //clearAllStoreInfo();
                    insertStoreInfoIntoDatabase((List<StoreInfo>) msg.obj);
                    Utils.storeFetched(DownloadActivity.this);
                } catch (Exception e) {
                    log.error("something went wrong while clearing stores or inserting store", e);
                    Utils.clearStoreFetchHistory(DownloadActivity.this);
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, BACKUP_DB, 0, "Backup Database");
        menu.add(0, SEND_EMAIL, 0, "Send Logs To Developer");
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == BACKUP_DB) {
            backupDatabase();
        } else if (item.getItemId()
                == SEND_EMAIL) {
            sendEmailToDeveloper();
        }
        return super.onOptionsItemSelected(item);
    }

    private void sendEmailToDeveloper() {
        log.debug("sendEmailToDeveloper()");
        File dir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath() + "/backup/logs/");
        File file = new File(dir, "log.txt");

        Uri uri = Uri.parse("file://" + file.getAbsolutePath());

        String android_id = Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID);

        Intent email = new Intent(Intent.ACTION_SEND);
        email.putExtra(Intent.EXTRA_EMAIL, new String[]{"anmbrr.bit0112@gmail.com"});
        email.putExtra(Intent.EXTRA_SUBJECT, "Perfect Store Logs Device ID: " + android_id);
        email.putExtra(Intent.EXTRA_TEXT, "Perfect Store App Logs sent from Device: " + android_id);
        email.putExtra(Intent.EXTRA_STREAM, uri);
        email.setType("message/rfc822");
        startActivity(Intent.createChooser(email, "Choose an Email client :"));
    }

    public File getCurrentDatabaseFile() {
        log.debug("getCurrentDatabaseFile() in location = /data/data/{}/databases/{} ", this.getPackageName(), Constants.DATABASE_NAME);
        return new File("/data/data/" + this.getPackageName() + "/databases/", Constants.DATABASE_NAME);
    }

    public File getBackupDatabaseFile() {
        log.debug("getBackupDatabaseFile()");
        File dir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath() + "/backup/database/");
        if (!dir.exists()) {
            log.debug("Dir doesn't exist-- creating dir");
            dir.mkdirs();
        }
        Date now = new Date();
        String android_id = Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yy-hh-mm-ss");
        return new File(dir, android_id + "-" + format.format(now) + "-" + Constants.DATABASE_NAME);
    }

    public final boolean backupDatabase() {
        log.debug("backupDatabase() ->going to backup database");
        File from = this.getCurrentDatabaseFile();
        File to = this.getBackupDatabaseFile();
        try {
            // copyFile(from, to);
            copyDataBase(from, to);
            return true;
        } catch (IOException e) {
            log.error("Error backuping up database:{} ", e.getMessage(), e);
        }
        return false;
    }

    public void copyFile(File src, File dst) throws IOException {
        log.debug("source={}, dest={}", src.getPath(), dst.getPath());
        log.debug("copyFile()");
        FileInputStream in = new FileInputStream(src);
        FileOutputStream out = new FileOutputStream(dst);
        FileChannel fromChannel = null, toChannel = null;
        try {
            fromChannel = in.getChannel();
            toChannel = out.getChannel();
            fromChannel.transferTo(0, fromChannel.size(), toChannel);
        } finally {
            if (fromChannel != null)
                fromChannel.close();
            if (toChannel != null)
                toChannel.close();
        }
    }

    private void copyDataBase(File src, File dest)
            throws IOException {
        log.debug("scopyDataBase={}, dest={}", src.getPath(), dest.getPath());
        InputStream myInput = new FileInputStream(src);

        OutputStream myOutput = new FileOutputStream(dest);

        byte[] buffer = new byte[1024];
        int length;
        while ((length = myInput.read(buffer)) > 0) {
            myOutput.write(buffer, 0, length);
        }

        myOutput.flush();
        myOutput.close();
        myInput.close();

    }

    public void downloadQuestion(View view) {
        log.debug("downloadQuestion");
        webServices.retrieveQuestionnaire(new DefaultMessageHandler(this, true) {
            @Override
            public void onSuccess(Message msg) {
                List<Questionnaire> questionnaires = (List<Questionnaire>) msg.obj;
                log.debug("retrieveQuestionnaire() ={}", questionnaires.size());

                new DefaultAsyncTask<List<Questionnaire>, Void, Void>(DownloadActivity.this) {

                    @Override
                    protected Void doInBackground(List<Questionnaire>... lists) {
                        try {
                            List<Questionnaire> questionnaires = lists[0];
                            //  questionnaireDao.deleteQuestionnaire();
                            questionnaireDao.saveQuestionnaires(questionnaires);
                            Utils.questionnaireFetched(DownloadActivity.this);
                        } catch (Exception e) {
                            log.error("Something went wrong while  saving questionnaire", e);
                            Utils.clearQuestionnaireFetchHistory(DownloadActivity.this);
                        }
                        return null;
                    }
                }.execute(questionnaires);
            }
        });
    }

    private void insertStoreInfoIntoDatabase(List<StoreInfo> storeInfoList) {
        log.debug("insertStoreInfoIntoDatabase()");
        new DefaultAsyncTask<List<StoreInfo>, Void, Void>(this) {
            @Override
            protected Void doInBackground(List<StoreInfo>... lists) {
                List<StoreInfo> storeInfoList = lists[0];
                try {
                    storeInfoDao.insertBulkData(storeInfoList);
                } catch (SQLException e) {
                    log.error("Unable to insertBulkData", e);
                }
                return null;
            }
        }.execute(storeInfoList);
    }

    private void clearAllStoreInfo() {
        log.debug("clearAllStoreInfo()");
        storeInfoDao.clearAll();
    }

    @Override
    protected void onPause() {
        super.onPause();
        log.debug("onPause()");
        if (Utils.isQuestionnaireAndStoreFetched(this)) {
            Utils.saveDataVersion(this, dataVersion);
        } else {
        }
    }
}