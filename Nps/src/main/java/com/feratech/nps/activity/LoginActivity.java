package com.feratech.nps.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Spannable;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import com.crittercism.app.Crittercism;
import com.feratech.nps.R;
import com.feratech.nps.service.LocationService;
import com.feratech.nps.utils.Validator;
import com.feratech.nps.utils.view.DialogBuilder;
import com.google.inject.Inject;
import roboguice.activity.RoboActivity;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

/**
 * User: Bazlur Rahman Rokon
 * Date: 7/5/13 - 7:23 AM
 */
@ContentView(R.layout.login)
public class LoginActivity extends RoboActivity {
    private static final String ADMIN = "admin";
    private static final String PASSWORD = "1234";

    @InjectView(R.id.application_name)
    private TextView applicationName;

    @InjectView(R.id.user_name)
    private EditText etUserName;
    @InjectView(R.id.password)
    private EditText etPassword;

    @Inject
    private LocationService locationService;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Crittercism.initialize(getApplicationContext(), "52485739a7928a48a7000002");

        applicationName.setText(getString(R.string.application_name), TextView.BufferType.SPANNABLE);
        Spannable spannable = (Spannable) applicationName.getText();
        StyleSpan styleSpan = new StyleSpan(Typeface.BOLD);
        spannable.setSpan(styleSpan, 0, 7, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
    }

    public void onLoginButtonClick(View v) {
        if (!validate()) {
            return;
        } else if (!login()) {
            DialogBuilder.buildOkDialog(this, "Incorrect username/password").show();
            return;
        }

        Intent intent = new Intent(this, DashboardActivity.class);
        startActivity(intent);
        finish();
    }

    private boolean login() {
        if (etPassword.getText().toString().equals(PASSWORD) && etUserName.getText().toString().equals(ADMIN)) {
            return true;
        }
        return false;
    }

    private boolean validate() {
        String required = getString(R.string.required);
        return (Validator.validateRequired(etUserName, required)
                & Validator.validateRequired(etPassword, required)
        );
    }
}