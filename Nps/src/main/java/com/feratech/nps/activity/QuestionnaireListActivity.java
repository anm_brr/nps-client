package com.feratech.nps.activity;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.actionbarsherlock.view.MenuItem;
import com.feratech.nps.R;
import com.feratech.nps.dao.*;
import com.feratech.nps.db.DatabaseHelper;
import com.feratech.nps.domain.HotSpot;
import com.feratech.nps.domain.Questionnaire;
import com.feratech.nps.domain.StoreInfo;
import com.feratech.nps.domain.converter.ModelConverter;
import com.feratech.nps.domain.json.QuestionnaireJsonModel;
import com.feratech.nps.handler.DefaultMessageHandler;
import com.feratech.nps.service.WebServices;
import com.feratech.nps.utils.Logger;
import com.feratech.nps.utils.media.ImageLoader;
import com.feratech.nps.utils.view.DialogBuilder;
import com.github.rtyley.android.sherlock.roboguice.activity.RoboSherlockActivity;
import com.google.inject.Inject;
import org.codehaus.jackson.map.ObjectMapper;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * User: Bazlur Rahman Rokon
 * Date: 8/5/13 - 11:52 PM
 */

@ContentView(R.layout.list_view)
public class QuestionnaireListActivity extends RoboSherlockActivity {
    private static final Logger log = Logger.getLogger(QuestionnaireListActivity.class);

    public static final int MENU_ITEM_UPLOADED = 11;
    public static final int MENU_ITEM_NOT_UPLOADED = 12;
    public static final int MENU_ITEM_ALL = 13;
    public static final int MENU_ITEM_UPLOAD = 21;

    public static final int NONE = -1;

    public static final int MENU_ITEM_ID_SEARCH = 10;

    public static final int GROUP_ID_NEVER_SHOW = 100;
    public static final int GROUP_ID_SHOW_ALWAYS = 101;

    private SimpleDateFormat dateFormat;

    @Inject
    private QuestionnaireDao questionnaireDao;

    @Inject
    private StoreInfoDao storeInfoDao;

    private QuestionnaireAdapter adapter;

    @InjectView(R.id.tv_nothing_found)
    private TextView nothingFoundTextView;

    @InjectView(R.id.list_view)
    private ListView listViewQuestionnaire;
    private List<Questionnaire> questionnaires = new ArrayList<Questionnaire>();

    @Inject
    private WebServices webServices;

    @Inject
    private DatabaseHelper databaseHelper;

    @Inject
    private HotSpotWithComplianceDao hotSpotWithComplianceDao;
    @Inject
    private HotSpotWithSkinCareDao hotSpotWithSkinCaresDao;
    @Inject
    private HotSpotWithOralCareDao hotSpotWithOralCareDao;

    private Questionnaire questionnaire;
    private Context context;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        log.debug("onCreate()");

        context = this;
        listViewQuestionnaire.setEmptyView(nothingFoundTextView);
        //registerForContextMenu(listViewQuestionnaire);
        dateFormat = new SimpleDateFormat(getString(R.string.date_format));
        new QuestionnaireAsyncTask().execute(MENU_ITEM_NOT_UPLOADED);

        WifiManager.WifiLock mWifiLock = null;
        WifiManager wm = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        mWifiLock = wm.createWifiLock(WifiManager.WIFI_MODE_FULL_HIGH_PERF, "MyLock");
        if (!mWifiLock.isHeld()) {
            mWifiLock.acquire();
        }

        listViewQuestionnaire.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                uploadSingleItem(questionnaires.get(position));
            }
        });
    }

    public File resize(String uri) {
        log.debug("resize()");

        Bitmap b = BitmapFactory.decodeFile(uri);
        int height = b.getHeight();
        int width = b.getWidth();

        log.debug("resize height={} width={}  width={}", height, width);

        int w = 480;
        int h = (height * w) / width;

        log.debug("resize height={} width={}", h, w);

        Bitmap out = Bitmap.createScaledBitmap(b, w, h, false);
        File f = new File(uri);
        f.delete();

        log.debug(f.getName() + "  " + f.getAbsolutePath());

        // File file = new File(dir, f.getName());
        FileOutputStream fOut;
        try {
            fOut = new FileOutputStream(f);
            out.compress(Bitmap.CompressFormat.JPEG, 100, fOut);
            fOut.flush();
            fOut.close();
            b.recycle();
            out.recycle();

        } catch (Exception e) { // TODO
            log.error("something went wrong here", e);
        }
        return f;
    }

    private void uploadSingleItem(final Questionnaire questionnaire) {
        log.debug("uploadSingleItem()");

        if (questionnaire.isSynced()) {
            DialogBuilder.buildOkDialog(context, "This shop already uploaded.").show();
            return;
        }


        DialogBuilder.buildYesNoDialog(context, "are  you sure you want to upload this ? ", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                webServices.uploadQuestionnaire(convertSingle(questionnaire), questionnaire.getShopType().getType(), new DefaultMessageHandler(context, false) {
                    @Override
                    public void onSuccess(Message msg) {
                        try {
                            final File file1 = resize(questionnaire.getImage1Url());
                            final File file2 = resize(questionnaire.getImageUrl2());

                            if (!file1.exists() && !file2.exists()) {
                                log.debug(" file not exist so, going  off here and calling next");

                                return;
                            }

                            webServices.saveImage(file1, new DefaultMessageHandler(QuestionnaireListActivity.this, false) {
                                @Override
                                public void onSuccess(Message msg) {
                                    webServices.saveImage(file2, new DefaultMessageHandler(QuestionnaireListActivity.this, false) {
                                        @Override
                                        public void onSuccess(Message msg) {

                                            try {
                                                Questionnaire questionnaire1 = questionnaire;
                                                questionnaire1.setSynced(true);
                                                questionnaireDao.updateQuestionnaire(questionnaire1);
                                                showNotification(questionnaire1);
                                            } catch (Exception e) {
                                                log.error("Something went wrong here", e);
                                            }
                                        }
                                    });
                                }
                            });
                        } catch (Exception e) {
                            log.error("Unable to upload images", e);
                        }
                    }
                });
            }
        }).show();
    }

    @Override
    public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
        menu.add(GROUP_ID_NEVER_SHOW, MENU_ITEM_UPLOADED, 0, "Stores Uploaded")
                .setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);

        menu.add(GROUP_ID_NEVER_SHOW, MENU_ITEM_NOT_UPLOADED, 1, "Store Not Uploaded")
                .setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);

        menu.add(GROUP_ID_NEVER_SHOW, MENU_ITEM_ALL, 2, "All Visited Store")
                .setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);

        menu.add(GROUP_ID_SHOW_ALWAYS, MENU_ITEM_UPLOAD, 1, "Upload")
                .setIcon(R.drawable.upload)
                .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS | MenuItem.SHOW_AS_ACTION_WITH_TEXT);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(com.actionbarsherlock.view.MenuItem item) {
        switch (item.getItemId()) {
            case MENU_ITEM_UPLOADED:
                findUploadedStores();
                break;
            case MENU_ITEM_NOT_UPLOADED:
                findNotUploadedStores();
                break;
            case MENU_ITEM_ALL:
                findAllStores();
                break;
            case MENU_ITEM_UPLOAD:
                final List<Questionnaire> list = questionnaireDao.getSavedQuestionnaire(QuestionnaireListActivity.MENU_ITEM_NOT_UPLOADED);

                uploadQuestionnaireRecursively(convert(list), list, 0);
//                Intent service = new Intent(this, QuestionnaireUploadService.class);
//                startService(service);
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void upload() {

    }

    private List<QuestionnaireJsonModel> convert(List<Questionnaire> list) {
        log.verbose("convert={} list.size()={}", list.size());
        List<QuestionnaireJsonModel> questionnaireJsonModels = new ArrayList<QuestionnaireJsonModel>();

        for (Questionnaire questionnaire : list) {
            questionnaireJsonModels.add(convertSingle(questionnaire));
        }
        return questionnaireJsonModels;
    }

    private QuestionnaireJsonModel convertSingle(Questionnaire questionnaire) {
        QuestionnaireJsonModel jsonModel = new QuestionnaireJsonModel();
        jsonModel.setLongitude(questionnaire.getLongitude());
        jsonModel.setLatitude(questionnaire.getLatitude());
        jsonModel.setCellPhone(questionnaire.getCellPhone());
        jsonModel.setDateTime(questionnaire.getDateTime());
        jsonModel.setDmsCode(questionnaire.getDmsCode());
        jsonModel.setNameOfShopOwnerOrSalesMan(questionnaire.getNameOfShopOwnerOrSalesMan());

        try {
            jsonModel.setHotSpots(ModelConverter.convertToHotSpotJson(getHotSpot(questionnaire)));
        } catch (SQLException e) {
            log.error("Unable to convert hotspot", e);
        }

        jsonModel.setAditionalSkus(ModelConverter.convertAdditionalSkuToJson(questionnaire.getAditionalSkus()));
        jsonModel.setMustHaveSkus(ModelConverter.convertMustHaveSkuToJson(questionnaire.getMustHaveSkus()));
        jsonModel.setWaveDisplays(ModelConverter.convertWaveDisplaySkuToJson(questionnaire.getWaveDisplays()));
        jsonModel.setPointOfPurchases(ModelConverter.convertPointOfPurchaseToJson(questionnaire.getPointOfPurchases()));
        jsonModel.setFixedDisplaySkus(ModelConverter.convertFixedDisplaySkuToJsons(questionnaire.getFixedDisplaySkus()));
        jsonModel.setUnsuccessfulAudits(ModelConverter.convertUnsuccessfulAuditToJson(questionnaire.getUnsuccessfulAudits()));

        return jsonModel;
    }

    private List<? extends HotSpot> getHotSpot(Questionnaire questionnaire) throws SQLException {
        log.verbose("saveHotSpots()");
        List list = null;
        String shopType = questionnaire.getShopType().getType();
        if (shopType.contains("RCS_Blue")
                || shopType.contains("RCS_Green")
                || shopType.contains("RCS_Purple")
                || shopType.contains("UCS_Blue")
                || shopType.contains("UCS_Green")
                ) {
            list = hotSpotWithSkinCaresDao.getDao().queryBuilder().where().eq("questionnaire_id", questionnaire.getId()).query();
        } else if (shopType.contains("UNG")
                || shopType.contains("RNG")
                || shopType.contains("RWMG")
                || shopType.contains("UWMG")
                ) {
            list = hotSpotWithComplianceDao.getDao().queryBuilder().where().eq("questionnaire_id", questionnaire.getId()).query();
        } else if (shopType.contains("UGS_Green")
                || shopType.contains("UGS_Purple")) {
            list = hotSpotWithOralCareDao.getDao().queryBuilder().where().eq("questionnaire_id", questionnaire.getId()).query();
        }
        return list;
    }

    private void findAllStores() {
        new QuestionnaireAsyncTask().execute(MENU_ITEM_ALL);
    }

    private void findUploadedStores() {
        new QuestionnaireAsyncTask().execute(MENU_ITEM_UPLOADED);
    }

    private void findNotUploadedStores() {
        new QuestionnaireAsyncTask().execute(MENU_ITEM_NOT_UPLOADED);
    }

    private void uploadQuestionnaireRecursively(final List<QuestionnaireJsonModel> list, final List<Questionnaire> questionnaires, final int currentIndex) {
        log.verbose("uploadQuestionnaireRecursively() list.size()={}, questionnaires.size()={},currentIndex={}", list.size(), questionnaires.size(), currentIndex);
        int size = list.size();
        if (size <= currentIndex) return;

        //writeJsonInFile(list.get(currentIndex));

        webServices.uploadQuestionnaire(list.get(currentIndex), questionnaires.get(currentIndex).getShopType().getType(), new DefaultMessageHandler(this, false) {
            @Override
            public void onSuccess(Message msg) {
                try {
                    final Questionnaire questionnaire = questionnaires.get(currentIndex);

                    final File file1 = new File(questionnaire.getImage1Url());
                    final File file2 = new File(questionnaire.getImageUrl2());

                    if (!file1.exists() && !file2.exists()) {
                        log.debug(" file not exist so, going  off here and calling next");
                        uploadQuestionnaireRecursively(list, questionnaires, (currentIndex + 1));
                    }

                    webServices.saveImage(file1, new DefaultMessageHandler(QuestionnaireListActivity.this, false) {
                        @Override
                        public void onSuccess(Message msg) {
                            webServices.saveImage(file2, new DefaultMessageHandler(QuestionnaireListActivity.this, false) {
                                @Override
                                public void onSuccess(Message msg) {

                                    Questionnaire questionnaire1 = questionnaire;
                                    questionnaire1.setSynced(true);
                                    questionnaireDao.updateQuestionnaire(questionnaire1);
                                    showNotification(questionnaire1);
                                    // uploadQuestionnaireRecursively(list, questionnaires, (currentIndex + 1));
                                }
                            });
                        }
                    });
                } catch (Exception e) {
                    log.error("Unable to upload images", e);
                }
            }
        });
    }

    private void writeJsonInFile(QuestionnaireJsonModel jsonModel) {
        log.verbose("writeJsonInFile()");
        File dir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath() + "/backup/json/");
        if (!dir.exists()) {
            log.debug("Dir doesn't exist-- creating dir");
            dir.mkdirs();
        }
        String fileName = "question.json";
        try {
            new ObjectMapper().writeValue(new File(dir + "/" + fileName), jsonModel);
        } catch (IOException e) {
            log.error("unable to parse jsonModel", e);
        }
    }

    private void showNotification(Questionnaire questionnaire) {
        log.verbose("showNotification()");
        try {
            //Bitmap bitmap = ImageUtils.decodeFromStream(this, Uri.parse(questionnaire.getImage1Url()), 96, 96);
            StoreInfo storeInfo = storeInfoDao.getStoreInfo(questionnaire.getDmsCode());
            String title = (storeInfo.getStoreName() + ", " + storeInfo.getStoreAddress() + " Uploaded Successfully");
            String message = storeInfo.getRegion() + ", " + storeInfo.getTerritory() + ", " + storeInfo.getTown();
            message += questionnaire.getDateTime() != null ? "\n" + "Audit Date & Time : " + dateFormat.format(questionnaire.getDateTime()) + " " : "";

            Toast.makeText(this, title + "\n" + message, Toast.LENGTH_SHORT).show();

//            Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//
////            Notification notification = new Notification.Builder(this)
////                    .setContentTitle(title)
////                    .setStyle(new Notification.BigTextStyle().bigText(message))
////                    .setLargeIcon(bitmap)
////                    .setSmallIcon(R.drawable.ic_launcher)
////                    .setLights(Color.BLUE, 500, 500)
////                    .setSound(alarmSound)
////                    .build();
//
//            Notification notification = new Notification.BigTextStyle(
//                    new Notification.Builder(this)
//                            .setContentTitle(title)
//                            .setLargeIcon(bitmap)
//                            .setSmallIcon(R.drawable.ic_launcher)
//                            .setLights(Color.BLUE, 500, 500)
//                            .setSound(alarmSound))
//                    .bigText(message)
//                    .build();
//
//
//            NotificationManager mNotificationManager =
//                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//            mNotificationManager.notify(id, notification);
        } catch (Exception e) {
            log.error("unable to show notification, something wen't wrong here", e);
        }
    }

    private boolean uploadImages() {
        final File file1 = new File(questionnaire.getImage1Url());
        final File file2 = new File(questionnaire.getImageUrl2());

        if (!file1.exists() && !file2.exists()) {
            log.debug(" file not exist so, going  off here");
            return true;
        }

        webServices.saveImage(file1, new DefaultMessageHandler(this, false) {
            @Override
            public void onSuccess(Message msg) {
                webServices.saveImage(file2, new DefaultMessageHandler(QuestionnaireListActivity.this, false) {
                    @Override
                    public void onSuccess(Message msg) {
                        changeSyncFlag(questionnaire);
                    }
                });
            }
        });

        return false;
    }

    private void changeSyncFlag(Questionnaire questionnaire) {
        Toast.makeText(QuestionnaireListActivity.this, "Questionnaire uploaded", Toast.LENGTH_SHORT).show();
        questionnaire.setSynced(true);
        questionnaireDao.updateQuestionnaire(questionnaire);

        questionnaires.clear();
        questionnaires.addAll(questionnaireDao.getSavedQuestionnaire(MENU_ITEM_ALL));
        adapter.clear();
        adapter.addAll(questionnaire);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, DashboardActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    class QuestionnaireAsyncTask extends AsyncTask<Integer, Void, List<Questionnaire>> {
        ProgressDialog progressDialog;

        @Override
        protected List<Questionnaire> doInBackground(Integer... integers) {
            Integer integer = integers[0];
            List<Questionnaire> questionnairesList = questionnaireDao.getSavedQuestionnaire(integer);
            log.verbose("QuestionnaireAsyncTask -- doInBackground() questionnairesList.size()={}", questionnairesList.size());
            return questionnairesList;
        }

        @Override
        protected void onPreExecute() {
            progressDialog = ProgressDialog.show(QuestionnaireListActivity.this, null, getString(R.string.loading));
        }

        @Override
        protected void onPostExecute(List<Questionnaire> questionnaireList) {
            if (adapter == null) {
                questionnaires = questionnaireList;
                adapter = new QuestionnaireAdapter(QuestionnaireListActivity.this, R.layout.list_view_single_item, questionnaireList);
                listViewQuestionnaire.setAdapter(adapter);
            } else {
                adapter.clear();
                adapter.addAll(questionnaireList);
                adapter.notifyDataSetChanged();
            }
            progressDialog.dismiss();
        }
    }

    class QuestionnaireAdapter extends ArrayAdapter<Questionnaire> {
        private Context context;
        private int resourceId;
        private ImageLoader imageLoader;

        public QuestionnaireAdapter(Context context, int textViewResourceId, List objects) {
            super(context, textViewResourceId, objects);
            this.context = context;
            this.resourceId = textViewResourceId;
            imageLoader = new ImageLoader(context);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater li = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            ViewHolder viewHolder;

            convertView = li.inflate(this.resourceId, null);
            viewHolder = new ViewHolder();
            viewHolder.title = (TextView) convertView.findViewById(R.id.tv_title);
            viewHolder.tvMessage = (TextView) convertView.findViewById(R.id.tv_message);
            viewHolder.storeImage = (ImageView) convertView.findViewById(R.id.shop_image);
            convertView.setTag(viewHolder);

            Questionnaire questionnaire = getItem(position);
            if (questionnaire == null) return convertView;

            log.debug("Questionnaire={}", questionnaire);

            StoreInfo storeInfo = storeInfoDao.getStoreInfo(questionnaire.getDmsCode());
            if (storeInfo != null) {
                imageLoader.displayImage(questionnaire.getImage1Url(), viewHolder.storeImage);
                viewHolder.title.setText(storeInfo.getStoreName() + ", " + storeInfo.getStoreAddress());
                String message = storeInfo.getRegion() + ", " + storeInfo.getTerritory() + ", " + storeInfo.getTown();
                message += questionnaire.getDateTime() != null ? "\n" + "Audit Date & Time : " + dateFormat.format(questionnaire.getDateTime()) + " " : "";
                viewHolder.tvMessage.setText(message);
            }
            return convertView;
        }

        class ViewHolder {
            public TextView title;
            public TextView tvMessage;
            public ImageView storeImage;
        }
    }
}