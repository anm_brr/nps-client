package com.feratech.nps.activity;

import android.os.Bundle;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.feratech.nps.utils.media.ImageHolder;
import com.google.inject.Inject;
import roboguice.activity.RoboActivity;
import uk.co.senab.photoview.PhotoViewAttacher;

/**
 * User: Bazlur Rahman Rokon
 * Date: 7/7/13 - 4:41 PM
 */
public class ImageViewerActivity extends RoboActivity {
    @Inject
    private ImageHolder imageHolder;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(createImageView());
    }

    private ImageView createImageView() {
        ImageView imageView = new ImageView(this);

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        params.gravity = Gravity.CENTER;

        imageView.setLayoutParams(params);
        imageView.setImageBitmap(imageHolder.getImage());

        new PhotoViewAttacher(imageView);

        return imageView;
    }

    @Override
    public void finish() {
        imageHolder.setImage(null);
        super.finish();
    }
}
