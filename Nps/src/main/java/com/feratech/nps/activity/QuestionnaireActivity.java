package com.feratech.nps.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.Menu;
import android.view.MenuItem;
import com.feratech.nps.R;
import com.feratech.nps.fragments.*;
import com.feratech.nps.helper.QuestionnaireHelper;
import com.feratech.nps.utils.Logger;
import com.feratech.nps.utils.view.DialogBuilder;
import com.feratech.nps.views.NonSwipeableViewPager;
import com.google.inject.Inject;
import com.viewpagerindicator.TabPageIndicator;
import roboguice.activity.RoboFragmentActivity;
import roboguice.fragment.RoboFragment;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * User: Bazlur Rahman Rokon
 * Date: 7/10/13 - 7:59 PM
 */
@ContentView(R.layout.questionnaire)
public class QuestionnaireActivity extends RoboFragmentActivity {
    private static final Logger log = Logger.getLogger(QuestionnaireActivity.class);

    @Inject
    private QuestionnaireHelper questionnaireHelper;

    private boolean[] errors = {false, false, false, false, false, false};

    private Map<SkuLayout, RoboFragment> CONTENT;

    private static final int MENU_ITEM_NEXT = 1;
    private static final int MENU_ITEM_BACK = 2;

    @InjectView(R.id.pager)
    private NonSwipeableViewPager viewPager;

    @InjectView(R.id.indicator)
    private TabPageIndicator indicator;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getActionBar().setTitle("Questionnaire");
        getActionBar().setSubtitle(questionnaireHelper.getStoreInfo().getStoreName() + ", " + questionnaireHelper.getStoreInfo().getStoreAddress());

        initiate();

        FragmentPagerAdapter adapter = new FragmentAdapter(getSupportFragmentManager());

        viewPager.setAdapter(adapter);
        indicator.setViewPager(viewPager);
        indicator.setEnabled(false);

    }

    private void initiate() {
        CONTENT = new LinkedHashMap<SkuLayout, RoboFragment>();

        CONTENT.put(SkuLayout.UNSUCCESSFUL_AUDIT, new UnsuccessfulAuditFragment());
        CONTENT.put(SkuLayout.FIXED_DISPLAY_SKU, new FixedDisplayFragment());

        if (questionnaireHelper.getQuestionnaireMeta().getWaveDisplayQuestionSize() > 0) {
            CONTENT.put(SkuLayout.WAVE_DISPLAY_SKU, new WaveDisplayFragment());
        }
        CONTENT.put(SkuLayout.HOT_SPOT_AND_POINT_OF_PURCHASE, new HotSpotAndPointOfPurchaseFragment());
        CONTENT.put(SkuLayout.ADDITIONAL_INFO, new AdditionalInfoFragment());
        CONTENT.put(SkuLayout.MUST_HAVE_SKU, new MustHaveSkuFragment());


//        if (questionnaireMeta.getWaveDisplayQuestionSize() > 0) {
//            intent = new Intent(this, WaveDisplayActivity.class);
//        } else {
//            intent = new Intent(this, HotSpotAndPointOfPurchaseActivity.class);
//        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, MENU_ITEM_BACK, 0, "Back")
                .setIcon(R.drawable.navigation_back)
                .setShowAsAction(MenuItem.SHOW_AS_ACTION_WITH_TEXT | MenuItem.SHOW_AS_ACTION_ALWAYS);

        menu.setQwertyMode(true);
        menu.add(0, MENU_ITEM_NEXT, 0, "Next")
                .setIcon(R.drawable.navigation_forward)
                .setShowAsAction(MenuItem.SHOW_AS_ACTION_WITH_TEXT | MenuItem.SHOW_AS_ACTION_ALWAYS);
        return true;
    }

    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        log.debug("onMenuItemSelected()");

        if (item.getItemId() == MENU_ITEM_BACK) {
            log.debug(" MENU_ITEM_BACK selected, viewPager.getCurrentItem()={}", viewPager.getCurrentItem());

            if (viewPager.getCurrentItem() > 0 && viewPager.getCurrentItem() <= (CONTENT.size() - 1)) {
                viewPager.setCurrentItem((viewPager.getCurrentItem() - 1));
            }

        } else if (item.getItemId() == MENU_ITEM_NEXT) {
            log.debug(" MENU_ITEM_NEXT selected, viewPager.getCurrentItem()={}", viewPager.getCurrentItem());

            int currentItem = viewPager.getCurrentItem();

            if (!isValid(currentItem)) {
                return false;
            }

            if (viewPager.getCurrentItem() >= 0 && viewPager.getCurrentItem() < (CONTENT.size() - 1)) {
                viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
                return true;
            }
            if (viewPager.getCurrentItem() == (CONTENT.size() - 1) & isValid(currentItem)) {
                DialogBuilder.buildYesNoDialog(this, "Are you sure all the data you have inserted are correct? ", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        startPhotoActivity();
                    }
                }).show();
            }
        }
        return true;
    }

    private void startPhotoActivity() {
        Intent intent = new Intent(this, CameraActivity.class);
        startActivity(intent);
    }

    private boolean isValid(int currentItem) {
        log.debug("isValid() ={}", currentItem);
        boolean isValid = true;

        try {
            List<SkuLayout> list = new ArrayList(CONTENT.keySet());
            SkuLayout skuLayout = list.get(currentItem);

            Fragment fragment = CONTENT.get(skuLayout);

            if (fragment instanceof Validatable) {
                log.verbose("fragment of currentItem [{}] item instanceof Validatable ", currentItem);
                return ((Validatable) fragment).validate();
            }
        } catch (Exception e) {
            log.error("Something went wrong here, I knew but now only God knows", e);
        }
        log.verbose("isValid() isValid={}", isValid);
        return isValid;
    }

    private class FragmentAdapter extends FragmentPagerAdapter {
        public FragmentAdapter(FragmentManager supportFragmentManager) {
            super(supportFragmentManager);
        }

        @Override
        public Fragment getItem(int i) {
            List<SkuLayout> keys = new ArrayList<SkuLayout>(CONTENT.keySet());
            return CONTENT.get(keys.get(i));
        }

        @Override
        public CharSequence getPageTitle(int position) {
            List<SkuLayout> keys = new ArrayList<SkuLayout>(CONTENT.keySet());
            return keys.get(position).getTitle();
        }

        @Override
        public int getCount() {
            return CONTENT.size();
        }
    }

    @Override
    public void onBackPressed() {
        DialogBuilder.buildYesNoDialog(this, "Are you sure you want to go back? You will lose all the data that was collected in this questionnaire.", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        }).show();
    }
}