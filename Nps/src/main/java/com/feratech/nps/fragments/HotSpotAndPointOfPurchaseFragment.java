package com.feratech.nps.fragments;

import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import com.feratech.nps.R;
import com.feratech.nps.adapter.HotSpotWithComplianceAdapter;
import com.feratech.nps.adapter.HotSpotWithSkinCareAdapter;
import com.feratech.nps.adapter.HotspotWithOralCareAdapter;
import com.feratech.nps.adapter.PointOfPurchaseAdapter;
import com.feratech.nps.dao.*;
import com.feratech.nps.db.DatabaseHelper;
import com.feratech.nps.domain.*;
import com.feratech.nps.helper.QuestionnaireHelper;
import com.feratech.nps.utils.Logger;
import com.google.inject.Inject;
import roboguice.fragment.RoboFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * User: Bazlur Rahman Rokon
 * Date: 7/11/13 - 2:22 AM
 */
public class HotSpotAndPointOfPurchaseFragment extends RoboFragment implements Validatable {
    private Logger log = Logger.getLogger(HotSpotAndPointOfPurchaseFragment.class);

    @Inject
    private QuestionnaireHelper helper;

    private ListView lvHotspots;
    private ListView lvPointOfPurchase;
    private List<HotSpot> hotSpotList;
    private List<PointOfPurchase> pointOfPurchaseList;

    private String shopType = "";
    private ShopType shopTypeDb;
    private Questionnaire questionnaire;
    private int questionnaireId;
    private String dmsCode;

    private List<HotSpotWithCompliance> hotSpotWithCompliances;
    private List<HotSpotWithOralCare> hotSpotWithOralCares;
    private List<HotSpotWithSkinCare> hotSpotWithSkinCares;


    private LinearLayout hotSpotAndPointOfPurchaseFragmentView;

    @Inject
    private HotSpotDao hotSpotDao;
    @Inject
    private PointOfPurchaseDao pointOfPurchaseDao;
    @Inject
    private ShopTypeDao shopTypeDao;
    @Inject
    private DatabaseHelper databaseHelper;
    @Inject
    private QuestionnaireDao questionnaireDao;
    @Inject
    private HotSpotWithComplianceDao hotSpotWithComplianceDao;
    @Inject
    private HotSpotWithOralCareDao hotSpotWithOralCareDao;
    @Inject
    private HotSpotWithSkinCareDao hotSpotWithSkinCareDao;
    @Inject
    private QuestionnaireHelper questionnaireHelper;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        shopType = helper.getShopType().getType();
        dmsCode = helper.getStoreInfo().getDmsCode();

        hotSpotList = hotSpotDao.getAll();
        shopTypeDb = shopTypeDao.findByType(shopType);
        pointOfPurchaseList = pointOfPurchaseDao.getPointOfPurchase(shopTypeDb);
        questionnaire = questionnaireDao.findQuestionnaire(questionnaireId);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        hotSpotAndPointOfPurchaseFragmentView = (LinearLayout) inflater.inflate(R.layout.hostspot_and_point_of_purchase_layout, container, false);

        LinearLayout skinCareTitle = (LinearLayout) hotSpotAndPointOfPurchaseFragmentView.findViewById(R.id.hotspot_list_title_with_skin_care);
        LinearLayout oralCareTitle = (LinearLayout) hotSpotAndPointOfPurchaseFragmentView.findViewById(R.id.hotspot_list_title_with_oral_care);
        LinearLayout complianceTitle = (LinearLayout) hotSpotAndPointOfPurchaseFragmentView.findViewById(R.id.hotspot_list_title_with_compliance);
        skinCareTitle.setVisibility(View.GONE);
        oralCareTitle.setVisibility(View.GONE);
        complianceTitle.setVisibility(View.GONE);


        initialize();

        if (shopType.contains("RCS_Blue")
                || shopType.contains("RCS_Green")
                || shopType.contains("RCS_Purple")
                || shopType.contains("UCS_Purple")
                || shopType.contains("UCS_Green")
                || shopType.contains("UCS_Blue")
                ) {
            skinCareTitle.setVisibility(View.VISIBLE);
            List<HotSpotWithSkinCare> hotSpotWithSkinCares = new ArrayList<HotSpotWithSkinCare>();
            HotSpotWithSkinCare hotSpotWithSkinCare;

            for (HotSpot h : hotSpotList) {
                hotSpotWithSkinCare = new HotSpotWithSkinCare();
                hotSpotWithSkinCare.setShopType(shopTypeDb);
                hotSpotWithSkinCare.setHead(h.getHead());
                hotSpotWithSkinCare.setDescription(h.getDescription());
                hotSpotWithSkinCares.add(hotSpotWithSkinCare);
            }

            HotSpotWithSkinCareAdapter hotSpotWithSkinCareAdapter = new HotSpotWithSkinCareAdapter(getActivity(), R.layout.hotspot_with_skin_care_single_item, hotSpotWithSkinCares);
            lvHotspots.setAdapter(hotSpotWithSkinCareAdapter);
            try {
                ((TextView) hotSpotAndPointOfPurchaseFragmentView.findViewById(R.id.llHeading).findViewById(R.id.tv_head_extra)).setText("Skin/Hair Care");
            } catch (Exception ignored) {
            }

        } else if (shopType.contains("UNG")
                || shopType.contains("RNG")
                || shopType.contains("RWMG")
                || shopType.contains("UWMG")
                ) {

            complianceTitle.setVisibility(View.VISIBLE);
            List<HotSpotWithCompliance> hotSpotWithCompliances1 = new ArrayList<HotSpotWithCompliance>();
            HotSpotWithCompliance hotSpotWithCompliance;
            for (HotSpot h : hotSpotList) {
                hotSpotWithCompliance = new HotSpotWithCompliance();
                hotSpotWithCompliance.setShopType(shopTypeDb);
                hotSpotWithCompliance.setHead(h.getHead());
                hotSpotWithCompliance.setDescription(h.getDescription());
                hotSpotWithCompliances1.add(hotSpotWithCompliance);
            }
            HotSpotWithComplianceAdapter hotoSpotWithComplianceAdapter = new HotSpotWithComplianceAdapter(getActivity(), R.layout.hotspot_single_item, hotSpotWithCompliances1);
            lvHotspots.setAdapter(hotoSpotWithComplianceAdapter);

        } else if (shopType.contains("UGS_Green")
                || shopType.contains("UGS_Purple")) {
            oralCareTitle.setVisibility(View.VISIBLE);
            List<HotSpotWithOralCare> hotSpotWithOralCareList = new ArrayList<HotSpotWithOralCare>();
            HotSpotWithOralCare hotSpotWithOralCare;
            for (HotSpot h : hotSpotList) {
                hotSpotWithOralCare = new HotSpotWithOralCare();
                hotSpotWithOralCare.setShopType(shopTypeDb);
                hotSpotWithOralCare.setHead(h.getHead());
                hotSpotWithOralCare.setDescription(h.getDescription());
                hotSpotWithOralCareList.add(hotSpotWithOralCare);
            }
            HotspotWithOralCareAdapter hotspotWithOralCareAdapter = new HotspotWithOralCareAdapter(getActivity(), R.layout.hotspot_with_oral_care_single_item, hotSpotWithOralCareList);
            lvHotspots.setAdapter(hotspotWithOralCareAdapter);
            try {
                ((TextView) hotSpotAndPointOfPurchaseFragmentView.findViewById(R.id.llHeading).findViewById(R.id.tv_head_extra)).setText("Oral/Skin/Hair Care");
            } catch (Exception ignored) {
            }
        }

        PointOfPurchaseAdapter pointOfPurchaseAdapter = new PointOfPurchaseAdapter(getActivity(), R.layout.point_of_purchase_single_item, pointOfPurchaseList);
        lvPointOfPurchase.setAdapter(pointOfPurchaseAdapter);

        return hotSpotAndPointOfPurchaseFragmentView;
    }

    private void initialize() {
        lvHotspots = (ListView) hotSpotAndPointOfPurchaseFragmentView.findViewById(R.id.lv_hot_spot);
        lvHotspots.setFocusable(true);
        lvPointOfPurchase = (ListView) hotSpotAndPointOfPurchaseFragmentView.findViewById(R.id.lv_point_of_purchase);
        lvPointOfPurchase.setFocusable(true);
    }


    private boolean checkHotSpotWithCompliance() {
        log.debug("checkHotSpotWithCompliance()");
        boolean hasErrors = false;
        int count = lvHotspots.getCount();
        log.debug("lvHotspots.getCount()={}", lvHotspots.getCount());

        hotSpotWithCompliances = new ArrayList<HotSpotWithCompliance>();
        hotSpotWithCompliances.clear();
        Spinner spinner;
        HotSpotWithCompliance hotSpotWithCompliance;

        for (int i = 0; i < count; i++) {
            HotSpot hotSpot = hotSpotList.get(i);
            hotSpotWithCompliance = new HotSpotWithCompliance(hotSpot);
            ViewGroup row = (ViewGroup) lvHotspots.getChildAt(i);

            spinner = (Spinner) row.getChildAt(row.getChildCount() - 1);

            String selectedText = spinner.getSelectedItem().toString();
            Resources res = getResources();
            String[] yesNoSelect = res.getStringArray(R.array.yes_no_select);

            if (selectedText.equals(yesNoSelect[0])) {
                TextView textView = (TextView) spinner.getChildAt(0);
                textView.setError("Required");
                hasErrors = true;
            } else {
                if (selectedText.equals(yesNoSelect[1])) {
                    hotSpotWithCompliance.setCompliance(true);
                } else if (selectedText.equals(yesNoSelect[2])) {
                    hotSpotWithCompliance.setCompliance(false);
                }
                hotSpotWithCompliances.add(hotSpotWithCompliance);
            }
        }
        log.verbose(" hotSpotWithCompliances.size()={}", hotSpotWithCompliances.size());
        return hasErrors;
    }

    private boolean checkHotSpotWithSkinCare() {
        log.debug("checkHotSpotWithSkinCare()");
        boolean hasErrors = false;
        int count = lvHotspots.getCount();

        hotSpotWithSkinCares = new ArrayList<HotSpotWithSkinCare>();
        hotSpotWithSkinCares.clear();

        Spinner spinner_hair_care;
        Spinner spinner_skin_care;
        HotSpotWithSkinCare hotSpotWithSkinCare;
        for (int i = 0; i < count; i++) {
            HotSpot hotSpot = hotSpotList.get(i);
            hotSpotWithSkinCare = new HotSpotWithSkinCare(hotSpot);
            ViewGroup row = (ViewGroup) lvHotspots.getChildAt(i);

            spinner_hair_care = (Spinner) row.getChildAt(row.getChildCount() - 1);
            String selectedText = spinner_hair_care.getSelectedItem().toString();
            Resources res = getResources();
            String[] yesNoSelect = res.getStringArray(R.array.yes_no_select);
            if (selectedText.equals(yesNoSelect[0])) {
                TextView textView = (TextView) spinner_hair_care.getChildAt(0);
                textView.setError("Required");
                hasErrors = true;
            } else {
                if (selectedText.equals(yesNoSelect[1])) {
                    hotSpotWithSkinCare.setHairCare(true);
                } else if (selectedText.equals(yesNoSelect[2])) {
                    hotSpotWithSkinCare.setHairCare(false);
                }
            }

            //------------------------------
            spinner_skin_care = (Spinner) row.getChildAt(row.getChildCount() - 2);
            selectedText = spinner_skin_care.getSelectedItem().toString();
            yesNoSelect = res.getStringArray(R.array.yes_no_select);
            if (selectedText.equals(yesNoSelect[0])) {
                TextView textView = (TextView) spinner_skin_care.getChildAt(0);
                textView.setError("Required");
                hasErrors = true;
            } else {
                if (selectedText.equals(yesNoSelect[1])) {
                    hotSpotWithSkinCare.setSkinCare(true);
                } else if (selectedText.equals(yesNoSelect[2])) {
                    hotSpotWithSkinCare.setSkinCare(false);
                }
            }
            //hotSpotWithSkinCare.setQuestionnaire(questionnaire);
            hotSpotWithSkinCares.add(hotSpotWithSkinCare);
        }
        return hasErrors;
    }

    private boolean checkHotSpotWithOralCare() {
        log.debug("checkHotSpotWithOralCare()");
        boolean hasErrors = false;
        int count = lvHotspots.getCount();

        hotSpotWithOralCares = new ArrayList<HotSpotWithOralCare>();
        hotSpotWithOralCares.clear();

        Spinner spinner_hair_care;
        Spinner spinner_oral_care;
        Spinner spinner_skin_care;
        HotSpotWithOralCare hotSpotWithOralCare;

        for (int i = 0; i < count; i++) {
            HotSpot hotSpot = hotSpotList.get(i);
            hotSpotWithOralCare = new HotSpotWithOralCare(hotSpot);
            ViewGroup row = (ViewGroup) lvHotspots.getChildAt(i);


            spinner_hair_care = (Spinner) row.getChildAt(row.getChildCount() - 1);
            String selectedText = spinner_hair_care.getSelectedItem().toString();
            Resources res = getResources();
            String[] yesNoSelect = res.getStringArray(R.array.yes_no_select);
            if (selectedText.equals(yesNoSelect[0])) {
                TextView textView = (TextView) spinner_hair_care.getChildAt(0);
                textView.setError("Required");
                hasErrors = true;
            } else {
                if (selectedText.equals(yesNoSelect[1])) {
                    hotSpotWithOralCare.setHairCare(true);
                } else if (selectedText.equals(yesNoSelect[2])) {
                    hotSpotWithOralCare.setHairCare(false);
                }
            }

            //----------------

            spinner_oral_care = (Spinner) row.getChildAt(row.getChildCount() - 3);
            selectedText = spinner_oral_care.getSelectedItem().toString();

            if (selectedText.equals(yesNoSelect[0])) {
                TextView textView = (TextView) spinner_oral_care.getChildAt(0);
                textView.setError("Required");
                hasErrors = true;
            } else {
                if (selectedText.equals(yesNoSelect[1])) {
                    hotSpotWithOralCare.setOralCare(true);
                } else if (selectedText.equals(yesNoSelect[2])) {
                    hotSpotWithOralCare.setOralCare(false);
                }
            }

            //------------------------------
            spinner_skin_care = (Spinner) row.getChildAt(row.getChildCount() - 2);
            selectedText = spinner_skin_care.getSelectedItem().toString();
            yesNoSelect = res.getStringArray(R.array.yes_no_select);
            if (selectedText.equals(yesNoSelect[0])) {
                TextView textView = (TextView) spinner_skin_care.getChildAt(0);
                textView.setError("Required");
                hasErrors = true;
            } else {
                if (selectedText.equals(yesNoSelect[1])) {
                    hotSpotWithOralCare.setSkinCare(true);
                } else if (selectedText.equals(yesNoSelect[2])) {
                    hotSpotWithOralCare.setSkinCare(false);
                }
            }
            hotSpotWithOralCares.add(hotSpotWithOralCare);
        }
        return hasErrors;
    }

    private boolean hasErrorsInPointOfPurchase() {
        log.debug("hasErrorsInPointOfPurchase()");
        boolean hasErrors = false;
        int count = lvPointOfPurchase.getCount();
        Spinner spinner;
        for (int i = 0; i < count; i++) {
            PointOfPurchase pointOfPurchase = pointOfPurchaseList.get(i);
            ViewGroup row = (ViewGroup) lvPointOfPurchase.getChildAt(i);
            View view = row.getChildAt(row.getChildCount() - 1);
            if (view instanceof Spinner) {
                spinner = (Spinner) row.getChildAt(row.getChildCount() - 1);

                String selectedText = spinner.getSelectedItem().toString();
                Resources res = getResources();
                String[] yesNoSelect = res.getStringArray(R.array.yes_no_select);
                if (selectedText.equals(yesNoSelect[0])) {
                    TextView textView = (TextView) spinner.getChildAt(0);
                    textView.setError("Required");
                    hasErrors = true;
                } else {
                    if (selectedText.equals(yesNoSelect[1])) {
                        pointOfPurchase.setAvailability(true);
                    } else if (selectedText.equals(yesNoSelect[2])) {
                        pointOfPurchase.setAvailability(false);
                    }
                    // pointOfPurchase.setQuestionnaire(questionnaire);
                }
            }
        }
        return hasErrors;
    }

    private boolean hasErrorsInHotSpots() {
        log.debug("hasErrorsInHotSpots()");
        if (shopType.contains("RCS_Blue")
                || shopType.contains("RCS_Green")
                || shopType.contains("RCS_Purple")
                || shopType.contains("UCS_Purple")
                || shopType.contains("UCS_Green")
                || shopType.contains("UCS_Blue")
                ) {
            return checkHotSpotWithSkinCare();
        } else if (shopType.contains("UNG")
                || shopType.contains("RNG")
                || shopType.contains("RWMG")
                || shopType.contains("UWMG")
                ) {
            return checkHotSpotWithCompliance();

        } else if (shopType.contains("UGS_Green")
                || shopType.contains("UGS_Purple")) {
            return checkHotSpotWithOralCare();
        }

        return false;
    }

    private void saveHotSpots() {
        log.verbose("saveHotSpots()");
        if (shopType.contains("RCS_Blue")
                || shopType.contains("RCS_Green")
                || shopType.contains("RCS_Purple")
                || shopType.contains("UCS_Blue")
                || shopType.contains("UCS_Green")
                ) {
            questionnaireHelper.setHotSpots(hotSpotWithSkinCares);
        } else if (shopType.contains("UNG")
                || shopType.contains("RNG")
                || shopType.contains("RWMG")
                || shopType.contains("UWMG")
                ) {
            questionnaireHelper.setHotSpots(hotSpotWithCompliances);
        } else if (shopType.contains("UGS_Green")
                || shopType.contains("UGS_Purple")) {
            questionnaireHelper.setHotSpots(hotSpotWithOralCares);
        }
    }

    private void savePointOfPurchase() {
        log.verbose("savePointOfPurchase()");
        questionnaireHelper.setPointOfPurchases(pointOfPurchaseList);
    }

    @Override
    public boolean validate() {
        log.verbose("validate()");

        boolean hasErrorsInHotSpots = hasErrorsInHotSpots();
        boolean hasErrorsInPointOfPurchase = hasErrorsInPointOfPurchase();
        boolean hasError = hasErrorsInHotSpots | hasErrorsInPointOfPurchase;

        if (!hasError) {
            saveHotSpots();
            savePointOfPurchase();
        }
        return !hasError;
    }
}
