package com.feratech.nps.fragments;

/**
 * User: Bazlur Rahman Rokon
 * Date: 8/3/13 - 7:23 AM
 */
public interface Validatable {
    public boolean validate();
}
