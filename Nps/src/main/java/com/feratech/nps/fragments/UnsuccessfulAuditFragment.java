package com.feratech.nps.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import com.feratech.nps.R;
import com.feratech.nps.domain.UnsuccessfulAudit;
import com.feratech.nps.helper.QuestionnaireHelper;
import com.feratech.nps.utils.Logger;
import com.google.inject.Inject;
import roboguice.fragment.RoboFragment;
import roboguice.inject.InjectView;

import java.util.ArrayList;
import java.util.List;

/**
 * User: Bazlur Rahman Rokon
 * Date: 7/11/13 - 4:38 AM
 */

public class UnsuccessfulAuditFragment extends RoboFragment implements Validatable {
    private static final Logger log = Logger.getLogger(UnsuccessfulAuditFragment.class);

    @InjectView(R.id.chk_duplicate_address)
    private CheckBox duplicateAddress;
    @InjectView(R.id.et_dealer_code)
    private EditText etDealerCode;
    @InjectView(R.id.chk_incomplete_address)
    private CheckBox incompleteAddress;
    @InjectView(R.id.chk_temporary_closed)
    private CheckBox temporaryClosed;
    @InjectView(R.id.chk_permanently_closed)
    private CheckBox permanentlyClosed;
    @InjectView(R.id.spnr_permanently_closed_month)
    private Spinner spnrPermanentlyClosedMonth;

    @InjectView(R.id.chk_not_covered_due_to_restricted_area)
    private CheckBox notCoveredDueToRestrictedArea;
    @InjectView(R.id.chk_not_covered_due_to_productivity)
    private CheckBox notCoveredDueToProductivity;
    @InjectView(R.id.chk_refusal_by_retailer)
    private CheckBox refusalForRetailer;
    @InjectView(R.id.chk_business_changed)
    private CheckBox businessChanged;
    @InjectView(R.id.et_business_changed)
    private EditText newBusinessName;

    @Inject
    private QuestionnaireHelper questionnaireHelper;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.unsuccessful_audit, container, false);
    }

    public void saveUnsuccessfulAudit() {
        log.verbose("saveUnsuccessfulAudit()");

        List<UnsuccessfulAudit> unsuccessfulAuditsList = new ArrayList<UnsuccessfulAudit>();

        if (duplicateAddress.isChecked()) {
            UnsuccessfulAudit unsuccessfulAudit = new UnsuccessfulAudit();
            unsuccessfulAudit.setReason(duplicateAddress.getText().toString());
            String dealerCode = etDealerCode.getText().toString();
            unsuccessfulAudit.setDealersCode(dealerCode);
            unsuccessfulAudit.setCode(1);
            unsuccessfulAuditsList.add(unsuccessfulAudit);
        }

        if (incompleteAddress.isChecked()) {
            UnsuccessfulAudit unsuccessfulAudit = new UnsuccessfulAudit();
            unsuccessfulAudit.setReason(incompleteAddress.getText().toString());
            unsuccessfulAudit.setCode(2);
            unsuccessfulAuditsList.add(unsuccessfulAudit);
        }

        if (temporaryClosed.isChecked()) {
            UnsuccessfulAudit unsuccessfulAudit = new UnsuccessfulAudit();
            unsuccessfulAudit.setReason(temporaryClosed.getText().toString());
            unsuccessfulAudit.setCode(3);
            unsuccessfulAuditsList.add(unsuccessfulAudit);
        }

        if (permanentlyClosed.isChecked()) {
            UnsuccessfulAudit unsuccessfulAudit = new UnsuccessfulAudit();
            unsuccessfulAudit.setReason(permanentlyClosed.getText().toString());
            unsuccessfulAudit.setCode(4);
            TextView textView = (TextView) spnrPermanentlyClosedMonth.getSelectedView();
            String result = textView.getText().toString();
            unsuccessfulAudit.setMonth(result);
            unsuccessfulAuditsList.add(unsuccessfulAudit);
        }


        if (notCoveredDueToRestrictedArea.isChecked()) {
            UnsuccessfulAudit unsuccessfulAudit = new UnsuccessfulAudit();
            unsuccessfulAudit.setReason(notCoveredDueToRestrictedArea.getText().toString());
            unsuccessfulAudit.setCode(5);
            unsuccessfulAuditsList.add(unsuccessfulAudit);
        }
        if (notCoveredDueToProductivity.isChecked()) {
            UnsuccessfulAudit unsuccessfulAudit = new UnsuccessfulAudit();
            unsuccessfulAudit.setReason(notCoveredDueToProductivity.getText().toString());
            unsuccessfulAudit.setCode(6);
            unsuccessfulAuditsList.add(unsuccessfulAudit);
        }
        if (refusalForRetailer.isChecked()) {
            UnsuccessfulAudit unsuccessfulAudit = new UnsuccessfulAudit();
            unsuccessfulAudit.setReason(refusalForRetailer.getText().toString());
            unsuccessfulAudit.setCode(7);
            unsuccessfulAuditsList.add(unsuccessfulAudit);
        }
        if (businessChanged.isChecked()) {
            UnsuccessfulAudit unsuccessfulAudit = new UnsuccessfulAudit();
            unsuccessfulAudit.setReason(businessChanged.getText().toString());
            unsuccessfulAudit.setCode(7);
            unsuccessfulAudit.setBusinessName(newBusinessName.getText().toString());
            unsuccessfulAuditsList.add(unsuccessfulAudit);
        }

        questionnaireHelper.setUnsuccessfulAudits(unsuccessfulAuditsList);
    }

    @Override
    public boolean validate() {
        log.verbose("validate()");
        saveUnsuccessfulAudit();
        log.verbose("questionnaireHelper.getUnsuccessfulAudits().size() ={}", questionnaireHelper.getUnsuccessfulAudits().size());
        return true;
    }
}
