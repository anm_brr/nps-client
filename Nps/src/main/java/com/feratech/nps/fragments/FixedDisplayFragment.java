package com.feratech.nps.fragments;

import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.feratech.nps.R;
import com.feratech.nps.dao.*;
import com.feratech.nps.db.DatabaseHelper;
import com.feratech.nps.domain.*;
import com.feratech.nps.helper.QuestionnaireHelper;
import com.feratech.nps.utils.Logger;
import com.feratech.nps.views.FixedDisplaySkuSingleItem;
import com.google.inject.Inject;
import roboguice.fragment.RoboFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * User: Bazlur Rahman Rokon
 * Date: 7/10/13 - 11:01 PM
 */
public class FixedDisplayFragment extends RoboFragment implements View.OnClickListener, Validatable {
    private static final Logger log = Logger.getLogger(FixedDisplayFragment.class);

    @Inject
    private QuestionnaireHelper questionnaireHelper;

    //private List<FixedDisplaySku> fixedDisplaySkuListToBeSaved = new ArrayList<FixedDisplaySku>();
    private LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

    private boolean allRequired = true;
    private int questionSet = 0;
    private int currentSet = 0; // by default its 0

    private String shopType = "";
    private ShopType shopTYpeDb;

    private List<List<List<FixedDisplaySku>>> nestedFixedDisplay;
    private int selectedIndex = 0;
    private QuestionnaireMeta questionnaireMeta;
    private List<QuestionMeta> questionMetas;
    private List<String> questionList;
    private Questionnaire questionnaire;
    private int questionnaireId;

    private List<FixedDisplaySku> fixedDisplaySkuListToBeSaved = new ArrayList<FixedDisplaySku>();

    private LinearLayout[] fixedDisplaySkuSingleSets;

    private ScrollView fixedDisplayFragmentView;

    @Inject
    private DatabaseHelper databaseHelper;
    @Inject
    private QuestionnaireMetaDao questionnaireMetaDao;
    @Inject
    private FixedDisplaySkuDao fixedDisplaySkuDao;
    @Inject
    private ShopTypeDao shopTypeDao;
    @Inject
    private QuestionSetDao questionSetDao;
    @Inject
    private QuestionMetaDao questionMetaDao;
    @Inject
    private QuestionnaireDao questionnaireDao;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        log.debug("onCreate()");

        shopType = questionnaireHelper.getShopType().getType();

        questionnaire = questionnaireDao.findQuestionnaire(questionnaireId);

        shopTYpeDb = shopTypeDao.findByType(shopType);
        questionnaireMeta = questionnaireMetaDao.findByShopType(shopTYpeDb);
        allRequired = questionnaireMeta.isFixedDisplayAllRequired();
        questionSet = questionnaireMeta.getFixedDisplaySetSize();

        questionMetas = questionMetaDao.getQuestionMetas(shopTYpeDb);
        questionList = getQuestion3();

        fixedDisplaySkuSingleSets = new LinearLayout[questionSet];

//        Map<String, List<List<List<FixedDisplaySku>>>> map = ((MainApplication) getActivity().getApplication()).getCache();
//        nestedFixedDisplay = map.get(MainApplication.FIXED_DISPLAY_SKU_CACHE_KEY);
//
//        if (nestedFixedDisplay == null) {
        nestedFixedDisplay = new ArrayList<List<List<FixedDisplaySku>>>(questionSet);
        //     }

        for (int i = 0; i < questionSet; i++) {
            QuestionSet set = questionSetDao.findById((i + 1));
            nestedFixedDisplay.add(fixedDisplaySkuDao.getNestedFixedDisplaySku(shopTYpeDb, set));
        }
    }

    private void hideAll(int index) {
        log.verbose("hideAll() index={}", index);
        log.verbose("fixedDisplaySkuSingleSets.length={}", fixedDisplaySkuSingleSets.length);
        for (int i = 0; i < fixedDisplaySkuSingleSets.length; i++) {
            if (i == index) {
                fixedDisplaySkuSingleSets[i].setVisibility(View.VISIBLE);
                //   ((TextView) getActivity().findViewById(R.id.tv_question)).setText(questionList.get(i));
            } else {
                fixedDisplaySkuSingleSets[i].setVisibility(View.GONE);
            }
        }
    }

    private void setQuestion(String text) {
        log.verbose("setQuestion() text={}", text);
        ((TextView) fixedDisplayFragmentView.findViewById(R.id.tv_question)).setText(text);
    }

    private void selectSet1() {
        log.debug("selectSet1()");
        selectedIndex = 0;
        setQuestion(questionList.get(0));

        if (fixedDisplaySkuSingleSets[1] != null) {
            fixedDisplaySkuSingleSets[1].setVisibility(View.GONE);
            fixedDisplaySkuSingleSets[0].setVisibility(View.VISIBLE);
        }
    }

    private void setSelectedIndex(int index) {
        log.verbose("setSelectedIndex() index={}", index);
        hideAll(index);
    }

    private void selectSet2() {
        log.debug("selectSet2()");
        selectedIndex = 1;
        setQuestion(questionList.get(1));
        if (fixedDisplaySkuSingleSets[0] != null) {
            fixedDisplaySkuSingleSets[0].setVisibility(View.GONE);
            fixedDisplaySkuSingleSets[1].setVisibility(View.VISIBLE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        log.verbose("onCreateView()");

        fixedDisplayFragmentView = (ScrollView) inflater.inflate(R.layout.fixed_display_layout, container, false);
        LinearLayout fixedDisplayLayout = (LinearLayout) fixedDisplayFragmentView.findViewById(R.id.fixed_display_layout);
        log.debug("nestedFixedDisplay.size(): {}", nestedFixedDisplay.size());

        for (int i = 0; i < questionSet; i++) {
            fixedDisplaySkuSingleSets[i] = new LinearLayout(getActivity());
            fixedDisplaySkuSingleSets[i].setLayoutParams(layoutParams);
            fixedDisplaySkuSingleSets[i].setOrientation(LinearLayout.VERTICAL);

            List<List<FixedDisplaySku>> fixedDisplaySkus = nestedFixedDisplay.get(i);
            for (List<FixedDisplaySku> f : fixedDisplaySkus) {
                fixedDisplaySkuSingleSets[i].addView(new FixedDisplaySkuSingleItem(getActivity(), f));
            }
            fixedDisplayLayout.addView(fixedDisplaySkuSingleSets[i]);
        }

        if (!allRequired) {
            selectSet1();
        } else {
            log.debug("currentSet={}", currentSet);
            hideAll(currentSet);
        }

        addTabButtons();
        return fixedDisplayFragmentView;
    }

//    private boolean hasErrorsOnAllList() {
//        log.debug("hasErrorsOnAllList()");
//        boolean hasErrors = false;
//        if (allRequired) {
//            for (int i = 0; i < nestedFixedDisplay.size(); i++) {
//                hasErrors = hasErrors(listViews[i], nestedFixedDisplay.get(i));
//                if (hasErrors) {
//                    getActivity().getActionBar().setSubtitle("Errors in SET -" + (i + 1));
//                    Toast.makeText(getActivity(), "Please check SET -" + (i + 1), Toast.LENGTH_LONG).show();
//                    return hasErrors;
//                }
//            }
//        } else {
//            hasErrors = hasErrors(listViews[selectedIndex], nestedFixedDisplay.get(selectedIndex));
//        }
//        return hasErrors;
//    }

    private List<String> getQuestion3() {
        log.verbose("getQuestion3()");
        List<String> questions = new ArrayList<String>();
        for (QuestionMeta q : questionMetas) {
            if (q.getQuestionNo().contains("3")) {
                questions.add(q.getQuestion());
            }
        }
        return questions;
    }

    private void extractDataFromList() {
        log.debug("extractDataFromList()");
        if (allRequired) {
            for (int i = 0; i < questionSet; i++) {
                List<List<FixedDisplaySku>> list = nestedFixedDisplay.get(i);
                for (List<FixedDisplaySku> l : list) {
                    for (FixedDisplaySku f : l) {
                        log.debug("FixedDisplaySku={}", f);
                        f.setQuestionnaire(questionnaire);
                        fixedDisplaySkuListToBeSaved.add(f);
                    }
                }
            }
        } else {
            List<List<FixedDisplaySku>> list = nestedFixedDisplay.get(selectedIndex);
            for (List<FixedDisplaySku> l : list) {
                for (FixedDisplaySku f : l) {
                    log.debug("FixedDisplaySku={}", f);
                    f.setQuestionnaire(questionnaire);
                    fixedDisplaySkuListToBeSaved.add(f);
                }
            }
        }
    }

    public void addTabButtons() {
        log.debug("addTabButtons()");
        LinearLayout linearLayout = (LinearLayout) fixedDisplayFragmentView.findViewById(R.id.tabs);
        RadioGroup displaySet = (RadioGroup) fixedDisplayFragmentView.findViewById(R.id.radioGroupSet);

        if (allRequired) {
            displaySet.setVisibility(View.GONE);
            linearLayout.setVisibility(View.VISIBLE);
            Button[] tabButton = new Button[questionSet];
            for (int i = 0; i < questionSet; i++) {
                tabButton[i] = new Button(getActivity());
                tabButton[i].setText("SET " + (i + 1));
                tabButton[i].setId(i);
                tabButton[i].setOnClickListener(this);
                linearLayout.addView(tabButton[i]);
            }
        } else {
            displaySet.setVisibility(View.VISIBLE);
            linearLayout.setVisibility(View.GONE);
            displaySet.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
                    if (checkedId == R.id.rbtn_set_1) {
                        selectSet1();
                    } else if (checkedId == R.id.rbtn_set_2) {
                        selectSet2();
                    }
                }
            });
        }
    }

    private boolean hasErrors(ListView listView, List<List<FixedDisplaySku>> fixedDisplaySkus) {
        log.debug("hasErrors()");
        int count = listView.getCount();
        boolean hasErrors = false;
        for (int i = 0; i < count; i++) {
            ViewGroup row = (ViewGroup) listView.getChildAt(i);
            List<FixedDisplaySku> list = fixedDisplaySkus.get(i);

            LinearLayout llTotalFaceUpCount = (LinearLayout) row.findViewById(R.id.total_face_up_count);
            for (int j = 0; j < llTotalFaceUpCount.getChildCount(); j++) {
                FixedDisplaySku fixedDisplaySku = list.get(j);
                TextView textView = (TextView) llTotalFaceUpCount.getChildAt(j);
                if (textView.getText().toString().trim().equals("")) {
                    textView.setError("Required");
                    hasErrors = true;
                } else {
                    try {
                        fixedDisplaySku.setFaceUpCount(Integer.parseInt(textView.getText().toString()));
                    } catch (Exception e) {
                        Log.d("LOG", "Something went wrong!");
                    }
                }
            }

            LinearLayout llTotalDisplayCount = (LinearLayout) row.findViewById(R.id.total_display_count);
            for (int j = 0; j < llTotalDisplayCount.getChildCount(); j++) {
                FixedDisplaySku fixedDisplaySku = list.get(j);
                TextView textView = (TextView) llTotalDisplayCount.getChildAt(j);
                if (textView.getText().toString().trim().equals("")) {
                    textView.setError("Required");
                    hasErrors = true;
                } else {
                    try {
                        fixedDisplaySku.setTotalDisplayCount(Integer.parseInt(textView.getText().toString()));
                    } catch (Exception e) {
                        Log.d("LOG", "Something went wrong!");
                    }
                }
            }

            {
                LinearLayout llAvailability = (LinearLayout) row.findViewById(R.id.availability);
                Spinner spinner = (Spinner) llAvailability.findViewById(R.id.spinner);
                String selectedText = spinner.getSelectedItem().toString();
                Resources res = getResources();
                String[] yesNoSelect = res.getStringArray(R.array.yes_no_select);
                if (selectedText.equals(yesNoSelect[0])) {
                    TextView textView = (TextView) spinner.getChildAt(0);
                    textView.setError("Required");
                    hasErrors = true;
                } else {
                    if (selectedText.equals(yesNoSelect[1])) {
                        setYesNoForAvailability(list, 1);
                    } else if (selectedText.equals(yesNoSelect[2])) {
                        setYesNoForAvailability(list, 0);
                    }
                }
            }

            {
                LinearLayout llSequence = (LinearLayout) row.findViewById(R.id.sequence);
                Spinner spinner2 = (Spinner) llSequence.findViewById(R.id.spinner);
                String selectedText = spinner2.getSelectedItem().toString();
                Resources res = getResources();
                String[] yesNoSelect = res.getStringArray(R.array.yes_no_select);
                if (selectedText.equals(yesNoSelect[0])) {
                    TextView textView = (TextView) spinner2.getChildAt(0);
                    textView.setError("Required");
                    hasErrors = true;
                } else {
                    if (selectedText.equals(yesNoSelect[1])) {
                        setYesNoForSequence(list, 1);
                    } else if (selectedText.equals(yesNoSelect[2])) {
                        setYesNoForSequence(list, 0);
                    }
                }
            }
        }
        return hasErrors;
    }

    private void setYesNoForAvailability(List<FixedDisplaySku> list, int value) {
        for (FixedDisplaySku f : list) {
            f.setAvailability(value == 1);
        }
    }

    private void setYesNoForSequence(List<FixedDisplaySku> list, int value) {
        for (FixedDisplaySku f : list) {
            f.setSequence(value == 1);
        }
    }

    @Override
    public void onClick(View view) {
        setSelectedIndex(view.getId());
    }

    @Override
    public boolean validate() {
        return !hasErrorsInAllPage();
    }

    private boolean hasErrorsInAllPage() {
        log.verbose("hasErrorsInAllPage()");
        boolean hasError = false;
        if (allRequired) {
            for (int i = 0; i < fixedDisplaySkuSingleSets.length; i++) {
                LinearLayout layout = fixedDisplaySkuSingleSets[i];

                int count = layout.getChildCount();
                for (int j = 0; j < count; j++) {
                    FixedDisplaySkuSingleItem singleItem = (FixedDisplaySkuSingleItem) layout.getChildAt(j);
                    hasError = singleItem.hasError();
                }
            }

            fixedDisplaySkuListToBeSaved.clear();
            for (List<List<FixedDisplaySku>> list : nestedFixedDisplay) {
                for (List<FixedDisplaySku> list1 : list) {
                    for (FixedDisplaySku fixedDisplaySku : list1) {
                        fixedDisplaySkuListToBeSaved.add(fixedDisplaySku);
                    }
                }
            }

        } else {
            LinearLayout layout = fixedDisplaySkuSingleSets[selectedIndex];

            int count = layout.getChildCount();
            for (int j = 0; j < count; j++) {
                FixedDisplaySkuSingleItem singleItem = (FixedDisplaySkuSingleItem) layout.getChildAt(j);
                hasError = singleItem.hasError();
            }
            List<List<FixedDisplaySku>> selectedFixedDisplay = nestedFixedDisplay.get(selectedIndex);
            fixedDisplaySkuListToBeSaved.clear();
            for (List<FixedDisplaySku> list1 : selectedFixedDisplay) {
                for (FixedDisplaySku fixedDisplaySku : list1) {
                    fixedDisplaySkuListToBeSaved.add(fixedDisplaySku);
                }
            }
        }
        questionnaireHelper.setFixedDisplaySkus(fixedDisplaySkuListToBeSaved);
        log.debug("FixedDisplaySkus()= {}", questionnaireHelper.getFixedDisplaySkus().toString());
        return hasError;
    }


//    private boolean hasErrorsOnAllList() {
//        log.debug("hasErrorsOnAllList()");
//        boolean hasErrors = false;
//        if (allRequired) {
//            for (int i = 0; i < nestedFixedDisplay.size(); i++) {
//                hasErrors = hasErrors(listViews[i], nestedFixedDisplay.get(i));
//                if (hasErrors) {
//                    getActivity().getActionBar().setSubtitle("Errors in SET -" + (i + 1));
//                    Toast.makeText(this, "Please check SET -" + (i + 1), Toast.LENGTH_LONG).show();
//                    return hasErrors;
//                }
//            }
//        } else {
//            hasErrors = hasErrors(listViews[selectedIndex], nestedFixedDisplay.get(selectedIndex));
//        }
//        return hasErrors;
//    }
}
