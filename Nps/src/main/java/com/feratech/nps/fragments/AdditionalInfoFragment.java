package com.feratech.nps.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.feratech.nps.R;
import com.feratech.nps.adapter.AdditionalSkuAdapter;
import com.feratech.nps.dao.AdditionalSkuDao;
import com.feratech.nps.dao.QuestionnaireDao;
import com.feratech.nps.dao.ShopTypeDao;
import com.feratech.nps.db.DatabaseHelper;
import com.feratech.nps.domain.AdditionalSku;
import com.feratech.nps.domain.Questionnaire;
import com.feratech.nps.domain.ShopType;
import com.feratech.nps.helper.QuestionnaireHelper;
import com.feratech.nps.utils.Logger;
import com.google.inject.Inject;
import roboguice.fragment.RoboFragment;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * User: Bazlur Rahman Rokon
 * Date: 7/11/13 - 3:31 AM
 */
public class AdditionalInfoFragment extends RoboFragment implements Validatable {
    private static final Logger log = Logger.getLogger(AdditionalInfoFragment.class);

    private static final int MENU_NEXT = 234;
    private SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM dd, yyyy - hh:mm:ss a");

    private EditText etNameShopOwnerSalesman;
    private EditText etCellNo;
    private TextView tvTimeAndDate;
    private ListView lvAdditionalSku;

    private List<AdditionalSku> additionalSkus;
    private Button btnAddPhotoAndGeoTag;

    private String shopType;
    private Questionnaire questionnaire;
    private int questionnaireId;
    private String dmsCode;

    private LinearLayout additionalInfoFragmentView;

    @Inject
    private QuestionnaireHelper questionnaireHelper;
    @Inject
    private DatabaseHelper databaseHelper;
    @Inject
    private AdditionalSkuDao additionalSkuDao;
    @Inject
    private ShopTypeDao shopTypeDao;
    @Inject
    private QuestionnaireDao questionnaireDao;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        dmsCode = questionnaireHelper.getStoreInfo().getDmsCode();
        shopType = questionnaireHelper.getShopType().getType();

        ShopType shopTypeDb = shopTypeDao.findByType(shopType);
        questionnaireHelper.setAdditionalSkus(additionalSkuDao.getAdditionalSku(shopTypeDb));
    }

    private void setCurrentDateAndTime() {
        Date date = new Date();
        String time = dateFormat.format(date).toString();
        tvTimeAndDate.setText(time);
    }

    private void initialize() {
        etNameShopOwnerSalesman = (EditText) additionalInfoFragmentView.findViewById(R.id.et_name_of_shop_owner_or_salesman);
        etCellNo = (EditText) additionalInfoFragmentView.findViewById(R.id.et_cell_no);
        tvTimeAndDate = (TextView) additionalInfoFragmentView.findViewById(R.id.tv_time_and_date);
        lvAdditionalSku = (ListView) additionalInfoFragmentView.findViewById(R.id.lv_additional_sku);
    }

    @Override
    public View onCreateView(LayoutInflater inflate, ViewGroup container, Bundle savedInstanceState) {
        additionalInfoFragmentView = (LinearLayout) inflate.inflate(R.layout.aditional_info_layout, container, false);
        initialize();

        setCurrentDateAndTime();

        AdditionalSkuAdapter additionalSkuAdapter = new AdditionalSkuAdapter(getActivity(),
                R.layout.must_have_sku_single_item, additionalSkuDao.getAdditionalSku(questionnaireHelper.getShopType()));
        lvAdditionalSku.setAdapter(additionalSkuAdapter);
        return additionalInfoFragmentView;
    }

    @Override
    public boolean validate() {
        boolean isValid;
        isValid = hasErrorsOnAdditionalSku() | hasErrorsOnShopNameAndCellNo();
        return !isValid;
    }

    private boolean hasErrorsOnShopNameAndCellNo() {
        log.verbose("hasErrorsOnShopNameAndCellNo()");
        boolean hasErrors = false;
        if (etNameShopOwnerSalesman.getText().toString().isEmpty()) {
            etNameShopOwnerSalesman.setError("Required");
            hasErrors = true;
        }
        if (etCellNo.getText().toString().isEmpty()) {
            etCellNo.setError("Required");
            hasErrors = true;
        }

        questionnaire = new Questionnaire();
        questionnaire.setDateTime(new Date());
        questionnaire.setCellPhone(etCellNo.getText().toString());
        questionnaire.setNameOfShopOwnerOrSalesMan(etNameShopOwnerSalesman.getText().toString());
        questionnaireHelper.setQuestionnaire(questionnaire);

        return hasErrors;
    }

    private boolean hasErrorsOnAdditionalSku() {
        log.verbose("hasErrorsOnAdditionalSku()");
        boolean hasErrors = false;
        int count = lvAdditionalSku.getCount();
        log.verbose("lvAdditionalSku.getCount()={}", lvAdditionalSku.getCount());

        for (int i = 0; i < count; i++) {
            ViewGroup row = (ViewGroup) lvAdditionalSku.getChildAt(i);
            if (row == null) {
                log.debug("Row is null");
                return false;
            }
            TextView textView = (TextView) row.getChildAt(row.getChildCount() - 1);
            AdditionalSku additionalSku = questionnaireHelper.getAdditionalSkus().get(i);

            if (textView.getText().toString().trim().equals("")) {
                textView.setError("Required");
                hasErrors = true;
            } else {
                try {
                    String skuCount = textView.getText().toString().trim();
                    additionalSku.setSkuCount(Integer.parseInt(skuCount));
                } catch (Exception e) {
                    log.error("Unable to parse integer", e);
                }
            }
        }
        log.debug("questionnaireHelper.getAdditionalSkus()={}", questionnaireHelper.getAdditionalSkus());
        return hasErrors;
    }
}
