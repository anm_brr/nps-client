package com.feratech.nps.fragments;

import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import com.feratech.nps.R;
import com.feratech.nps.domain.MustHaveSku;
import com.feratech.nps.helper.QuestionnaireHelper;
import com.feratech.nps.utils.Logger;
import com.feratech.nps.views.MustHaveSkuSingleItem;
import com.google.inject.Inject;
import roboguice.fragment.RoboFragment;

import java.util.Collection;

/**
 * User: Bazlur Rahman Rokon
 * Date: 7/10/13 - 10:24 PM
 */
public class MustHaveSkuFragment extends RoboFragment implements Validatable {
    private static final Logger log = Logger.getLogger(MustHaveSkuFragment.class);
    @Inject
    private QuestionnaireHelper questionnaireHelper;

    private Collection<MustHaveSku> mustHaveSkus;
    private MustHaveSkuSingleItem[] mustHaveSkuSingleItems;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mustHaveSkus = questionnaireHelper.getMustHaveSkus();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        log.verbose("onCreateView()");
        ScrollView scrollView = (ScrollView) inflater.inflate(R.layout.must_have_sku, container, false);
        scrollView.setPadding(5, 5, 5, 5);

        LinearLayout linearLayout = (LinearLayout) scrollView.findViewById(R.id.must_have_sku);

        LinearLayout layout = new LinearLayout(getActivity());
        layout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
        layout.setGravity(Gravity.TOP);
        layout.setOrientation(LinearLayout.VERTICAL);

        mustHaveSkuSingleItems = new MustHaveSkuSingleItem[mustHaveSkus.size()];

        int index = 0;
        for (MustHaveSku mustHaveSku : mustHaveSkus) {
            mustHaveSkuSingleItems[index] = new MustHaveSkuSingleItem(getActivity(), mustHaveSku);
            layout.addView(mustHaveSkuSingleItems[index]);
            index++;
        }
        linearLayout.addView(layout);

        return scrollView;
    }

    public int getErrorCount() {
        log.debug("getErrorCount()");
        int errorCount = 0;
        for (MustHaveSkuSingleItem singleItem : mustHaveSkuSingleItems) {
            if (!singleItem.isValid()) {
                errorCount++;
            }
        }
        log.debug("getErrorCount(), errorCount={}", errorCount);
        log.debug("mustHaveSkus:={} ", questionnaireHelper.getMustHaveSkus().toString());
        return errorCount;
    }

    @Override
    public boolean validate() {
        return getErrorCount() == 0;
    }
}
