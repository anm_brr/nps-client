package com.feratech.nps.fragments;

import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.feratech.nps.R;
import com.feratech.nps.adapter.WaveDisplayListAdapter;
import com.feratech.nps.dao.*;
import com.feratech.nps.db.DatabaseHelper;
import com.feratech.nps.domain.*;
import com.feratech.nps.helper.QuestionnaireHelper;
import com.feratech.nps.utils.Logger;
import com.google.inject.Inject;
import roboguice.fragment.RoboFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * User: Bazlur Rahman Rokon
 * Date: 7/11/13 - 1:48 AM
 */
public class WaveDisplayFragment extends RoboFragment implements Validatable {
    private static final Logger log = Logger.getLogger(WaveDisplayFragment.class);

    private LinearLayout waveDisplayFragmentView;

    private static final int MENU_NEXT = 24;
    private LinearLayout llSet1, llSet2;
    private ListView waveListView;
    private WaveDisplayListAdapter waveDisplayListAdapter;

    private String shopType;

    private ShopType shopTypeDb;

    private boolean allRequired = true;
    private int questionSet = 0;
    private int currentSet = 0; // by default its 0

    private int selectedIndex = 1;
    private QuestionnaireMeta questionnaireMeta;
    private List<QuestionMeta> questionMetas;

    private List<String> questionList;
    private ListView[] listViews;
    private WaveDisplayListAdapter[] waveDisplayListAdapters;
    private List<List<WaveDisplaySku>> waveDisplaySkuList;
    private Questionnaire questionnaire;
    private int questionnaireId;
    private String dmsCode;
    private List<WaveDisplaySku> waveDisplaySkuListToBeSaved = new ArrayList<WaveDisplaySku>();


    @Inject
    private QuestionnaireHelper questionnaireHelper;
    @Inject
    private DatabaseHelper databaseHelper;
    @Inject
    private QuestionMetaDao questionMetaDao;
    @Inject
    private QuestionSetDao questionSetDao;
    @Inject
    private QuestionnaireDao dao;
    @Inject
    private WaveDisplaySkuDao waveDisplaySkuDao;
    @Inject
    private ShopTypeDao shopTypeDao;
    @Inject
    private QuestionnaireMetaDao questionnaireMetaDao;
    @Inject
    private QuestionnaireDao questionnaireDao;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        dmsCode = questionnaireHelper.getStoreInfo().getDmsCode();
        shopType = questionnaireHelper.getShopType().getType();

        shopTypeDb = shopTypeDao.findByType(shopType);
        questionnaireMeta = questionnaireMetaDao.findByShopType(shopTypeDb);
        questionSet = questionnaireMeta.getWaveDisplayQuestionSize();
        allRequired = questionnaireMeta.isWaveDisplayAllRequired();

        listViews = new ListView[questionSet];
        waveDisplayListAdapters = new WaveDisplayListAdapter[questionSet];
        waveDisplaySkuList = new ArrayList<List<WaveDisplaySku>>(questionSet);

        questionMetas = questionMetaDao.getQuestionMetas(shopTypeDb);
        questionnaire = questionnaireDao.findQuestionnaire(questionnaireId);

        questionList = getQuestion3();

        for (int i = 0; i < questionSet; i++) {
            QuestionSet set = questionSetDao.findById((i + 1));
            waveDisplaySkuList.add(waveDisplaySkuDao.getWaveDisplaySku(shopTypeDb, set));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        waveDisplayFragmentView = (LinearLayout) inflater.inflate(R.layout.wave_display_layout, container, false);

        log.debug("waveDisplaySkuList.size()={}", waveDisplaySkuList.size());

        for (int i = 0; i < questionSet; i++) {
            listViews[i] = new ListView(getActivity());
            ListView.LayoutParams params = new AbsListView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            listViews[i].setLayoutParams(params);
            listViews[i].setDivider(getResources().getDrawable(R.drawable.list_item_divider));
            waveDisplayFragmentView.addView(listViews[i]);
            waveDisplayListAdapters[i] = new WaveDisplayListAdapter(getActivity(), R.layout.wave_display_item_single, waveDisplaySkuList.get(i));
            listViews[i].setAdapter(waveDisplayListAdapters[i]);
        }
        //if (!allRequired) {
        selectSet1();
        //}
        // selectSet1();
        initialize();

        return waveDisplayFragmentView;
    }

    private List<String> getQuestion3() {
        List<String> questions = new ArrayList<String>();
        for (QuestionMeta q : questionMetas) {
            if (q.getQuestionNo().contains("4")) {
                questions.add(q.getQuestion());
            }
        }
        return questions;
    }

    private void initialize() {
        RadioGroup displaySet = (RadioGroup) waveDisplayFragmentView.findViewById(R.id.radioGroupSet);

        displaySet.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
                if (checkedId == R.id.rbtn_set_1) {
                    selectSet1();
                } else if (checkedId == R.id.rbtn_set_2) {
                    if (questionSet == 1) {
                        return;
                    } else
                        selectSet2();
                }
            }
        });
    }

    private void selectSet1() {
        log.debug("selectSet1()");
        selectedIndex = 0;
        //setQuestion(questionList.get(0));
        if (questionSet == 1) {
            listViews[0].setVisibility(View.VISIBLE);
        } else {
            if (listViews[1] != null) {
                listViews[1].setVisibility(View.GONE);
                listViews[0].setVisibility(View.VISIBLE);
            }
        }
    }

    private void selectSet2() {
        log.debug("selectSet2()");
        selectedIndex = 1;
        //  setQuestion(questionList.get(1));
        if (listViews[0] != null) {
            listViews[0].setVisibility(View.GONE);
            listViews[1].setVisibility(View.VISIBLE);
        }
    }

    private void setQuestion(String text) {
        //   ((TextView) findViewById(R.id.tv_question)).setText(text);
    }

    @Override
    public boolean validate() {


        return !hasErrors(listViews[selectedIndex], waveDisplaySkuList.get(selectedIndex));
    }

    private boolean hasErrors(ListView waveListView, List<WaveDisplaySku> waveDisplaySkuList) {
        log.debug("hasErrors()");
        int count = waveListView.getCount();

        boolean hasErrors = false;

        for (int i = 0; i < count; i++) {
            ViewGroup row = (ViewGroup) waveListView.getChildAt(i);
            WaveDisplaySku waveDisplaySku = waveDisplaySkuList.get(i);
            TextView textView = (TextView) row.getChildAt(row.getChildCount() - 2);
            if (textView.getText().toString().trim().equals("")) {
                textView.setError("Required");
                hasErrors = true;
            } else {
                try {
                    waveDisplaySku.setSkuCount(Integer.parseInt(textView.getText().toString()));
                } catch (Exception e) {
                    log.debug("LOG", "Something went wrong!");
                }
            }

            Spinner spinner = (Spinner) row.getChildAt(row.getChildCount() - 1);
            String selectedText = spinner.getSelectedItem().toString();
            Resources res = getResources();
            String[] yesNoSelect = res.getStringArray(R.array.yes_no_select);
            if (selectedText.equals(yesNoSelect[0])) {
                TextView tv = (TextView) spinner.getChildAt(0);
                tv.setError("Required");
                hasErrors = true;
            } else {
                if (selectedText.equals(yesNoSelect[1])) {
                    waveDisplaySku.setSequence(true);
                } else if (selectedText.equals(yesNoSelect[2])) {
                    waveDisplaySku.setSequence(false);
                }
            }
//            waveDisplaySku.setQuestionnaire(questionnaire);
        }

        waveDisplaySkuListToBeSaved.clear();
        waveDisplaySkuListToBeSaved.addAll(waveDisplaySkuList);
        questionnaireHelper.setWaveDisplaySkus(waveDisplaySkuListToBeSaved);
        return hasErrors;
    }
}
