/*
 * ******************************************************
 *  * Copyright (C) 2013 A. N. M. Bazlur Rahman <anmbrr.bit0112@gmail.com>
 *  *
 *  * This file is part of perfect-store.
 *  *
 *  * perfect-store can not be copied and/or distributed without the express
 *  * permission of {A. N. M. Bazlur Rahman}
 *  ******************************************************
 */

package com.feratech.nps.dao;


import com.feratech.nps.db.DatabaseHelper;
import com.feratech.nps.domain.PointOfPurchase;
import com.feratech.nps.domain.ShopType;
import com.feratech.nps.utils.Logger;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * User: Bazlur Rahman Rokon
 * Date: 6/15/13 - 4:52 AM
 */
@Singleton
public class PointOfPurchaseDao extends SimpleDao<PointOfPurchase, Integer> {
    private Logger log = Logger.getLogger(PointOfPurchaseDao.class);

    @Inject
    public PointOfPurchaseDao(DatabaseHelper databaseHelper) throws SQLException {
        super(databaseHelper, databaseHelper.getPointOfPurchaseDao());
    }

    public List<PointOfPurchase> getPointOfPurchase(ShopType shopType) {
        log.debug("getPointOfPurchase(), shopType.getType()={} ", shopType.getType());
        QueryBuilder<PointOfPurchase, Integer> queryBuilder = getDao().queryBuilder();
        try {
            return
                    queryBuilder.where()
                            .eq("shopType_id", shopType.getId())
                            .and()
                            .eq("rawData", true)
                            .query();
        } catch (SQLException e) {
            log.error("unable to get questionnaire-- getPointOfPurchase()", e);
        }
        return new ArrayList<PointOfPurchase>();
    }
}
