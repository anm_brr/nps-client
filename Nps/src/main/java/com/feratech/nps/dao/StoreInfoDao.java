package com.feratech.nps.dao;

import com.feratech.nps.domain.StoreInfo;

import java.sql.SQLException;
import java.util.List;
import java.util.Set;

/**
 * User: Bazlur Rahman Rokon
 * Date: 7/9/13 - 3:16 AM
 */
public interface StoreInfoDao {
    public List<String> getRegion();

    public List<String> getTerritory();

    public List<String> getTerritory(String region);

    public List<String> getTown();

    public List<String> getTown(String territory);

    public List<String> getShopType();

    public Set<String> getAddress();

    public long count();

    public void save(List<StoreInfo> list);

    public Set<String> getShops();

    public boolean isValidDmsCode(String dmsCode, String shopName);

    public boolean isValidDmsCode(String region, String territory, String town, String dmsCode);

    public boolean isValidDmsCode(String region, String territory, String town, String shopType, String dmsCode);

    public StoreInfo findStore(String region, String territory, String town, String shopType, String dmsCode);

    public void clearAll();

    public void insertBulkData(List<StoreInfo> storeInfoList) throws SQLException;

    public Set<String> getShop(String shop);

    public StoreInfo getStoreInfo(String dmsCode);

    public void updateStoreInfo(StoreInfo storeInfo);
}
