/*
 * ******************************************************
 *  * Copyright (C) 2013 A. N. M. Bazlur Rahman <anmbrr.bit0112@gmail.com>
 *  *
 *  * This file is part of perfect-store.
 *  *
 *  * perfect-store can not be copied and/or distributed without the express
 *  * permission of {A. N. M. Bazlur Rahman}
 *  ******************************************************
 */

package com.feratech.nps.dao;

import com.feratech.nps.db.DatabaseHelper;
import com.feratech.nps.domain.QuestionnaireMeta;
import com.feratech.nps.domain.ShopType;
import com.feratech.nps.utils.Logger;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import java.sql.SQLException;
import java.util.List;

/**
 * User: Bazlur Rahman Rokon
 * Date: 6/15/13 - 4:53 AM
 */
@Singleton
public class QuestionnaireMetaDao extends SimpleDao<QuestionnaireMeta, Integer> {
    private Logger log = Logger.getLogger(QuestionnaireMetaDao.class);

    @Inject
    public QuestionnaireMetaDao(DatabaseHelper databaseHelper) throws SQLException {
        super(databaseHelper, databaseHelper.getQuestionnaireMetaDao());
    }


    public QuestionnaireMeta findByShopType(ShopType shopType) {
        log.debug("findByShopType shopType={}", shopType);
        try {
            List<QuestionnaireMeta> questionnairemetas = getDao().queryBuilder().where().eq("shopType_id", shopType.getId()).query();
            if (questionnairemetas != null && questionnairemetas.size() > 0) return questionnairemetas.get(0);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
