/*
 * ******************************************************
 *  * Copyright (C) 2013 A. N. M. Bazlur Rahman <anmbrr.bit0112@gmail.com>
 *  *
 *  * This file is part of perfect-store.
 *  *
 *  * perfect-store can not be copied and/or distributed without the express
 *  * permission of {A. N. M. Bazlur Rahman}
 *  ******************************************************
 */

package com.feratech.nps.dao;

import com.feratech.nps.db.DatabaseHelper;
import com.feratech.nps.domain.QuestionSet;
import com.feratech.nps.domain.ShopType;
import com.feratech.nps.domain.WaveDisplaySku;
import com.feratech.nps.utils.Logger;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * User: Bazlur Rahman Rokon
 * Date: 6/15/13 - 4:53 AM
 */
@Singleton
public class WaveDisplaySkuDao extends SimpleDao<WaveDisplaySku, Integer> {
    private Logger log = Logger.getLogger(WaveDisplaySku.class);

    @Inject
    public WaveDisplaySkuDao(DatabaseHelper databaseHelper) throws SQLException {
        super(databaseHelper, databaseHelper.getWaveDisplaySkuDao());
    }

    public List<WaveDisplaySku> getWaveDisplaySku(ShopType shopType, QuestionSet questionSet) {
        log.debug("getWaveDisplaySku(), shopType.getType()={},questionSet.getId()={} ", shopType.getType(), questionSet.getId());
        QueryBuilder<WaveDisplaySku, Integer> queryBuilder = getDao().queryBuilder();
        try {
            return
                    queryBuilder.where()
                            .eq("shopType_id", shopType.getId())
                            .and()
                            .eq("questionSet_id", questionSet.getId())
                            .and()
                            .eq("rawData", true)
                            .query();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new ArrayList<WaveDisplaySku>();
    }
}
