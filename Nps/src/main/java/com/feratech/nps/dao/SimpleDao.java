/*
 * ******************************************************
 *  * Copyright (C) 2013 A. N. M. Bazlur Rahman <anmbrr.bit0112@gmail.com>
 *  *
 *  * This file is part of perfect-store.
 *  *
 *  * perfect-store can not be copied and/or distributed without the express
 *  * permission of {A. N. M. Bazlur Rahman}
 *  ******************************************************
 */

package com.feratech.nps.dao;

import com.feratech.nps.db.DatabaseHelper;
import com.feratech.nps.utils.Logger;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.DatabaseConnection;

import java.sql.SQLException;
import java.sql.Savepoint;
import java.util.List;

/**
 * User: Bazlur Rahman Rokon
 * Date: 6/15/13 - 1:11 AM
 */
public abstract class SimpleDao<ObjectType, Identity> {
    private static final Logger log = Logger.getLogger(SimpleDao.class);
    private DatabaseHelper databaseHelper;

    protected Dao<ObjectType, Identity> simpleDao;

    public SimpleDao(DatabaseHelper databaseHelper, Dao<ObjectType, Identity> simpleDao) {
        this.databaseHelper = databaseHelper;
        this.simpleDao = simpleDao;
    }

    public Dao<ObjectType, Identity> getDao() {
        return this.simpleDao;
    }

    public long countOf() {
        log.debug("countOf()");
        long count = 0;
        try {
            count = getDao().countOf();
        } catch (SQLException e) {
            log.error("unable to get countOf", e);
        }
        return count;
    }

    public ObjectType save(ObjectType objectType) {
        log.debug("save()");
        try {
            getDao().create(objectType);
        } catch (SQLException e) {
            e.printStackTrace();
            log.error("Unable to create object={}", objectType);
        }
        return objectType;
    }

    public void saveBulkData(List<ObjectType> objectTypes) throws SQLException {
        log.debug("saveBulkData(), objectTypes.size()={}", objectTypes.size());
        long t1 = System.nanoTime();
        DatabaseConnection conn = simpleDao.startThreadConnection();
        Savepoint savepoint = null;
        try {
            savepoint = conn.setSavePoint(null);
            doInsert(objectTypes, simpleDao);
        } finally {
            conn.commit(savepoint);
            simpleDao.endThreadConnection(conn);
        }
        long t2 = System.nanoTime();
        log.debug("Total time to update ={} seconds", (t2 - t1) * 1e-9);
    }

    public void updateBulkData(List<ObjectType> objectTypes, Dao<ObjectType, Identity> simpleDao) {
        log.debug("updateBulkData(), objectTypes.size()={}", objectTypes.size());
        long t1 = System.nanoTime();
        try {
            DatabaseConnection conn = simpleDao.startThreadConnection();
            Savepoint savepoint = null;
            try {
                savepoint = conn.setSavePoint(null);
                doUpdate(objectTypes, simpleDao);
            } finally {
                conn.commit(savepoint);
                simpleDao.endThreadConnection(conn);
            }
            long t2 = System.nanoTime();
            log.debug("Total time to update ={} seconds", (t2 - t1) * 1e-9);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void doUpdate(List<ObjectType> objectTypes, Dao<ObjectType, Identity> simpleDao) {
        log.debug("doUpdate()");
        for (ObjectType a : objectTypes) {
            try {
                if (a != null) {
                    simpleDao.createOrUpdate(a);
                } else {
                    log.info("Null OBject found, can crate or update");
                }
            } catch (SQLException e) {
                e.printStackTrace();
                log.error("unable to save or update ={}");
            }
        }
    }


    private void doInsert(List<ObjectType> objectTypes, Dao<ObjectType, Identity> simpleDao) {
        log.debug("doInsert() objectTypes.size()={}", objectTypes.size());
        for (ObjectType a : objectTypes) {
            try {
                if (a != null) {
                    log.debug("going to create={}", a);
                    simpleDao.create(a);
                } else {
                    log.info("Null Object found, can not create");
                }
            } catch (SQLException e) {
                e.printStackTrace();
                log.error("unable to save or update ={}", e);
            }
        }
    }
}
