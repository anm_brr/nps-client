/*
 * ******************************************************
 *  * Copyright (C) 2013 A. N. M. Bazlur Rahman <anmbrr.bit0112@gmail.com>
 *  *
 *  * This file is part of perfect-store.
 *  *
 *  * perfect-store can not be copied and/or distributed without the express
 *  * permission of {A. N. M. Bazlur Rahman}
 *  ******************************************************
 */

package com.feratech.nps.dao;


import com.feratech.nps.db.DatabaseHelper;
import com.feratech.nps.domain.UnsuccessfulAudit;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import java.sql.SQLException;

/**
 * User: Bazlur Rahman Rokon
 * Date: 6/15/13 - 4:53 AM
 */

@Singleton
public class UnsuccessfulAuditDao extends SimpleDao<UnsuccessfulAudit, Integer> {
    @Inject
    public UnsuccessfulAuditDao(DatabaseHelper databaseHelper) throws SQLException {
        super(databaseHelper, databaseHelper.getUnsuccessfulAuditsDao());
    }
}
