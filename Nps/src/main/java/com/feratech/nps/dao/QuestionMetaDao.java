/*
 * ******************************************************
 *  * Copyright (C) 2013 A. N. M. Bazlur Rahman <anmbrr.bit0112@gmail.com>
 *  *
 *  * This file is part of perfect-store.
 *  *
 *  * perfect-store can not be copied and/or distributed without the express
 *  * permission of {A. N. M. Bazlur Rahman}
 *  ******************************************************
 */

package com.feratech.nps.dao;

import com.feratech.nps.db.DatabaseHelper;
import com.feratech.nps.domain.QuestionMeta;
import com.feratech.nps.domain.ShopType;
import com.feratech.nps.utils.Logger;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * User: Bazlur Rahman Rokon
 * Date: 6/15/13 - 4:52 AM
 */
@Singleton
public class QuestionMetaDao extends SimpleDao<QuestionMeta, Integer> {
    private static final Logger log = Logger.getLogger(QuestionMetaDao.class);

    @Inject
    public QuestionMetaDao(DatabaseHelper databaseHelper) throws SQLException {
        super(databaseHelper, databaseHelper.getQuestionMetaDao());
    }

    public List<QuestionMeta> getQuestionMetas(ShopType shopType) {
        log.debug("getQuestionMetas()");
        try {
            return getDao().queryBuilder().where().eq("shopType_id", shopType.getId()).query();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new ArrayList<QuestionMeta>();
    }
}
