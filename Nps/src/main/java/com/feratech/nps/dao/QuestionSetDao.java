/*
 * ******************************************************
 *  * Copyright (C) 2013 A. N. M. Bazlur Rahman <anmbrr.bit0112@gmail.com>
 *  *
 *  * This file is part of perfect-store.
 *  *
 *  * perfect-store can not be copied and/or distributed without the express
 *  * permission of {A. N. M. Bazlur Rahman}
 *  ******************************************************
 */

package com.feratech.nps.dao;


import com.feratech.nps.db.DatabaseHelper;
import com.feratech.nps.domain.QuestionSet;
import com.feratech.nps.utils.Logger;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.List;

/**
 * User: Bazlur Rahman Rokon
 * Date: 6/15/13 - 4:53 AM
 */
@Singleton
public class QuestionSetDao extends SimpleDao<QuestionSet, Integer> {
    private static final Logger log = Logger.getLogger(QuestionSetDao.class);

    @Inject
    public QuestionSetDao(DatabaseHelper databaseHelper) throws SQLException {
        super(databaseHelper, databaseHelper.getQuestionSetDao());
    }

    public QuestionSet findById(int id) {
        QueryBuilder<QuestionSet, Integer> queryBuilder = getDao().queryBuilder();
        try {
            List<QuestionSet> questionSetList = queryBuilder.where().eq("id", id).query();
            if (questionSetList != null && questionSetList.size() > 0) {
                return questionSetList.get(0);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public QuestionSet createQuestionSetById(int id) {
        log.debug("createQuestionSetById()");
        QuestionSet questionSet = new QuestionSet();
        questionSet.setId(id);
        questionSet.setNumber(id);
        questionSet.setSet("SET " + id);
        try {
            return
                    getDao().createIfNotExists(questionSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
