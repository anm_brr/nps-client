package com.feratech.nps.dao;

import com.feratech.nps.db.DatabaseHelper;
import com.feratech.nps.domain.StoreInfo;
import com.feratech.nps.utils.Logger;
import com.feratech.nps.utils.Utils;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.SelectArg;
import com.j256.ormlite.support.DatabaseConnection;

import java.sql.SQLException;
import java.sql.Savepoint;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * User: Bazlur Rahman Rokon
 * Date: 7/9/13 - 3:17 AM
 */
@Singleton
public class StoreInfoDaoImpl implements StoreInfoDao {
    private static final Logger log = Logger.getLogger(StoreInfoDaoImpl.class);
    private Dao<StoreInfo, Integer> storeInfoDao;
    private QueryBuilder<StoreInfo, Integer> queryBuilder;

    @Inject
    public StoreInfoDaoImpl(final DatabaseHelper databaseHelper) {
        try {
            this.storeInfoDao = databaseHelper.getStoreInfoDao();
        } catch (SQLException e) {
            log.debug("Unable to loadStoreInfo storeInfoDao", e);
        }
        queryBuilder =
                storeInfoDao.queryBuilder();
    }

    @Override
    public void insertBulkData(List<StoreInfo> storeInfoList) throws SQLException {
        log.debug("insertBulkData()");
        long t1 = System.nanoTime();
        DatabaseConnection conn = storeInfoDao.startThreadConnection();
        Savepoint savepoint = null;
        try {
            savepoint = conn.setSavePoint(null);
            doInsert(storeInfoList, storeInfoDao);
        } finally {
            conn.commit(savepoint);
            storeInfoDao.endThreadConnection(conn);
        }
        long t2 = System.nanoTime();
        log.debug("total time inserting bulk data ={} seconds", (t2 - t1) * 1e-9);

    }

    private void doInsert(List<StoreInfo> storeInfoList, final Dao<StoreInfo, Integer> storeInfoDao) {
        log.debug("doInsert()");
        for (StoreInfo storeInfo : storeInfoList) {
            try {
                storeInfo.setVisited(false);
                storeInfoDao.create(storeInfo);
            } catch (SQLException e) {
                log.error("Unable to create or Update", e);
            }
        }
    }

    @Override
    public List<String> getRegion() {
        log.debug("getRegion()");
        List<String> set = new ArrayList<String>();
        try {
            queryBuilder.clear();
            List<StoreInfo> storeInfoList =
                    queryBuilder
                            .distinct()
                            .selectColumns(StoreInfo.REGION)
                            .query();
            for (StoreInfo storeInfo : storeInfoList) set.add(storeInfo.getRegion());
        } catch (SQLException e) {
            log.error("unable to get regions", e);
        }
        return set;
    }

    @Override
    public List<String> getTerritory() {
        log.debug("getTerritory()");
        List<String> set = new ArrayList<String>();
        try {
            queryBuilder.clear();
            List<StoreInfo> list =
                    queryBuilder
                            .distinct()
                            .selectColumns(StoreInfo.TERRITORY)
                            .query();
            for (StoreInfo storeInfo : list) set.add(storeInfo.getTerritory());
        } catch (SQLException e) {
            log.error("getTerritory() -> unable to loadStoreInfo Territory", e);
        }
        return set;
    }

    @Override
    public List<String> getTerritory(String region) {
        log.debug("getTerritory() -> region={}", region);
        List<String> list = new ArrayList<String>();
        if (Utils.isEmptyString(region)) return getTerritory();
        else {
            try {
                queryBuilder.clear();
                SelectArg selectArg = new SelectArg();
                selectArg.setValue(region);

                List<StoreInfo> storeInfo1 =storeInfoDao.query( queryBuilder
                        .distinct()
                        .selectColumns(StoreInfo.TERRITORY)
                        .where()
                        .eq(StoreInfo.REGION, region).prepare());

                for (StoreInfo info : storeInfo1) {
                    list.add(info.getTerritory());
                }
            } catch (SQLException e) {
                log.debug("unable to getTerritory", e);
            }
        }

        return list;
    }

    @Override
    public List<String> getTown() {
        log.debug("getTown()");
        List<String> townList = new ArrayList<String>();

        try {
            queryBuilder.clear();

            List<StoreInfo> list = queryBuilder
                    .distinct()
                    .selectColumns(StoreInfo.TOWN)
                    .query();
            for (StoreInfo storeInfo : list) {
                townList.add(storeInfo.getTown());
            }

        } catch (SQLException e) {
            log.error("unable to get town", e);
        }
        return townList;
    }

    @Override
    public List<String> getTown(String territory) {
        log.debug("getTown() -> territory={} ", territory);
        List<String> townList = new ArrayList<String>();
        if (Utils.isEmptyString(territory)) return getTown();
        else {
            try {
                queryBuilder.clear();
                SelectArg selectArg = new SelectArg();
                selectArg.setValue(territory);
                List<StoreInfo> storeInfo1 = storeInfoDao.query(queryBuilder
                        .distinct()
                        .selectColumns(StoreInfo.TOWN)
                        .where()
                        .eq(StoreInfo.TERRITORY, selectArg).prepare());

                for (StoreInfo info : storeInfo1) {
                    log.debug("inside for loop={}", info);
                    townList.add(info.getTown());
                }

            } catch (SQLException e) {
                log.error("unable to get town", e);
            }
        }
        return townList;
    }

    @Override
    public Set<String> getAddress() {
        return null;
    }

    @Override
    public long count() {
        try {
            return storeInfoDao.countOf();
        } catch (SQLException e) {

        }
        return 0;
    }

    @Override
    public void save(List<StoreInfo> list) {
        for (StoreInfo info : list) {
            try {
                this.storeInfoDao.createIfNotExists(info);
            } catch (SQLException e) {

            }
        }
    }

    @Override
    public Set<String> getShops() {
        Set<String> set = new HashSet<String>();
//        try {
//            List<StoreInfo> list = storeInfoDao.queryForAll();
//            for (StoreInfo storeInfo : list) set.add(storeInfo.getShopName());
//
//        } catch (SQLException e) {
//            
//        }
        return set;
    }

    @Override
    public Set<String> getShop(String town) {
        Set<String> set = new HashSet<String>();
//        if (Utils.isEmptyString(town)) return getShops();
//        else {
//            try {
//                List<StoreInfo> storeInfo1 =
//                        storeInfoDao.query(
//                                storeInfoDao.queryBuilder()
//                                        .where()
//                                        .eq(StoreInfo.TOWN, town)
//                                        .prepare());
//                for (StoreInfo info : storeInfo1) set.add(info.getShopName());
//            } catch (SQLException e) {
//                
//            }
//        }
        return set;
    }

    @Override
    public StoreInfo getStoreInfo(String dmsCode) {
        log.debug("getStoreInfo() dmsCode={}", dmsCode);
        queryBuilder.clear();
        try {
            return queryBuilder.where().eq(StoreInfo.DMS_CODE, dmsCode).queryForFirst();
        } catch (SQLException e) {

            log.error("unable to get storeInfo by ={}", dmsCode, e);
        }
        return null;
    }

    @Override
    public void updateStoreInfo(StoreInfo storeInfo) {
        log.verbose("updateStoreInfo()");
        try {
            storeInfoDao.update(storeInfo);
        } catch (SQLException e) {
            log.error("Unable to update storeInfo", e);
        }
    }

    @Override
    public List<String> getShopType() {
        log.debug("getShopType()");
        List<String> shopType = new ArrayList<String>();
        try {
            queryBuilder.clear();
            List<StoreInfo> list =
                    queryBuilder
                            .distinct()
                            .selectColumns(StoreInfo.SHOP_TYPE)
                            .query();

            for (StoreInfo storeInfo : list) shopType.add(storeInfo.getShopType());

        } catch (SQLException e) {

            log.debug("unable to get shop type", e);
        }
        return shopType;
    }

    @Override
    public boolean isValidDmsCode(String dms, String shopName) {
        try {
            List<StoreInfo> storeInfo1 =
                    storeInfoDao.query(
                            storeInfoDao.queryBuilder()
                                    .where()
                                    .eq(StoreInfo.DMS_CODE, dms)
                                    .and()
                                    .eq(StoreInfo.SHOP_NAME, shopName)
                                    .prepare());
            if (storeInfo1 != null && storeInfo1.size() > 0) {
                return true;
            }
        } catch (SQLException e) {
        }

        return false;
    }

    @Override
    public boolean isValidDmsCode(String region, String territory, String town, String dmsCode) {
        log.debug("isValidDmsCode()=> region={},territory={},town={},dmsCode={}", region, territory, town, dmsCode);
        try {
            queryBuilder.clear();
            SelectArg regionArg = new SelectArg();
            regionArg.setValue(region);
            SelectArg territoryArg = new SelectArg();
            territoryArg.setValue(territory);
            SelectArg townArg = new SelectArg();
            townArg.setValue(town);

            List<StoreInfo> storeInfos = storeInfoDao.query(
                    queryBuilder.where()
                            .like(StoreInfo.DMS_CODE, dmsCode)
                            .and()
                            .like(StoreInfo.REGION, regionArg)
                            .and()
                            .like(StoreInfo.TERRITORY, territoryArg)
                            .and()
                            .like(StoreInfo.TOWN, townArg).prepare());

            log.debug("storeInfos.size()={}", storeInfos.size());
            if (storeInfos != null && storeInfos.size() > 0) {
                return true;
            }
        } catch (SQLException e) {

            log.error("unable to check validity", e);
        }
        return false;
    }

    @Override
    public boolean isValidDmsCode(String region, String territory, String town, String shopType, String dmsCode) {
        log.debug("isValidDmsCode()=> region={},territory={},town={},shopType={},dmsCode={}", region, territory, town, shopType, dmsCode);
        try {
            queryBuilder.clear();
            SelectArg regionArg = new SelectArg();
            regionArg.setValue(region);
            SelectArg territoryArg = new SelectArg();
            territoryArg.setValue(territory);
            SelectArg townArg = new SelectArg();
            townArg.setValue(town);

            List<StoreInfo> storeInfos = storeInfoDao.query(
                    queryBuilder.where()
                            .like(StoreInfo.DMS_CODE, dmsCode)
                            .and()
                            .like(StoreInfo.REGION, regionArg)
                            .and()
                            .like(StoreInfo.TERRITORY, territoryArg)
                            .and()
                            .like(StoreInfo.TOWN, townArg)
                            .and()
                            .like("shop_type", shopType).prepare()
            );

            log.debug("storeInfos.size()={}", storeInfos.size());
            if (storeInfos != null && storeInfos.size() > 0) {
                return true;
            }
        } catch (SQLException e) {

            log.error("unable to check validity", e);
        }
        return false;
    }

    @Override
    public StoreInfo findStore(String region, String territory, String town, String shopType, String dmsCode) {

        log.debug("isValidDmsCode()=> region={},territory={},town={},shopType={},dmsCode={}", region, territory, town, shopType, dmsCode);
        try {
            queryBuilder.clear();

            SelectArg regionArg = new SelectArg();
            regionArg.setValue(region);
            SelectArg territoryArg = new SelectArg();
            territoryArg.setValue(territory);
            SelectArg townArg = new SelectArg();
            townArg.setValue(town);

            List<StoreInfo> storeInfos =  storeInfoDao.query(
                    queryBuilder.where()
                            .like(StoreInfo.DMS_CODE, dmsCode)
                            .and()
                            .like(StoreInfo.REGION, regionArg)
                            .and()
                            .like(StoreInfo.TERRITORY, territoryArg)
                            .and()
                            .like(StoreInfo.TOWN, townArg)
                            .and()
                            .like("shop_type", shopType).prepare());

            log.debug("storeInfos.size()={}", storeInfos.size());
            if (storeInfos != null && storeInfos.size() > 0) {
                return storeInfos.get(0);
            }
        } catch (SQLException e) {

            log.error("unable to check validity", e);
        }
        return null;
    }

    private StoreInfo findByDmsCode(String dmsCode) throws SQLException {
        queryBuilder.clear();
        List<StoreInfo> storeInfos =
                queryBuilder.where()
                        .eq(StoreInfo.DMS_CODE, dmsCode).query();
        if (storeInfos != null && storeInfos.size() > 0) {
            return storeInfos.get(0);
        }
        return null;
    }

    @Override
    public void clearAll() {
        log.debug("clearAll()");

        try {
            DeleteBuilder<StoreInfo, Integer> deleteBuilder = storeInfoDao.deleteBuilder();
            deleteBuilder.where().eq("visited", false);
            deleteBuilder.delete();
        } catch (SQLException e) {

            log.error("unable to clear all storeInfo from database");
        }
    }
}
