/*
 * ******************************************************
 *  * Copyright (C) 2013 A. N. M. Bazlur Rahman <anmbrr.bit0112@gmail.com>
 *  *
 *  * This file is part of perfect-store.
 *  *
 *  * perfect-store can not be copied and/or distributed without the express
 *  * permission of {A. N. M. Bazlur Rahman}
 *  ******************************************************
 */

package com.feratech.nps.dao;


import com.feratech.nps.activity.QuestionnaireListActivity;
import com.feratech.nps.db.DatabaseHelper;
import com.feratech.nps.domain.*;
import com.feratech.nps.utils.Logger;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * User: Bazlur Rahman Rokon
 * Date: 6/15/13 - 12:49 AM
 */

@Singleton
public class QuestionnaireDaoImpl extends SimpleDao<Questionnaire, Integer> implements QuestionnaireDao {
    private static final Logger log = Logger.getLogger(QuestionnaireDaoImpl.class);
    @Inject
    DatabaseHelper databaseHelper;

    private AdditionalSkuDao additionalSkuDao;
    private FixedDisplaySkuDao fixedDisplaySkuDao;
    private HotSpotDao hotSpotDao;
    private MustHaveSkuDao mustHaveSkuDao;
    private PointOfPurchaseDao pointOfPurchaseDao;
    private QuestionnaireMetaDao questionnaireMetaDao;
    private QuestionSetDao questionSetDao;
    private ShopTypeDao shopTypeDao;
    private UnsuccessfulAuditDao unsuccessfulAuditDao;
    private WaveDisplaySkuDao waveDisplaySkuDao;
    private QuestionMetaDao questionMetaDao;
    private HotSpotWithOralCareDao hotSpotWithOralCareDao;
    private HotSpotWithComplianceDao hotSpotWithComplianceDao;
    private HotSpotWithSkinCareDao hotSpotWithSkinCareDao;

    @Inject
    public QuestionnaireDaoImpl(DatabaseHelper databaseHelper) throws SQLException {
        super(databaseHelper, databaseHelper.getQuestionnaireDao());
        log.debug("Constructor--");

        additionalSkuDao = new AdditionalSkuDao(databaseHelper);
        fixedDisplaySkuDao = new FixedDisplaySkuDao(databaseHelper);
        hotSpotDao = new HotSpotDao(databaseHelper);
        mustHaveSkuDao = new MustHaveSkuDao(databaseHelper);
        pointOfPurchaseDao = new PointOfPurchaseDao(databaseHelper);
        questionnaireMetaDao = new QuestionnaireMetaDao(databaseHelper);
        questionMetaDao = new QuestionMetaDao(databaseHelper);
        questionSetDao = new QuestionSetDao(databaseHelper);
        shopTypeDao = new ShopTypeDao(databaseHelper);
        unsuccessfulAuditDao = new UnsuccessfulAuditDao(databaseHelper);
        waveDisplaySkuDao = new WaveDisplaySkuDao(databaseHelper);
        hotSpotWithSkinCareDao = new HotSpotWithSkinCareDao(databaseHelper);
        hotSpotWithOralCareDao = new HotSpotWithOralCareDao(databaseHelper);
        hotSpotWithComplianceDao = new HotSpotWithComplianceDao(databaseHelper);
    }

    @Override
    public void saveQuestionnaires(List<Questionnaire> questionnaireList) {
        log.debug("saveQuestionnaires()");
        try {
            for (Questionnaire q : questionnaireList) {
                ShopType shopType = q.getShopType();
                shopTypeDao.save(shopType);

                saveNewAdditionalSku(q);
                saveNewFixedDisplaySku(q);
                saveNewHotSpots(q);
                saveNewMustHaveSku(q);
                saveNewPointOfPurchase(q);
                saveNewQuestionnaireMeta(q);
                saveNewUnsuccessfulAudits(q);
                saveWaveDisplaySku(q);
                saveQuestionMetas(q);

                q.setNewData(true);
                Questionnaire q2 = save(q);
                log.debug(q2.getWaveDisplays());
            }

        } catch (SQLException e) {
            log.error("Unable to saveBulkData questionnaire Dao ", e);
        }
    }

    private void saveQuestionMetas(Questionnaire q) {
        log.debug("saveQuestionMetas()");
        try {
            List<QuestionMeta> questionMetas = (List<QuestionMeta>) q.getQuestionMetas();
            for (QuestionMeta m : questionMetas) {
                m.setShopType(q.getShopType());
            }
            questionMetaDao.saveBulkData(questionMetas);
        } catch (SQLException e) {
            log.error("Unable to save question questionMetas", e);
        }
    }

    private void saveWaveDisplaySku(Questionnaire questionnaire) throws SQLException {
        log.debug("saveWaveDisplaySku()");
        List<WaveDisplaySku> waveDisplaySkus = (List<WaveDisplaySku>) questionnaire.getWaveDisplays();
        if (waveDisplaySkus != null && waveDisplaySkus.size() > 0) {
            log.debug("waveDisplaySkus.size()={}", waveDisplaySkus.size());
            for (WaveDisplaySku w : waveDisplaySkus) {
                QuestionSet questionSet = questionSetDao.findById(w.getQuestionSetId());
                if (questionSet == null) {
                    questionSet = questionSetDao.createQuestionSetById(w.getQuestionSetId());
                }
                w.setQuestionSet(questionSet);
                w.setShopType(questionnaire.getShopType());
                w.setRawData(true);
            }
            waveDisplaySkuDao.saveBulkData(waveDisplaySkus);
        }
    }

    private void saveWaveDisplaySku(Questionnaire questionnaire, boolean isRaw) throws SQLException {
        log.debug("saveWaveDisplaySku()");
        List<WaveDisplaySku> waveDisplaySkus = (List<WaveDisplaySku>) questionnaire.getWaveDisplays();
        if (waveDisplaySkus != null && waveDisplaySkus.size() > 0) {
            log.debug("waveDisplaySkus.size()={}", waveDisplaySkus.size());
            for (WaveDisplaySku w : waveDisplaySkus) {
                QuestionSet questionSet = questionSetDao.findById(w.getQuestionSetId());
                if (questionSet == null) {
                    questionSet = questionSetDao.createQuestionSetById(w.getQuestionSetId());
                }
                w.setQuestionSet(questionSet);
                w.setShopType(questionnaire.getShopType());
                w.setRawData(isRaw);
                w.setQuestionnaire(questionnaire);
            }
            waveDisplaySkuDao.saveBulkData(waveDisplaySkus);
        }
    }

    private void saveNewUnsuccessfulAudits(Questionnaire questionnaire) throws SQLException {
        log.verbose("saveNewUnsuccessfulAudits()");
        List<UnsuccessfulAudit> unsuccessfulAudits = (List<UnsuccessfulAudit>) questionnaire.getUnsuccessfulAudits();
        if (unsuccessfulAudits != null && unsuccessfulAudits.size() > 0) {
            for (UnsuccessfulAudit unsuccessfulAudit : unsuccessfulAudits) {
                unsuccessfulAudit.setQuestionnaire(questionnaire);
            }
            unsuccessfulAuditDao.saveBulkData(unsuccessfulAudits);
        }
    }

    private void saveNewQuestionnaireMeta(Questionnaire questionnaire) {
        log.verbose("saveNewQuestionnaireMeta()");
        if (questionnaire.getQuestionnaireMeta() != null) {
            questionnaire.getQuestionnaireMeta().setShopType(questionnaire.getShopType());
            questionnaireMetaDao.save(questionnaire.getQuestionnaireMeta());
        }
    }

    private void saveNewPointOfPurchase(Questionnaire questionnaire) throws SQLException {
        log.verbose("saveNewPointOfPurchase()");
        List<PointOfPurchase> pointOfPurchases = (List<PointOfPurchase>) questionnaire.getPointOfPurchases();
        if (pointOfPurchases != null && pointOfPurchases.size() > 0) {
            for (PointOfPurchase p : pointOfPurchases) {
                p.setShopType(questionnaire.getShopType());
                p.setRawData(true);
                p.setQuestionnaire(questionnaire);
            }
            pointOfPurchaseDao.saveBulkData(pointOfPurchases);
        }
    }

    private void saveNewPointOfPurchase(Questionnaire questionnaire, boolean isRaw) throws SQLException {
        log.verbose("saveNewPointOfPurchase()");
        List<PointOfPurchase> pointOfPurchases = (List<PointOfPurchase>) questionnaire.getPointOfPurchases();
//        log.verbose("pointOfPurchases= {}", pointOfPurchases.size());

        if (pointOfPurchases != null && pointOfPurchases.size() > 0) {
            for (PointOfPurchase p : pointOfPurchases) {
                p.setShopType(questionnaire.getShopType());
                p.setRawData(isRaw);
                p.setQuestionnaire(questionnaire);
                pointOfPurchaseDao.save(p);
            }
        }
    }

    private void saveNewMustHaveSku(Questionnaire questionnaire) throws SQLException {
        log.verbose("saveNewMustHaveSku()");
        List<MustHaveSku> mustHaveSkus = (List<MustHaveSku>) questionnaire.getMustHaveSkus();
        if (mustHaveSkus != null && mustHaveSkus.size() > 0) {
            for (MustHaveSku m : mustHaveSkus) {
                m.setShopType(questionnaire.getShopType());
                m.setRawData(true);
            }
            mustHaveSkuDao.saveBulkData(mustHaveSkus);
        }
    }

    private void saveNewMustHaveSku(Questionnaire questionnaire, boolean isRaw) throws SQLException {
        log.verbose("saveNewMustHaveSku()");
        List<MustHaveSku> mustHaveSkus = (List<MustHaveSku>) questionnaire.getMustHaveSkus();
        if (mustHaveSkus != null && mustHaveSkus.size() > 0) {
            for (MustHaveSku m : mustHaveSkus) {
                m.setShopType(questionnaire.getShopType());
                m.setRawData(isRaw);
                m.setQuestionnaire(questionnaire);
            }
            mustHaveSkuDao.saveBulkData(mustHaveSkus);
        }
    }

    private void saveHotSpots(Questionnaire questionnaire) throws SQLException {
        log.verbose("saveHotSpots()");
        List<? extends HotSpot> hotSpots = (List<? extends HotSpot>) questionnaire.getHotSpots();
        if (hotSpots.size() > 0) {
            Object obj = hotSpots.get(0);
            if (obj instanceof HotSpotWithCompliance) {
                hotSpotWithComplianceDao.saveBulkData((List<HotSpotWithCompliance>) hotSpots);
            } else if (obj instanceof HotSpotWithOralCare) {
                hotSpotWithOralCareDao.saveBulkData((List<HotSpotWithOralCare>) hotSpots);
            } else if (obj instanceof HotSpotWithSkinCare) {
                hotSpotWithSkinCareDao.saveBulkData((List<HotSpotWithSkinCare>) hotSpots);
            }
        }
    }

    private void saveNewHotSpots(Questionnaire q) throws SQLException {
        log.verbose("saveNewHotSpots()");
        List<HotSpot> hotSpots = (List<HotSpot>) q.getHotSpots();
        if (hotSpots != null && hotSpots.size() > 0) {
            for (HotSpot hotSpot : hotSpots) hotSpot.setRawData(true);

            hotSpotDao.saveBulkData(hotSpots);
        }
    }

    private void saveNewFixedDisplaySku(Questionnaire q) throws SQLException {
        log.debug("saveNewFixedDisplaySku()");
        List<FixedDisplaySku> fixedDisplaySkus = (List<FixedDisplaySku>) q.getFixedDisplaySkus();

        if (fixedDisplaySkus != null && fixedDisplaySkus.size() > 0) {
            for (FixedDisplaySku f : fixedDisplaySkus) {
                f.setRawData(true);
                log.debug("getQuestionSet()={}", f.getQuestionSet());
                QuestionSet questionSet = questionSetDao.findById(f.getQuestionSetId());
                if (questionSet == null) {
                    questionSet = questionSetDao.createQuestionSetById(f.getQuestionSetId());
                }
                f.setQuestionSet(questionSet);
                f.setShopType(q.getShopType());
            }
            fixedDisplaySkuDao.saveBulkData(fixedDisplaySkus);
        }
    }

    private void saveNewFixedDisplaySku(Questionnaire q, boolean isRaw) throws SQLException {
        log.debug("saveNewFixedDisplaySku()");
        List<FixedDisplaySku> fixedDisplaySkus = (List<FixedDisplaySku>) q.getFixedDisplaySkus();

        if (fixedDisplaySkus != null && fixedDisplaySkus.size() > 0) {
            for (FixedDisplaySku f : fixedDisplaySkus) {
                f.setRawData(true);
                log.debug("getQuestionSet()={}", f.getQuestionSet());
                QuestionSet questionSet = questionSetDao.findById(f.getQuestionSetId());
                if (questionSet == null) {
                    questionSet = questionSetDao.createQuestionSetById(f.getQuestionSetId());
                }
                f.setQuestionSet(questionSet);
                f.setShopType(q.getShopType());
                f.setRawData(isRaw);
                f.setQuestionnaire(q);
            }
            fixedDisplaySkuDao.saveBulkData(fixedDisplaySkus);
        }
    }

    private void saveNewAdditionalSku(Questionnaire q) throws SQLException {
        log.debug("saveNewAdditionalSku()");
        List<AdditionalSku> additionalSkus = (List<AdditionalSku>) q.getAditionalSkus();

        if (additionalSkus != null && additionalSkus.size() > 0) {
            for (AdditionalSku additionalSku : additionalSkus) {
                additionalSku.setRawData(true);
                additionalSku.setShopType(q.getShopType());
            }
            additionalSkuDao.saveBulkData(additionalSkus);
        }
    }

    private void saveNewAdditionalSku(Questionnaire q, boolean isRaw) throws SQLException {
        log.debug("saveNewAdditionalSku()");
        List<AdditionalSku> additionalSkus = (List<AdditionalSku>) q.getAditionalSkus();

        if (additionalSkus != null && additionalSkus.size() > 0) {
            for (AdditionalSku additionalSku : additionalSkus) {
                additionalSku.setRawData(isRaw);
                additionalSku.setShopType(q.getShopType());
                additionalSku.setQuestionnaire(q);
            }
            additionalSkuDao.saveBulkData(additionalSkus);
        }
    }

    @Override
    public long count() {
        log.debug("count()");
        long countOf = 0;
        try {
            countOf = getDao().countOf();
        } catch (SQLException e) {
            e.printStackTrace();
            log.error("Unable to get count", e);
        }
        return countOf;
    }

    @Override
    public void updateQuestionnaire(Questionnaire questionnaire) {
        log.debug("updateQuestionnaire={}");
        try {
            getDao().update(questionnaire);
        } catch (SQLException e) {
            log.verbose("unable to update questionnaire", e);
        }
    }

    @Override
    public List<Questionnaire> getSavedQuestionnaire(int flag) {
        log.verbose("getSavedQuestionnaire() flag={}", flag);
        QueryBuilder<Questionnaire, Integer> queryBuilder = getDao().queryBuilder();
        queryBuilder.clear();
        try {
            if (flag == QuestionnaireListActivity.MENU_ITEM_NOT_UPLOADED) {
                return queryBuilder.where()
                        .eq("newData", false)
                        .and()
                        .eq("synced", false)
                        .and()
                        .isNotNull("dmsCode")
                        .query();
            } else if (flag == QuestionnaireListActivity.MENU_ITEM_UPLOADED) {
                return queryBuilder.where()
                        .eq("newData", false)
                        .and()
                        .eq("synced", true)
                        .and()
                        .isNotNull("dmsCode")
                        .query();
            } else if (flag == QuestionnaireListActivity.MENU_ITEM_ALL) {
                return queryBuilder.where()
                        .eq("newData", false)
                        .and()
                        .isNotNull("dmsCode")
                        .query();
            }

        } catch (SQLException e) {
            log.error("unable to find Questionnaire", e);
        }
        return new ArrayList<Questionnaire>();
    }

    @Override
    public void saveHotSpots(List<? extends HotSpot> hotSpots, Questionnaire questionnaire) {
        if (hotSpots == null) return;
        log.debug("saveHotSpots () hotSpots.size()={}", hotSpots.size());
        try {
            if (hotSpots.size() > 0) {
                Object obj = hotSpots.get(0);
                if (obj instanceof HotSpotWithCompliance) {
                    List<HotSpotWithCompliance> hotSpotWithComplianceList = (List<HotSpotWithCompliance>) hotSpots;
                    for (HotSpotWithCompliance hc : hotSpotWithComplianceList) hc.setQuestionnaire(questionnaire);
                    hotSpotWithComplianceDao.saveBulkData(hotSpotWithComplianceList);
                } else if (obj instanceof HotSpotWithOralCare) {
                    List<HotSpotWithOralCare> hotSpotWithOralCareList = (List<HotSpotWithOralCare>) hotSpots;
                    for (HotSpotWithOralCare hc : hotSpotWithOralCareList) hc.setQuestionnaire(questionnaire);
                    hotSpotWithOralCareDao.saveBulkData(hotSpotWithOralCareList);
                } else if (obj instanceof HotSpotWithSkinCare) {
                    List<HotSpotWithSkinCare> hotSpotWithSkinCareList = (List<HotSpotWithSkinCare>) hotSpots;
                    for (HotSpotWithSkinCare hc : hotSpotWithSkinCareList) hc.setQuestionnaire(questionnaire);
                    hotSpotWithSkinCareDao.saveBulkData(hotSpotWithSkinCareList);
                }
            }
        } catch (SQLException ex) {
            log.error("Unable to save hotspots", ex);
        }
    }

    @Override
    public List<UnsuccessfulAudit> findUnsuccessfulAudits(Questionnaire questionnaire) {
        log.verbose("findUnsuccessfulAudits() by questionnaire.getId()= {}", questionnaire.getId());
        Dao<UnsuccessfulAudit, Integer> dao = unsuccessfulAuditDao.getDao();
        QueryBuilder<UnsuccessfulAudit, Integer> queryBuilder = dao.queryBuilder();
        try {
            List<UnsuccessfulAudit> unsuccessfulAudits = queryBuilder.where().eq("questionnaire_id", questionnaire.getId()).query();
            log.debug("found unsuccessfulAudits.size()={}", unsuccessfulAudits.size());
            return unsuccessfulAudits;
        } catch (SQLException e) {
            log.error("Unable to find unsuccessfulAudit", e);
        }
        return new ArrayList<UnsuccessfulAudit>();
    }

    @Override
    public List<MustHaveSku> findMustHaveSkus(Questionnaire questionnaire) {
        log.debug("findMustHaveSkus() by questionnaire.getId()={}", questionnaire.getId());
        Dao<MustHaveSku, Integer> dao = mustHaveSkuDao.getDao();
        QueryBuilder<MustHaveSku, Integer> queryBuilder = dao.queryBuilder();
        try {
            List<MustHaveSku> mustHaveSkus = queryBuilder.where().eq("questionnaire_id", questionnaire.getId()).query();
            log.debug("found mustHaveSkus.size()={}", mustHaveSkus.size());
            return mustHaveSkus;
        } catch (SQLException e) {
            log.error("Unable to find MustHaveSku", e);
        }
        return new ArrayList<MustHaveSku>();
    }

    @Override
    public List<FixedDisplaySku> findFixedDisplaySku(Questionnaire questionnaire) {
        log.debug("findFixedDisplaySku() by questionnaire.getId()={}", questionnaire.getId());
        Dao<FixedDisplaySku, Integer> dao = fixedDisplaySkuDao.getDao();
        QueryBuilder<FixedDisplaySku, Integer> queryBuilder = dao.queryBuilder();

        try {
            List<FixedDisplaySku> fixedDisplaySkus = queryBuilder.where().eq("questionnaire_id", questionnaire.getId()).query();
            log.debug("found fixedDisplaySkus.size()={}", fixedDisplaySkus.size());
            return queryBuilder.where().eq("questionnaire_id", questionnaire.getId()).query();
        } catch (SQLException e) {
            log.error("Unable to find MustHaveSku", e);
        }
        return new ArrayList<FixedDisplaySku>();
    }

    @Override
    public List<WaveDisplaySku> findWaveDisplaySkus(Questionnaire questionnaire) {
        log.verbose("findWaveDisplaySkus() by questionnaire.getId() ={}", questionnaire.getId());
        Dao<WaveDisplaySku, Integer> dao = waveDisplaySkuDao.getDao();
        QueryBuilder<WaveDisplaySku, Integer> queryBuilder = dao.queryBuilder();
        try {
            List<WaveDisplaySku> waveDisplaySkus = queryBuilder.where().eq("questionnaire_id", questionnaire.getId()).query();
            log.debug("found waveDisplaySkus.size()={}", waveDisplaySkus.size());
            return waveDisplaySkus;
        } catch (SQLException e) {
            log.error("Unable to find WaveDisplaySku", e);
        }
        return new ArrayList<WaveDisplaySku>();
    }

    @Override
    public List<HotSpot> findStoreInfo(Questionnaire questionnaire) {

        return null;
    }

    @Override
    public List<PointOfPurchase> findPointOfPurchases(Questionnaire questionnaire) {
        log.verbose("findPointOfPurchases() by questionnaire.getId()={}", questionnaire.getId());
        Dao<PointOfPurchase, Integer> dao = pointOfPurchaseDao.getDao();
        QueryBuilder<PointOfPurchase, Integer> queryBuilder = dao.queryBuilder();
        try {
            List<PointOfPurchase> pointOfPurchases = queryBuilder.where().eq("questionnaire_id", questionnaire.getId()).query();
            log.debug("pointOfPurchases.size()={}", pointOfPurchases.size());
            return pointOfPurchases;
        } catch (SQLException e) {
            log.error("Unable to find PointOfPurchase");
        }
        return new ArrayList<PointOfPurchase>();
    }

    @Override
    public List<AdditionalSku> findAdditionalSkus(Questionnaire questionnaire) {
        log.verbose("findAdditionalSkus() by questionnaire.getId()={}", questionnaire.getId());
        Dao<AdditionalSku, Integer> dao = additionalSkuDao.getDao();
        QueryBuilder<AdditionalSku, Integer> queryBuilder = dao.queryBuilder();

        try {
            List<AdditionalSku> additionalSkus = queryBuilder.where().eq("questionnaire_id", questionnaire.getId()).query();
            log.debug("additionalSkus.size()={}", additionalSkus.size());
            return additionalSkus;
        } catch (SQLException e) {
            log.error("Unable to find AdditionalSkuJson", e);
        }
        return new ArrayList<AdditionalSku>();
    }

    @Override
    public void deleteQuestionnaire() {

        DeleteBuilder<Questionnaire, Integer> questionDeleteBuilder = getDao().deleteBuilder();
        DeleteBuilder<AdditionalSku, Integer> additionalSkuDeleteBuilder = additionalSkuDao.getDao().deleteBuilder();
        DeleteBuilder<MustHaveSku, Integer> mustHaveSkuDeleteBuilder = mustHaveSkuDao.getDao().deleteBuilder();
        DeleteBuilder<FixedDisplaySku, Integer> fixedDisplaySkuDeleteBuilder = fixedDisplaySkuDao.getDao().deleteBuilder();
        DeleteBuilder<PointOfPurchase, Integer> pointOfPurchaseDeleteBuilder = pointOfPurchaseDao.getDao().deleteBuilder();
        DeleteBuilder<UnsuccessfulAudit, Integer> unsuccessfulAuditDeleteBuilder = unsuccessfulAuditDao.getDao().deleteBuilder();
        DeleteBuilder<WaveDisplaySku, Integer> waveDisplayDeleteBuilder = waveDisplaySkuDao.getDao().deleteBuilder();
        DeleteBuilder<ShopType, Integer> shopTypeDeleteBuilder = shopTypeDao.getDao().deleteBuilder();
        DeleteBuilder<HotSpot, Integer> hotSpotDeleteBuilder = hotSpotDao.getDao().deleteBuilder();

        try {
            questionDeleteBuilder.where().eq("newData", true);
            questionDeleteBuilder.delete();

            additionalSkuDeleteBuilder.where().eq("rawData", true);
            additionalSkuDeleteBuilder.delete();

            mustHaveSkuDeleteBuilder.where().eq("rawData", true);
            mustHaveSkuDeleteBuilder.delete();

            fixedDisplaySkuDeleteBuilder.where().eq("rawData", true);
            fixedDisplaySkuDeleteBuilder.delete();

            pointOfPurchaseDeleteBuilder.where().eq("rawData", true);
            pointOfPurchaseDeleteBuilder.delete();

            unsuccessfulAuditDeleteBuilder.where().eq("newData", true);
            unsuccessfulAuditDeleteBuilder.delete();

            waveDisplayDeleteBuilder.where().eq("newData", true);
            waveDisplayDeleteBuilder.delete();

//            shopTypeDeleteBuilder.where().eq("newData", true);
//            shopTypeDeleteBuilder.delete();

            hotSpotDeleteBuilder.where().eq("newData", true);
            hotSpotDeleteBuilder.delete();


        } catch (SQLException e) {
            log.error("unable delete raw data", e);
        }

    }

    @Override
    public Questionnaire saveQuestionnaire(Questionnaire questionnaire) {
        log.verbose("saveQuestionnaire()");
        ShopType shopType = questionnaire.getShopType();

        try {
            questionnaire.setNewData(false);
            questionnaire = save(questionnaire);

            saveNewAdditionalSku(questionnaire, false);
            saveNewFixedDisplaySku(questionnaire, false);
            //saveNewHotSpots(questionnaire);
            saveNewMustHaveSku(questionnaire, false);
            saveNewPointOfPurchase(questionnaire, false);
            saveNewUnsuccessfulAudits(questionnaire);
            saveWaveDisplaySku(questionnaire, false);
            //saveHotSpots(questionnaire);
        } catch (SQLException e) {
            log.debug("unable to save questionnaire");
        }
        return questionnaire;
    }

    @Override
    public Questionnaire findQuestionnaire(int id) {
        log.debug("findQuestionnaire() by id={}", id);
        try {
            return getDao().queryForId(id);
        } catch (SQLException e) {
            e.printStackTrace();
            log.error("Unable to find queryForId id={}", id);
        }
        return null;
    }
}
