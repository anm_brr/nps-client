/*
 * ******************************************************
 *  * Copyright (C) 2013 A. N. M. Bazlur Rahman <anmbrr.bit0112@gmail.com>
 *  *
 *  * This file is part of perfect-store.
 *  *
 *  * perfect-store can not be copied and/or distributed without the express
 *  * permission of {A. N. M. Bazlur Rahman}
 *  ******************************************************
 */

package com.feratech.nps.dao;

import com.feratech.nps.db.DatabaseHelper;
import com.feratech.nps.domain.MustHaveSku;
import com.feratech.nps.domain.ShopType;
import com.feratech.nps.utils.Logger;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * User: Bazlur Rahman Rokon
 * Date: 6/15/13 - 4:51 AM
 */
@Singleton
public class MustHaveSkuDao extends SimpleDao<MustHaveSku, Integer> {
    private Logger log = Logger.getLogger(MustHaveSkuDao.class);

    @Inject
    public MustHaveSkuDao(DatabaseHelper databaseHelper) throws SQLException {
        super(databaseHelper, databaseHelper.getMustHaveSkuDao());
    }

    public List<MustHaveSku> getMustHaveSkus(ShopType type) {
        log.debug("getMustHaveSkus() type={}", type.getType());
        QueryBuilder<MustHaveSku, Integer> queryBuilder = getDao().queryBuilder();
        try {
            queryBuilder.clear();
            queryBuilder
                    .where()
                    .eq("shopType_id", type.getId())
                    .and()
                    .eq("rawData", true);
            List list = queryBuilder.query();
            log.debug("total found mustHaveSku={}", list.size());
            return list;
        } catch (SQLException e) {
            log.error("Unable to find must have sku", e);
        }
        return new ArrayList<MustHaveSku>();
    }
}
