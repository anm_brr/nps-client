/*
 * ******************************************************
 *  * Copyright (C) 2013 A. N. M. Bazlur Rahman <anmbrr.bit0112@gmail.com>
 *  *
 *  * This file is part of perfect-store.
 *  *
 *  * perfect-store can not be copied and/or distributed without the express
 *  * permission of {A. N. M. Bazlur Rahman}
 *  ******************************************************
 */

package com.feratech.nps.dao;

import com.feratech.nps.db.DatabaseHelper;
import com.feratech.nps.domain.FixedDisplaySku;
import com.feratech.nps.domain.QuestionSet;
import com.feratech.nps.domain.ShopType;
import com.feratech.nps.utils.Logger;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * User: Bazlur Rahman Rokon
 * Date: 6/15/13 - 4:49 AM
 */

@Singleton
public class FixedDisplaySkuDao extends SimpleDao<FixedDisplaySku, Integer> {
    private Logger log = Logger.getLogger(FixedDisplaySku.class);

    @Inject
    private DatabaseHelper databaseHelper;

    @Inject
    public FixedDisplaySkuDao(DatabaseHelper databaseHelper) throws SQLException {
        super(databaseHelper, databaseHelper.getFixedDisplaySkuDao());
    }

    public List<FixedDisplaySku> getFixedDisplaySku(ShopType shopType, QuestionSet questionSet) {
        log.debug("getFixedDisplaySku(), shopType.getType()={},questionSet.getId()={} ", shopType.getType(), questionSet.getId());
        QueryBuilder<FixedDisplaySku, Integer> queryBuilder = getDao().queryBuilder();
        try {
            return
                    queryBuilder.where()
                            .eq("shopType_id", shopType.getId())
                            .and()
                            .eq("questionSet_id", questionSet.getId())
                            .and()
                            .eq("rawData", true)
                            .query();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new ArrayList<FixedDisplaySku>();
    }

    public List<List<FixedDisplaySku>> getNestedFixedDisplaySku(ShopType shopType, QuestionSet questionSet) {
        List<FixedDisplaySku> list = getFixedDisplaySku(shopType, questionSet);
        return getNestedFixedDisplaySku(list);
    }

    public List<List<FixedDisplaySku>> getNestedFixedDisplaySku(List<FixedDisplaySku> list) {
        log.debug("getNestedFixedDisplaySku()");
        Set<Integer> groupIds = new HashSet<Integer>();
        for (FixedDisplaySku f : list) {
            groupIds.add(f.getGroup());
        }
        log.debug("groupIds={}", groupIds);
        List<List<FixedDisplaySku>> nestedFixedDisplaySku = new ArrayList<List<FixedDisplaySku>>();

        for (Integer id : groupIds) {
            List<FixedDisplaySku> fixedDisplaySkus = new ArrayList<FixedDisplaySku>();
            for (FixedDisplaySku f : list) {
                if (f.getGroup() == id) {
                    fixedDisplaySkus.add(f);
                }
            }
            nestedFixedDisplaySku.add(fixedDisplaySkus);
        }
        return nestedFixedDisplaySku;
    }
}
