/*
 * ******************************************************
 *  * Copyright (C) 2013 A. N. M. Bazlur Rahman <anmbrr.bit0112@gmail.com>
 *  *
 *  * This file is part of perfect-store.
 *  *
 *  * perfect-store can not be copied and/or distributed without the express
 *  * permission of {A. N. M. Bazlur Rahman}
 *  ******************************************************
 */

package com.feratech.nps.dao;

import com.feratech.nps.db.DatabaseHelper;
import com.feratech.nps.domain.ShopType;
import com.feratech.nps.utils.Logger;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


/**
 * User: Bazlur Rahman Rokon
 * Date: 6/15/13 - 4:53 AM
 */
@Singleton
public class ShopTypeDao extends SimpleDao<ShopType, Integer> {
    private static final Logger log = Logger.getLogger(ShopTypeDao.class);

    @Inject
    public ShopTypeDao(DatabaseHelper databaseHelper) throws SQLException {
        super(databaseHelper, databaseHelper.getShopTypeDao());
    }

    public List<String> getAllShopType() {
        List<String> types = new ArrayList<String>();
        log.debug("getAllShopType()");
        try {
            List<ShopType> shopTypes = getDao().queryBuilder().distinct().selectColumns("type").query();

            for (ShopType s : shopTypes) {
                types.add(s.getType());
            }
            log.info("getAllShopType()= types.size()={}", types.size());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return types;
    }

    public ShopType findByType(String shopType) {
        log.debug("findByType()");
        QueryBuilder<ShopType, Integer> builder = getDao().queryBuilder();
        try {
            List<ShopType> shopTypes = builder.where().eq("type", shopType).query();
            if (shopTypes != null && shopTypes.size() > 0) return shopTypes.get(0);
        } catch (SQLException e) {
            log.error("unable to find shoType by type");
            e.printStackTrace();
        }
        return null;
    }
}
