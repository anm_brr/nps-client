/*
 * ******************************************************
 *  * Copyright (C) 2013 A. N. M. Bazlur Rahman <anmbrr.bit0112@gmail.com>
 *  *
 *  * This file is part of perfect-store.
 *  *
 *  * perfect-store can not be copied and/or distributed without the express
 *  * permission of {A. N. M. Bazlur Rahman}
 *  ******************************************************
 */

package com.feratech.nps.dao;

import com.feratech.nps.domain.*;

import java.util.List;

/**
 * User: Bazlur Rahman Rokon
 * Date: 5/31/13 - 1:28 AM
 */
public interface QuestionnaireDao {
    public Questionnaire saveQuestionnaire(Questionnaire questionnaire);

    public Questionnaire findQuestionnaire(int id);

    public void saveQuestionnaires(List<Questionnaire> questionnaireList);

    public long count();

    public void updateQuestionnaire(Questionnaire questionnaire);

    public List<Questionnaire> getSavedQuestionnaire(int i);

    public void saveHotSpots(List<? extends HotSpot> hotSpots, Questionnaire questionnaire);

    public List<UnsuccessfulAudit> findUnsuccessfulAudits(Questionnaire questionnaire);

    public List<MustHaveSku> findMustHaveSkus(Questionnaire questionnaire);

    public List<FixedDisplaySku> findFixedDisplaySku(Questionnaire questionnaire);

    public List<WaveDisplaySku> findWaveDisplaySkus(Questionnaire questionnaire);

    public List<HotSpot> findStoreInfo(Questionnaire questionnaire);

    public List<PointOfPurchase> findPointOfPurchases(Questionnaire questionnaire);

    public List<AdditionalSku> findAdditionalSkus(Questionnaire questionnaire);

    void deleteQuestionnaire();


//    public questionnaire getNewQuestionnaire() {
//        questionnaire questionnaire = new questionnaire();
////        questionnaire.setHotSpotList(getNewHotSpotList());
////        questionnaire.setMustHaveSkuList(getNewMustHaveSkuList());
////        questionnaire.setPointOfPurchaseList(getNewPointOfPurchaseList());
////        questionnaire.setAdditionalSkuList(getNewAdditionalSkuList());
////        questionnaire.setWaveDisplaySkuList(getNewWaveDisplaySkuList());
////        questionnaire.setFixedDisplaySkuList(getNewFixedDisplaySkuList());
//        return questionnaire;
//    }
//
////    private Collection<HotSpotJson> getNewHotSpotList() {
////        return new ArrayList() {{
////            add(new HotSpotJson("Location", "Near/In front of Entrance", 0));
////            add(new HotSpotJson("Location", "Beside/Adjacent Cash Counter", 0));
////            add(new HotSpotJson("Height", "Eye Level of Consumers", 0));
////            add(new HotSpotJson("Visibility Status", "Clearly visible/NO obstacle in front of the shelf", 0));
////        }};
////    }
////
////    private Collection<MustHaveSku> getNewMustHaveSkuList() {
////        return new ArrayList<MustHaveSku>() {{
////            add(new MustHaveSku("Wheel Laundry Soap 130g", "201", -1));
////            add(new MustHaveSku("Wheel Washing Powder 500g", "202", -1));
////            add(new MustHaveSku("Wheel Washing Powder 200g", "203", -1));
////            add(new MustHaveSku("Rin Powder 450g", "204", -1));
////            add(new MustHaveSku("Rin Powder 180g", "205", -1));
////            add(new MustHaveSku("Rin Powder 25g", "206", -1));
////            add(new MustHaveSku("Surf excel 20g", "209", -1));
////            add(new MustHaveSku("Close Up 50g", "213", -1));
////            add(new MustHaveSku("Peposdent ToothPowder 100g", "271", -1));
////            add(new MustHaveSku("Peposdent ToothPowder 50g", "272", -1));
////            add(new MustHaveSku("Peposdent 45g", "273", -1));
////        }};
////    }
////
////    private Collection<PointOfPurchase> getNewPointOfPurchaseList() {
////        return new ArrayList<PointOfPurchase>() {{
////            add(new PointOfPurchase("In Store", "Perfect Store Identifier", 0));
////            add(new PointOfPurchase("In Store", "Permanent Shelf Marker", 0));
////            add(new PointOfPurchase("In Store", "Perfect Store Retailers' book", 0));
////        }};
////    }
////
////    private Collection<WaveDisplaySku> getNewWaveDisplaySkuList() {
////        return new ArrayList<WaveDisplaySku>() {{
////            add(new WaveDisplaySku("Wheel Laundry Soap 130g", "201", 1, 1, 0));
////            add(new WaveDisplaySku("Wheel Washing Powder 500g", "202", 1, 1, 0));
////            add(new WaveDisplaySku("Wheel Washing Powder 200g", "203", 1, 1, 0));
////            add(new WaveDisplaySku("Rin Powder 450g", "204", 1, 1, 0));
////            add(new WaveDisplaySku("Rin Powder 180g", "205", 1, 1, 0));
////            add(new WaveDisplaySku("Rin Powder 25g", "206", 1, 0, 0));
////            add(new WaveDisplaySku("Surf excel 20g", "209", 1, 0, 0));
////            add(new WaveDisplaySku("Close Up 50g", "213", 1, 0, 0));
////            add(new WaveDisplaySku("Peposdent ToothPowder 100g", "271", 1, 0, 0));
////            add(new WaveDisplaySku("Peposdent ToothPowder 50g", "272", 1, 0, 0));
////            add(new WaveDisplaySku("Peposdent 45g", "273", 1, 0, 0));
////        }};
////    }
////
////    private Collection<AdditionalSkuJson> getNewAdditionalSkuList() {
////        return new ArrayList<AdditionalSkuJson>() {{
////            add(new AdditionalSkuJson("Wheel Laundry Soap 130g", "201", 0));
////        }};
////    }
////
////    private Collection<FixedDisplaySku> getNewFixedDisplaySkuList() {
////        return new ArrayList<FixedDisplaySku>() {{
////            add(new FixedDisplaySku("Fal Multi Vitamin", "401", 1, 1, 1, 1, 1, 1, 1));
////            add(new FixedDisplaySku("Fal Max Fairness ", "402", 1, 1, 1, 1, 1, 1, 1));
////            add(new FixedDisplaySku("Fal Ayurvedic", "403", 1, 1, 1, 1, 1, 1, 1));
////            add(new FixedDisplaySku("Fal Multi Vitamin Face wash 50g", "404", 1, 1, 1, 1, 1, 1, 1));
////            add(new FixedDisplaySku("Fal Max Fw", "405", 1, 2, 1, 1, 1, 1, 1));
////            add(new FixedDisplaySku("Ponds WB Cream", "406", 1, 2, 1, 1, 1, 1, 1));
////            add(new FixedDisplaySku("Ponds Daily FW", "407", 1, 2, 1, 1, 1, 1, 1));
////            add(new FixedDisplaySku("Ponds Srub 60g", "408", 1, 3, 1, 1, 1, 1, 1));
////            add(new FixedDisplaySku("Pods White Beauty Face wash", "409", 1, 3, 1, 1, 1, 1, 1));
////            add(new FixedDisplaySku("Clear Shampo", "410", 1, 3, 1, 1, 1, 1, 1));
////        }};
////    }
//
////    public Collection<SurveyForm> getAllSurveyForm() {
////        final StoreInfo storeInfo = new StoreInfo();
////        storeInfo.setRegion("Chittagong");
////        storeInfo.setTerritory("CTG Outer-1");
////        storeInfo.setTown("Kaptai");
////        storeInfo.setShopName("Prodip PS");
////        storeInfo.setDmsCode("C46-0047");
////        storeInfo.setShopType("RNG");
////        storeInfo.setShopType("RNG");
////
////        return new ArrayList<SurveyForm>() {{
////            add(new SurveyForm(FormIdGen.getFormId(storeInfo.dmsCode, storeInfo.getShopType()), true, storeInfo));
////            add(new SurveyForm(FormIdGen.getFormId(storeInfo.dmsCode, storeInfo.getShopType()), true, storeInfo));
////            add(new SurveyForm(FormIdGen.getFormId(storeInfo.dmsCode, storeInfo.getShopType()), true, storeInfo));
////            add(new SurveyForm(FormIdGen.getFormId(storeInfo.dmsCode, storeInfo.getShopType()), true, storeInfo));
////        }};
//    }
}
