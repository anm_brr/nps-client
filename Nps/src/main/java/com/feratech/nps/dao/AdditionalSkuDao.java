/*
 * ******************************************************
 *  * Copyright (C) 2013 A. N. M. Bazlur Rahman <anmbrr.bit0112@gmail.com>
 *  *
 *  * This file is part of perfect-store.
 *  *
 *  * perfect-store can not be copied and/or distributed without the express
 *  * permission of {A. N. M. Bazlur Rahman}
 *  ******************************************************
 */

package com.feratech.nps.dao;

import com.feratech.nps.db.DatabaseHelper;
import com.feratech.nps.domain.AdditionalSku;
import com.feratech.nps.domain.ShopType;
import com.feratech.nps.utils.Logger;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * User: Bazlur Rahman Rokon
 * Date: 6/15/13 - 4:44 AM
 */
@Singleton
public class AdditionalSkuDao extends SimpleDao<AdditionalSku, Integer> {
    private Logger log = Logger.getLogger(AdditionalSkuDao.class);

    @Inject
    private DatabaseHelper databaseHelper;

    @Inject
    public AdditionalSkuDao(final DatabaseHelper databaseHelper) throws SQLException {
        super(databaseHelper, databaseHelper.getAdditionalSkuDao());
    }

    public List<AdditionalSku> getAdditionalSku(ShopType shopType) {
        log.debug("getAdditionalSku(), shopType.getType()={}", shopType.getType());
        QueryBuilder<AdditionalSku, Integer> queryBuilder = getDao().queryBuilder();
        try {
            List list = queryBuilder
                    .distinct().selectColumns("skuCode","skuName")
                    .where()
                    .eq("shopType_id", shopType.getId())
                    .and()
                    .eq("rawData", true)
                    .query();

            log.verbose("total found={}",list.size());
            return list;
        } catch (SQLException e) {
            log.verbose("unable to get getWaveDisplaySku()", e);
        }
        return new ArrayList<AdditionalSku>();
    }
}
