/*
 * ******************************************************
 *  * Copyright (C) 2013 A. N. M. Bazlur Rahman <anmbrr.bit0112@gmail.com>
 *  *
 *  * This file is part of perfect-store.
 *  *
 *  * perfect-store can not be copied and/or distributed without the express
 *  * permission of {A. N. M. Bazlur Rahman}
 *  ******************************************************
 */

package com.feratech.nps.dao;


import com.feratech.nps.db.DatabaseHelper;
import com.feratech.nps.domain.HotSpot;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * User: Bazlur Rahman Rokon
 * Date: 6/15/13 - 4:50 AM
 */
@Singleton
public class HotSpotDao extends SimpleDao<HotSpot, Integer> {
    @Inject
    public HotSpotDao(DatabaseHelper databaseHelper) throws SQLException {
        super(databaseHelper, databaseHelper.getHotSpotDao());
    }

    public List<HotSpot> getAll() {
        try {
            return getDao().queryBuilder().limit(4).query();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new ArrayList<HotSpot>();
    }
}
