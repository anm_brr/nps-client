package com.feratech.nps.helper;

import android.app.Application;
import com.feratech.nps.R;
import com.feratech.nps.utils.Logger;
import com.google.inject.Inject;
import roboguice.inject.InjectResource;

/**
 * @author sharafat
 */
public class UrlResolver {
    private static final Logger log = Logger.getLogger(UrlResolver.class);

    @Inject
    private static Application application;

    @InjectResource(R.string.base_url)
    private static String baseUrl;

    @InjectResource(R.string.base_url_2)
    private static String baseUrl2;

    @InjectResource(R.string.api_base_url)
    private static String apiBaseUrl;

    public static String resolve(int urlResourceId) {
        return resolve(application.getString(urlResourceId));
    }

    public static String resolve(int urlResourceId, boolean addApiUrl) {
        return resolve(application.getString(urlResourceId), false);
    }

    public static String resolve(int urlResourceId, Object... args) {
        return resolve(String.format(application.getString(urlResourceId), args));
    }

    public static String resolve(String url) {
        return resolve(url, true);
    }

    public static String resolve(String url, boolean addApiUrl) {
        String absoluteUrl = baseUrl + (addApiUrl ? apiBaseUrl : "") + url;
        log.debug(absoluteUrl);

        return absoluteUrl;
    }

    public static String resolve2(int urlResourceId) {
        return resolve2(application.getString(urlResourceId));
    }

    public static String resolve2(int urlResourceId, boolean addApiUrl) {
        return resolve2(application.getString(urlResourceId), false);
    }

    public static String resolve2(int urlResourceId, Object... args) {
        return resolve2(String.format(application.getString(urlResourceId), args));
    }

    public static String resolve2(String url) {
        return resolve2(url, true);
    }

    public static String resolve2(String url, boolean addApiUrl) {
        String absoluteUrl = baseUrl2 + (addApiUrl ? apiBaseUrl : "") + url;
        log.debug(absoluteUrl);

        return absoluteUrl;
    }
}
