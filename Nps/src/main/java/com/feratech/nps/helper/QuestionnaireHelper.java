package com.feratech.nps.helper;

import com.feratech.nps.domain.*;
import com.google.inject.Singleton;

import java.util.List;

/**
 * User: Bazlur Rahman Rokon
 * Date: 7/10/13 - 11:27 PM
 */
@Singleton
public class QuestionnaireHelper {

    private Questionnaire questionnaire;
    private String dmsCode;
    private ShopType shopType;
    private StoreInfo storeInfo;
    private List<List<List<FixedDisplaySku>>> nestedFixedDisplay;
    private List<MustHaveSku> mustHaveSkus;
    private List<FixedDisplaySku> fixedDisplaySkus;
    private List<? extends HotSpot> hotSpots;
    private List<PointOfPurchase> pointOfPurchases;
    private List<UnsuccessfulAudit> unsuccessfulAudits;
    private List<WaveDisplaySku> waveDisplaySkus;
    private List<AdditionalSku> additionalSkus;
    private List<QuestionMeta> questionMetas;
    private QuestionnaireMeta questionnaireMeta;

    public Questionnaire getQuestionnaire() {
        return questionnaire;
    }

    public void setQuestionnaire(Questionnaire questionnaire) {
        this.questionnaire = questionnaire;
    }

    public QuestionnaireMeta getQuestionnaireMeta() {
        return questionnaireMeta;
    }

    public void setQuestionnaireMeta(QuestionnaireMeta questionnaireMeta) {
        this.questionnaireMeta = questionnaireMeta;
    }

    public List<QuestionMeta> getQuestionMetas() {
        return questionMetas;
    }

    public void setQuestionMetas(List<QuestionMeta> questionMetas) {
        this.questionMetas = questionMetas;
    }

    public List<List<List<FixedDisplaySku>>> getNestedFixedDisplay() {
        return nestedFixedDisplay;
    }

    public void setNestedFixedDisplay(List<List<List<FixedDisplaySku>>> nestedFixedDisplay) {
        this.nestedFixedDisplay = nestedFixedDisplay;
    }

    public String getDmsCode() {
        return dmsCode;
    }

    public void setDmsCode(String dmsCode) {
        this.dmsCode = dmsCode;
    }

    public ShopType getShopType() {
        return shopType;
    }

    public void setShopType(ShopType shopType) {
        this.shopType = shopType;
    }

    public StoreInfo getStoreInfo() {
        return storeInfo;
    }

    public void setStoreInfo(StoreInfo storeInfo) {
        this.storeInfo = storeInfo;
    }

    public List<MustHaveSku> getMustHaveSkus() {
        return mustHaveSkus;
    }

    public void setMustHaveSkus(List<MustHaveSku> mustHaveSkus) {
        this.mustHaveSkus = mustHaveSkus;
    }

    public List<FixedDisplaySku> getFixedDisplaySkus() {
        return fixedDisplaySkus;
    }

    public void setFixedDisplaySkus(List<FixedDisplaySku> fixedDisplaySkus) {
        this.fixedDisplaySkus = fixedDisplaySkus;
    }

    public List<? extends HotSpot> getHotSpots() {
        return hotSpots;
    }

    public void setHotSpots(List<? extends HotSpot> hotSpots) {
        this.hotSpots = hotSpots;
    }

    public List<PointOfPurchase> getPointOfPurchases() {
        return pointOfPurchases;
    }

    public void setPointOfPurchases(List<PointOfPurchase> pointOfPurchases) {
        this.pointOfPurchases = pointOfPurchases;
    }

    public List<UnsuccessfulAudit> getUnsuccessfulAudits() {
        return unsuccessfulAudits;
    }

    public void setUnsuccessfulAudits(List<UnsuccessfulAudit> unsuccessfulAudits) {
        this.unsuccessfulAudits = unsuccessfulAudits;
    }

    public List<WaveDisplaySku> getWaveDisplaySkus() {
        return waveDisplaySkus;
    }

    public void setWaveDisplaySkus(List<WaveDisplaySku> waveDisplaySkus) {
        this.waveDisplaySkus = waveDisplaySkus;
    }

    public List<AdditionalSku> getAdditionalSkus() {
        return additionalSkus;
    }

    public void setAdditionalSkus(List<AdditionalSku> additionalSkus) {
        this.additionalSkus = additionalSkus;
    }
}
