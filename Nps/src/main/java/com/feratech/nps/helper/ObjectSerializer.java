package com.feratech.nps.helper;

import android.content.Context;
import android.os.Environment;
import com.feratech.nps.domain.StoreInfo;
import com.feratech.nps.utils.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.JavaType;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * User: Bazlur Rahman Rokon
 * Date: 8/19/13 - 9:13 PM
 */
public class ObjectSerializer {
    private static final Logger log = Logger.getLogger(ObjectSerializer.class);

    public static void write(Context context, List<StoreInfo> storeInfos) {
        log.verbose("write() storeInfos.size()={}", storeInfos.size());

        try {

            File dir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath() + "/backup/");
            if (!dir.exists()) {
                log.debug("Dir doesn't exist--creating dir");
                dir.mkdirs();
            }
            String path = dir.getPath() + "/stores.bin";
            log.debug("going to write object in ={}", path);

            new ObjectMapper().writeValue(new File(path), storeInfos);

        } catch (FileNotFoundException e) {
            log.error("unable to write file", e);
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (JsonGenerationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static List<StoreInfo>  read(Context context) {
        log.verbose("read()");
        List<StoreInfo> infos =null;
        try {
            InputStream inputStream = context.getAssets().open("stores.bin");
            ObjectMapper mapper = new ObjectMapper();
            JavaType javaType = mapper.getTypeFactory().constructCollectionType(List.class, StoreInfo.class);
           infos = mapper.readValue(inputStream, javaType);
            log.verbose("total Found ={}", infos.size());
        } catch (IOException e) {
            log.error("unable to get binary from assets");
        }
        return infos;
    }


    public static List<StoreInfo>  readFromJson(Context context) {
        log.verbose("read()");
        List<StoreInfo> infos =null;
        try {
            InputStream inputStream = context.getAssets().open("shopesall.json");
            ObjectMapper mapper = new ObjectMapper();
            JavaType javaType = mapper.getTypeFactory().constructCollectionType(List.class, StoreInfo.class);
            infos = mapper.readValue(inputStream, javaType);
            log.verbose("total Found ={}", infos.size());
        } catch (IOException e) {
            log.error("unable to get binary from assets");
        }
        return infos;
    }




}
