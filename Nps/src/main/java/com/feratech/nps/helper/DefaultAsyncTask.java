package com.feratech.nps.helper;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import com.feratech.nps.R;

/**
 * User: Bazlur Rahman Rokon
 * Date: 7/31/13 - 3:45 AM
 */
public abstract class DefaultAsyncTask<Params, Progress, Result> extends AsyncTask<Params, Progress, Result> {
    private Context context;

    ProgressDialog progressDialog;

    protected DefaultAsyncTask(Context context) {
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        progressDialog = new ProgressDialog(context, R.style.Theme_Nps_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.show();
        progressDialog.show();
    }

    @Override
    protected void onPostExecute(Result result) {
        progressDialog.dismiss();
    }
}
