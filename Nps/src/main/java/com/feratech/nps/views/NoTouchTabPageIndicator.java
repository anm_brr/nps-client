package com.feratech.nps.views;

import android.content.Context;
import android.util.AttributeSet;
import com.viewpagerindicator.TabPageIndicator;

/**
 * User: Bazlur Rahman Rokon
 * Date: 8/6/13 - 12:35 PM
 */
public class NoTouchTabPageIndicator extends TabPageIndicator {
    public NoTouchTabPageIndicator(Context context) {
        super(context);
    }

    public NoTouchTabPageIndicator(Context context, AttributeSet attrs) {
        super(context, attrs);
    }


}
