package com.feratech.nps.views;

import android.content.Context;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.feratech.nps.domain.MustHaveSku;
import com.feratech.nps.utils.Logger;
import com.feratech.nps.utils.Utils;

/**
 * User: Bazlur Rahman Rokon
 * Date: 6/24/13 - 1:25 AM
 */
public class MustHaveSkuSingleItem extends LinearLayout {
    private static final Logger log = Logger.getLogger(MustHaveSkuSingleItem.class);

    private MustHaveSku mustHaveSku;

    private int maxLength = 3;

    private TextView skuName;
    private TextView skuCode;
    private EditText skuCount;
    private Context context;
    private InputFilter[] inputFilters;

    public MustHaveSkuSingleItem(Context context, MustHaveSku mustHaveSku) {
        super(context);
        this.context = context;
        this.mustHaveSku = mustHaveSku;
        this.inputFilters = new InputFilter[]{new InputFilter.LengthFilter(maxLength)};

        initialize();
    }

    private void initialize() {
        if (mustHaveSku == null)
            throw new NullPointerException(" MustHaveSku can not be null, you may forget to initialize MustHaveSku");

        this.setOrientation(HORIZONTAL);

        int width = Utils.getWindowWidth(context);
        int skuNameWidth = (int) (width * 0.5);
        int skuCodeWidth = (int) (width * 0.2);
        int skuCountWidth = (int) (width * 0.3);

        final LinearLayout.LayoutParams skuNameParams = new LinearLayout.LayoutParams(skuNameWidth, 60);
        final LinearLayout.LayoutParams skuCodeParams = new LinearLayout.LayoutParams(skuCodeWidth, 60);
        final LinearLayout.LayoutParams skuCountParams = new LinearLayout.LayoutParams(skuCountWidth, 60);

        skuName = new TextView(context);
        skuName.setTextSize(18);
        skuName.setGravity(Gravity.LEFT | Gravity.CENTER);
        skuName.setLayoutParams(skuNameParams);
        skuName.setText(mustHaveSku.getSkuName());
        addView(skuName);

        skuCode = new TextView(context);
        skuCode.setTextSize(18);
        skuCode.setGravity(Gravity.CENTER);
        skuCode.setLayoutParams(skuCodeParams);
        skuCode.setText(mustHaveSku.getSkuCode());
        addView(skuCode);

        skuCount = new EditText(context);
        skuCount.setTextSize(18);
        skuCount.setGravity(Gravity.CENTER);
        skuCount.setLayoutParams(skuCountParams);
        skuCount.setFilters(inputFilters);
        skuCount.setText(mustHaveSku.getSkuCount() <= 0 ? "" : mustHaveSku.getSkuCount() + "");
        skuCount.setInputType(EditorInfo.TYPE_NUMBER_FLAG_SIGNED | EditorInfo.TYPE_CLASS_NUMBER);
        skuCount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                try {
                    mustHaveSku.setSkuCount(Integer.parseInt(editable.toString()));
                } catch (NumberFormatException e) {
                    log.debug("Unable parse to integer");
                }
            }
        });
        addView(skuCount);
    }

    public MustHaveSku getMustHaveSku() {
        log.debug("mustHaveSku={}", mustHaveSku);
        return mustHaveSku;
    }

    public boolean isValid() {
        log.verbose("isValid()");
        boolean isValid = true;
        if (Utils.isEmptyString(skuCount.getText().toString())) {
            isValid = false;
            setError();
        }
        log.verbose("isValid() ,isValid={}", isValid);
        return isValid;
    }

    public void setError() {
        skuCount.setError("Required");
    }
}
