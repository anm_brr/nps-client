package com.feratech.nps.views;

import android.content.Context;
import android.content.res.Resources;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.*;
import com.feratech.nps.R;
import com.feratech.nps.domain.FixedDisplaySku;
import com.feratech.nps.utils.Logger;
import com.feratech.nps.utils.Utils;

import java.util.List;

/**
 * User: Bazlur Rahman Rokon
 * Date: 7/12/13 - 12:19 AM
 */
public class FixedDisplaySkuSingleItem extends LinearLayout {
    private static final Logger log = Logger.getLogger(FixedDisplaySkuSingleItem.class);

    private int maxLength = 2;

    private Context context;
    private List<FixedDisplaySku> fixedDisplaySkus;
    private InputFilter[] inputFilters;

    private LinearLayout llSkuName;
    private LinearLayout llSkuCode;
    private LinearLayout llAvailability;
    private LinearLayout llTotalDisplayCount;
    private LinearLayout llFaceUpDisplayCount;
    private LinearLayout llSSequence;

    private Spinner spnrAvailability;
    private Spinner spinnerSequence;

    private EditText[] totalDisplayCountEditTexts;
    private EditText[] totalFaceUpCountEditTexts;

    public FixedDisplaySkuSingleItem(Context context, List<FixedDisplaySku> fixedDisplaySkus) {
        super(context);
        if (fixedDisplaySkus == null || fixedDisplaySkus.size() == 0) {
            throw new IllegalArgumentException("Fixed Display Sku must not null or empty");
        }

        this.fixedDisplaySkus = fixedDisplaySkus;
        this.context = context;
        this.inputFilters = new InputFilter[]{new InputFilter.LengthFilter(maxLength)};

        initialize();
    }

    private void initialize() {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.fixed_display_single_item, null);

        totalDisplayCountEditTexts = new EditText[fixedDisplaySkus.size()];
        totalFaceUpCountEditTexts = new EditText[fixedDisplaySkus.size()];

        generateGroupView(view);
        addView(view);
    }

    private void generateGroupView(View view) {
        log.debug("generateGroupView()");

        llSkuName = (LinearLayout) view.findViewById(R.id.sku_name);
        llSkuCode = (LinearLayout) view.findViewById(R.id.sku_code);
        llAvailability = (LinearLayout) view.findViewById(R.id.availability);
        llTotalDisplayCount = (LinearLayout) view.findViewById(R.id.total_display_count);
        llFaceUpDisplayCount = (LinearLayout) view.findViewById(R.id.total_face_up_count);
        llSSequence = (LinearLayout) view.findViewById(R.id.sequence);

        int availability = 0, sequence = 0;

        int width = Utils.getWindowWidth(context);

        int height = 80;

        int skuWidth = (int) (width * 0.3);
        int skuCodeWidth = (int) (width * 0.15);
        int availabilityWidth = (int) (width * 0.15);
        int totalDisplayWidth = (int) (width * 0.12);
        int faceUpCountWidth = (int) (width * 0.12);
        int sequenceWidth = (int) (width * 0.15);

        final LayoutParams skuNameParams = new LayoutParams(skuWidth, height);
        final LayoutParams skuCodeParams = new LayoutParams(skuCodeWidth, height);
        final LayoutParams availabilityParams = new LayoutParams(availabilityWidth, height);
        final LayoutParams totalDisplayParams = new LayoutParams(totalDisplayWidth, height);
        final LayoutParams faceUpCountParams = new LayoutParams(faceUpCountWidth, height);
        final LayoutParams sequenceParams = new LayoutParams(sequenceWidth, height);

        int maxLength = 2;

        InputFilter[] inputFilters = new InputFilter[]{new InputFilter.LengthFilter(maxLength)};

        int count = 0;
        for (final FixedDisplaySku fixedDisplaySku : fixedDisplaySkus) {
            TextView skuName = new TextView(context);

            skuName.setTextSize(18);
            skuName.setText(fixedDisplaySku.getSkuName());
            skuName.setLayoutParams(skuNameParams);
            skuName.setGravity(Gravity.LEFT | Gravity.CENTER);
            llSkuName.addView(skuName);

            TextView skuCode = new TextView(context);
            skuCode.setLayoutParams(skuCodeParams);
            skuCode.setText(fixedDisplaySku.getSkuCode());
            skuCode.setTextSize(18);
            skuCode.setFreezesText(true);
            skuCode.setGravity(Gravity.CENTER);
            llSkuCode.addView(skuCode);

            EditText etTotalDisplayCount = new EditText(context);
            etTotalDisplayCount.setLayoutParams(totalDisplayParams);
            etTotalDisplayCount.setTextSize(18);
            etTotalDisplayCount.setEms(2);
            etTotalDisplayCount.setMaxLines(1);
            etTotalDisplayCount.setMaxWidth(totalDisplayWidth);
            etTotalDisplayCount.setFreezesText(true);
            etTotalDisplayCount.setFilters(inputFilters);
            if (fixedDisplaySku.getTotalDisplayCount() > 0) {
                etTotalDisplayCount.setText(String.valueOf(fixedDisplaySku.getTotalDisplayCount()));
            }
            etTotalDisplayCount.setInputType(EditorInfo.TYPE_CLASS_NUMBER);
            etTotalDisplayCount.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                }

                @Override
                public void afterTextChanged(Editable editable) {
                    try {
                        fixedDisplaySku.setTotalDisplayCount(Integer.parseInt(editable.toString()));
                    } catch (NumberFormatException e) {
                        log.error("Unable parse to integer", e);
                    }
                }
            });
            totalDisplayCountEditTexts[count] = etTotalDisplayCount;
            llTotalDisplayCount.addView(etTotalDisplayCount);

            EditText etTotalFaceUpCount = new EditText(context);
            etTotalFaceUpCount.setTextSize(18);
            etTotalFaceUpCount.setLayoutParams(faceUpCountParams);
            etTotalFaceUpCount.setFilters(inputFilters);
            if (fixedDisplaySku.getFaceUpCount() > 0) {
                etTotalFaceUpCount.setText(String.valueOf(fixedDisplaySku.getFaceUpCount()));
            }
            etTotalFaceUpCount.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                }

                @Override
                public void afterTextChanged(Editable editable) {
                    try {
                        fixedDisplaySku.setFaceUpCount(Integer.parseInt(editable.toString()));
                    } catch (NumberFormatException e) {
                        log.debug("Unable parse to integer");
                    }
                }
            });
            etTotalFaceUpCount.setInputType(EditorInfo.TYPE_CLASS_NUMBER);
            totalFaceUpCountEditTexts[count] = etTotalFaceUpCount;
            llFaceUpDisplayCount.addView(etTotalFaceUpCount);

            availability = fixedDisplaySku.isAvailability() ? 1 : 0;
            sequence = fixedDisplaySku.isSequence() ? 1 : 0;

            count++;
        }

        spnrAvailability = (Spinner) llAvailability.findViewById(R.id.spinner);
        spnrAvailability.setLayoutParams(availabilityParams);
        spnrAvailability.setSelection(availability);


        spnrAvailability.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                String selectedText = adapterView.getItemAtPosition(position).toString();
                log.debug("selectedText={}", selectedText);

                Resources res = getResources();
                String[] yesNoSelect = res.getStringArray(R.array.yes_no_select);
                if (selectedText.equals(yesNoSelect[1])) {
                    setYesNoForAvailability(fixedDisplaySkus, 1);
                } else if (selectedText.equals(yesNoSelect[2])) {
                    setYesNoForAvailability(fixedDisplaySkus, 0);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        log.debug("availability ={} " + availability);

        spinnerSequence = (Spinner) llSSequence.findViewById(R.id.spinner);
        spinnerSequence.setSelection(sequence);
        spinnerSequence.setLayoutParams(sequenceParams);
        spinnerSequence.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                String selectedText = adapterView.getItemAtPosition(position).toString();
                log.debug("selectedText={}", selectedText);

                Resources res = getResources();
                String[] yesNoSelect = res.getStringArray(R.array.yes_no_select);
                if (selectedText.equals(yesNoSelect[1])) {
                    setYesNoForSequence(fixedDisplaySkus, 1);
                } else if (selectedText.equals(yesNoSelect[2])) {
                    setYesNoForSequence(fixedDisplaySkus, 0);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        log.debug("sequence ={} " + sequence);
    }

    private void setYesNoForAvailability(List<FixedDisplaySku> list, int value) {
        for (FixedDisplaySku f : list) {
            f.setAvailability(value == 1);
        }
    }

    private void setYesNoForSequence(List<FixedDisplaySku> list, int value) {
        for (FixedDisplaySku f : list) {
            f.setSequence(value == 1);
        }
    }

    public boolean hasError() {
        return (hasErrorFaceUpDisplayCount()
                | hasErrorTotalDisplayCount()
                | hasErrorInAvailability()
                | hasErrorInSequence())
                || hasErrorInDisplayCountAndFlareupCountTogether();
    }

    private boolean hasErrorFaceUpDisplayCount() {
        log.verbose("");
        boolean hasError = false;
        int count = llFaceUpDisplayCount.getChildCount();
        for (int index = 0; index < count; index++) {
            EditText editText = (EditText) llFaceUpDisplayCount.getChildAt(index);
            if (editText.getText().toString().isEmpty()) {
                hasError = true;
                editText.setError("Required");
            }
        }
        return hasError;
    }

    private boolean hasErrorTotalDisplayCount() {
        boolean hasError = false;
        int count = llTotalDisplayCount.getChildCount();
        for (int index = 0; index < count; index++) {
            EditText editText = (EditText) llTotalDisplayCount.getChildAt(index);
            if (editText.getText().toString().isEmpty()) {
                hasError = true;
                editText.setError("Required");
            }
        }
        return hasError;
    }

    private boolean hasErrorInAvailability() {
        boolean hasError = false;
        String selectedText = spnrAvailability.getSelectedItem().toString();
        Resources res = getResources();
        String[] yesNoSelect = res.getStringArray(R.array.yes_no_select);
        if (selectedText.equals(yesNoSelect[0])) {

            int count = spnrAvailability.getChildCount();
            for (int i = 0; i < count; i++) {
                View view = spnrAvailability.getChildAt(i);
                if (view instanceof TextView) {
                    TextView textView = (TextView) spnrAvailability.getChildAt(i);
                    textView.setError("Required");
                    break;
                }
            }
        }
        return hasError;
    }

    private boolean hasErrorInSequence() {
        boolean hasError = false;
        String selectedText = spinnerSequence.getSelectedItem().toString();
        Resources res = getResources();
        String[] yesNoSelect = res.getStringArray(R.array.yes_no_select);
        if (selectedText.equals(yesNoSelect[0])) {
            int count = spinnerSequence.getChildCount();
            for (int i = 0; i < count; i++) {
                View view = spinnerSequence.getChildAt(i);
                if (view instanceof TextView) {
                    TextView textView = (TextView) spinnerSequence.getChildAt(i);
                    textView.setError("Required");
                    break;
                }
            }
            hasError = true;
        }
        return hasError;
    }

    private boolean hasErrorInDisplayCountAndFlareupCountTogether() {
        boolean hasError = false;
        for (int i = 0; i < totalDisplayCountEditTexts.length; i++) {
            String totalDisplayCount = totalDisplayCountEditTexts[i].getText().toString();
            String totalFaceUpCount = totalFaceUpCountEditTexts[i].getText().toString();
            try {
                if (Integer.parseInt(totalDisplayCount) < Integer.parseInt(totalFaceUpCount)) {
                    totalDisplayCountEditTexts[i].setError("Display Count should be bigger than Faceup Count");
                    hasError = true;
                }
            } catch (NumberFormatException e) {
                //ignored
            }
        }

        return hasError;
    }
}
